package Util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.CharBuffer;

/**
 * Created by james on 2/16/2016.
 */
public class F {
    public static String InStream2String(InputStream in)
    {
        StringBuilder builder = new StringBuilder(256);
        InputStreamReader r = new InputStreamReader(in);
        CharBuffer buff = CharBuffer.allocate(64);
        try {
            while (-1 != r.read(buff)) {
                buff.flip();
                builder.append(buff);
                buff.clear();
            }
        } catch (IOException e)
        {
            return null;
        }
        finally {
            try {
                r.close();
            } catch (IOException e) {
                //ignore
            }
        }
        return builder.toString();
    }
}
