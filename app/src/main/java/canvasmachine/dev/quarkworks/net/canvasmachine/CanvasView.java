package canvasmachine.dev.quarkworks.net.canvasmachine;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

/**
 * TODO: document your custom view class.
 */
public class CanvasView extends View {

    public OnDraw od_cb = null;
    String mLabelAttr = null;
    Rect m_drawBounds = new Rect();
    Paint mTextPaint = null;
    int label_baseline = 70;
    int label_startx = 10;
    Paint.FontMetricsInt met_tmp = new Paint.FontMetricsInt();
    public CanvasView(Context context) {
        super(context);
        init(null, 0);
    }

    public CanvasView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.CanvasView, defStyle, 0);
        mLabelAttr = a.getString(R.styleable.CanvasView_label);
        a.recycle();
        mTextPaint = new Paint(Paint.UNDERLINE_TEXT_FLAG);
        mTextPaint.setColor(Color.BLACK);
        mTextPaint.setShadowLayer(5, 5, 5, Color.HSVToColor(100, new float[]{135f, .9f, .1f}));
        mTextPaint.setTypeface(Typeface.MONOSPACE);
        mTextPaint.setTextSize(50);
        mTextPaint.setTextAlign(Paint.Align.LEFT);


    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.save();
        if (mLabelAttr != null) {
            canvas.drawText(mLabelAttr, (float) label_startx, (float) label_baseline, mTextPaint);
        }

        if (od_cb != null) {
            try {
                od_cb.draw_function(canvas);
            } catch (Throwable e) {
                od_cb.exception(e.getMessage(), e);
            }
        }

        canvas.restore();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int min_w;
        int min_h;
        if (mLabelAttr != null) { // this will not account for shadow layer
            mTextPaint.getFontMetricsInt(met_tmp);
            min_h = met_tmp.bottom + label_baseline + 1;
            min_w = (int) (mTextPaint.measureText(mLabelAttr) + .5) + 10 + 1;

        } else {
            min_w = getSuggestedMinimumWidth();
            min_h = getSuggestedMinimumHeight();
        }

        setMeasuredDimension(
                resolveSizeAndState(min_w, widthMeasureSpec, 0),
                resolveSizeAndState(min_h, heightMeasureSpec, 0)
        );

    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        m_drawBounds.set(left, top, right, bottom);
    }

    public interface OnDraw {
        void draw_function(Canvas canvas);

        void exception(String message, Throwable cause);
    }
}
