package canvasmachine.dev.quarkworks.net.canvasmachine;

import android.app.Activity;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.CharBuffer;

import Native.CanvasMachine;
import Native.MyPipe.MyPipe;

public class Display extends Activity {

    static {
        System.loadLibrary("mymodule");
    }


    private Handler main = new Handler();
    //private CanvasView mCanvas_view;
    //private TextView mMessages;


    @Override
    protected void onDestroy() {
        super.onDestroy();
     }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rootview);
        //mCanvas_view = (CanvasView) findViewById(R.id.canvas_view);
        //mMessages = (TextView) findViewById(R.id.messages_TV);
        //mMessages.setMovementMethod(new ScrollingMovementMethod());
        init_();
    }

    private void init_() {
        CanvasMachine.test(getApplication(), R.raw.test);
    }




}
