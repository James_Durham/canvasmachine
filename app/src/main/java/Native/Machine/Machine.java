package Native.Machine;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.graphics.Picture;
import android.graphics.RectF;

import java.util.HashMap;


/**
 * Created by james on 1/11/2016.
 */
public class Machine {

    public PathEffect PE_regs[] = new PathEffect[10];
    public Path PT_regs[] = new Path[10];
    public Picture PI_regs[] = new Picture[10];
    public Bitmap BM_regs[] = new Bitmap[10];
    public Paint PA_regs[] = new Paint[10];
    public RectF RF_regs[] = new RectF[10];

    ACB_imp acb;

    public Machine() {
        acb = new ACB_imp(this);
    }



    public ActionsCallBacks getACB_imp()
    {
        return acb;
    }
    public ReBindableRunList<Canvas> getOnDraw()
    {
        return acb.onDraw_runtime;
    }



}


