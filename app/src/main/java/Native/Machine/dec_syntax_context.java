package Native.Machine;

/**
 * Created by james on 1/25/2016.
 */
public abstract class dec_syntax_context<T> {

    Class t_class;
    T [] regs;
    int reg_index;
    T reg;
    Object [] params;

    public dec_syntax_context(T[] regs) {
        this.regs = regs;
        t_class = regs.getClass().getComponentType();
    }
    public void start(int reg_index, Object... params)
    {
        this.reg_index = reg_index;
        this.reg = regs[reg_index];
        this.params = params;
        A.reg_test(t_class, regs, reg_index);
        onStart();
    }
    protected abstract void onStart();

    public void stop()
    {
        onStop();
    }
    protected abstract void onStop();
}

