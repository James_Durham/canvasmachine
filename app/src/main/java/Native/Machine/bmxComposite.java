package Native.Machine;

import android.graphics.Bitmap;

/**
 * Created by james on 1/19/2016.
 */
class bmxComposite extends dec_syntax_context<Bitmap> {
    public BM_Init bminit = new BM_Init();
    public bmxComposite(Bitmap[] regs) {
        super(regs);
    }


    @Override
    protected void onStart() {
        bminit.set_BM(reg);
    }

    @Override
    protected void onStop() {
        bminit.complete();
        bminit.calls = null;
        bminit.bm = null;
        bminit.canvas = null;
    }
}
