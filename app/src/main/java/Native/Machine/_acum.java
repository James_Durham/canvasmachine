package Native.Machine;

import java.util.LinkedList;

/**
 * Created by james on 1/25/2016.
 */
public abstract class _acum<T> {
    private String class_simple_name = this.getClass().getSimpleName();
    protected LinkedList<T> acum = new LinkedList<T>();

    protected void push(T item) {
        acum.push(item);
    }

    protected T pop() {
        return acum.pop();
    }

    protected void sanity_check() {
        String ass = String.format("%s, sanity check: acum.size() != 0!", class_simple_name);
        A.assert_(ass, acum.size() == 0);
    }

    public T getresult() {
        T ret = pop();
        sanity_check();
        return ret;
    }
}
