package Native.Machine;

import android.graphics.Path;

/**
 * Created by james on 1/19/2016.
 */
class ptxInitial extends dec_syntax_context<Path> {
    public PT_Init ptinit = new PT_Init();
    public ptxInitial(Path[] regs) {
        super(regs);
    }

    @Override
    protected void onStart() {
        ptinit.set_PT(reg);
    }

    @Override
    protected void onStop() {
        ptinit.complete();
    }
}
