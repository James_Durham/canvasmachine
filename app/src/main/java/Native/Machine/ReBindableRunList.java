package Native.Machine;


/**
 * Created by james on 1/27/2016.
 */
public abstract class ReBindableRunList<T> extends RunList {

    public T binding;

    public void runlist(T binding)
    {
        this.binding = binding;
        super.runlist();
        this.binding = null;
    }

    @Override
    public void runlist() {
        A.assert_("ReBindableRunList<T> runlist(): binding == null", binding != null);
        super.runlist();
        binding = null;
    }
}
