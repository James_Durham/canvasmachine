package Native.Machine;

/**
 * Created by james on 1/2/2016.
 */
public interface ActionsCallBacks {
    int float_bin_cnst_Act( float a, float b );
    int float_tri_cnst_Act( float a, float b, float c );
    int float_quad_cnst_Act( float a, float b, float c, float d );
    int float_list_empty_Act();
    int float_list_F_Act( float apnd );
    int float_list_FL_F_Act( float apnd );
    int float_bin_list_empty_Act();
    int float_bin_list_FBC_Act();
    int float_bin_list_FBL_FBC_Act();
    int float_quad_list_empty_Act();
    int float_quad_list_FQC_Act();
    int float_quad_list_FQL_FQC_Act();
    int float_array_Act();
    int float_bin_array_Act();
    int float_quad_array_Act();

    int color_cnst_sv_color_string_sv_Act( int c );
    int color_cnst_sv_color_hsv_sv_Act   ( int c );
    int color_cnst_sv_color_argb_sv_Act  ( int c );

    int color_cnst_list_empty_Act();
    int color_cnst_list_CC_Act( );
    int color_cnst_list_CCL_CC_Act();
    int color_cnst_array_Act();


    int pdm_enum_PDM_ADD_Act();
    int pdm_enum_PDM_CLEAR_Act();
    int pdm_enum_PDM_DARKEN_Act();
    int pdm_enum_PDM_DST_Act();
    int pdm_enum_PDM_DST_ATOP_Act();
    int pdm_enum_PDM_DST_IN_Act();
    int pdm_enum_PDM_DST_OUT_Act();
    int pdm_enum_PDM_DST_OVER_Act();
    int pdm_enum_PDM_LIGHTEN_Act();
    int pdm_enum_PDM_MULTIPLY_Act();
    int pdm_enum_PDM_OVERLAY_Act();
    int pdm_enum_PDM_SCREEN_Act();
    int pdm_enum_PDM_SRC_Act();
    int pdm_enum_PDM_SRC_ATOP_Act();
    int pdm_enum_PDM_SRC_IN_Act();
    int pdm_enum_PDM_SRC_OUT_Act();
    int pdm_enum_PDM_SRC_OVER_Act();
    int pdm_enum_PDM_XOR_Act();

    int pe_element_PE_COMPOSE_Act();
    int pe_element_PE_CORNER_Act( float radius );
    int pe_element_PE_DASH_Act( float phase );
    int pe_element_PE_DISCRETE_Act( float segmentLength, float deviation );
    int pe_element_PE_PATHDASH_Act( int path, float advance, float phase );
    int pe_element_PE_SUM_Act();

    int pathdash_style_enum_MORPH_Act();
    int pathdash_style_enum_ROTATE_Act();
    int pathdash_style_enum_TRANSLATE_Act();

    int shader_BITMAP_Act( int bitmap );
    int shader_COMPOSE_Act();
    int shader_LINEAR_Act( float x0, float y0, float x1, float y1);
    int shader_RADIAL_Act( float centerX, float centerY, float radius );
    int shader_SWEEP_Act( float cx, float cy );
    int shader_tile_CLAMP_Act();
    int shader_tile_MIRROR_Act();
    int shader_tile_REPEAT_Act();

    int xfermode_Avoid_Act( int opColor, int tolerance );
    int xfermode_PixelXor_Act( int opColor );
    int xfermode_PorterDuff_Act();
    int xf_avoid_enum_AVOID_Act();
    int xf_avoid_enum_TARGET_Act();

    int maskfilter_BLUR_Act( float radius );
    int maskfilter_EMBOSS_Act( float ambient, float specular, float blurRadius );
    int mf_blur_enum_INNER_Act();
    int mf_blur_enum_NORMAL_Act();
    int mf_blur_enum_OUTER_Act();
    int mf_blur_enum_SOLID_Act();

    int colorfilter_ColorMatrix_Act();
    int colorfilter_Lighting_Act( int mul, int add );
    int colorfilter_PorterDuff_Act( int color );
    int colormatrix_Scale_Act( float rScale, float gScale, float bScale, float aScale );
    int colormatrix_Saturation_Act( float sat );
    int colormatrix_YUV2RGB_Act();
    int colormatrix_RGB2YUV_Act();

    int canvascall_CLIPPATH_Act( int path );
    int canvascall_CLIPRECT_ffff_Act( float left, float top, float right, float bottom );
    int canvascall_CLIPRECT_rf_Act( int rect );
    int canvascall_SAVE_Act();
    int canvascall_RESTORE_Act();
    int canvascall_ROTATE_f_Act( float degrees );
    int canvascall_ROTATE_fff_Act( float degrees, float px, float py );
    int canvascall_SCALE_ff_Act( float sx, float sy );
    int canvascall_SCALE_ffff_Act( float sx, float sy, float px, float py );
    int canvascall_SKEW_Act( float sx, float sy );
    int canvascall_TRANSLATE_Act( float dx, float dy );
    int canvascall_DRAWARC_Act( int oval, float startAngle, float sweepAngle, int useCenter, int paint );
    int canvascall_DRAWBITMAP_Act( int bitmap, float left, float top, int paint );
    int canvascall_DRAWBITMAPMESH_Act( int bitmap, int meshWidth, int meshHeight, int vertOffset, int colorOffset, int paint );
    int canvascall_DRAWCIRCLE_Act( float cx, float cy, float radius, int paint );
    int canvascall_DRAWCOLOR_i_Act( int color );
    int canvascall_DRAWCOLOR_ie_Act( int color );
    int canvascall_DRAWLINES_Act( int paint );
    int canvascall_DRAWOVAL_Act( int oval, int paint );
    int canvascall_DRAWPATH_Act( int path, int paint );
    int canvascall_DRAWPICTURE_pirf_Act( int picture, int dst );
    int canvascall_DRAWPICTURE_pi_Act( int picture );
    int canvascall_DRAWPOINTS_Act( int paint );
    int canvascall_DRAWRECT_ffff_Act( float left, float top, float right, float bottom, int paint );
    int canvascall_DRAWRECT_rf_Act( int rect, int paint );
    int canvascall_DRAWROUNDRECT_Act( int rect, float rx, float ry, int paint );
    int canvascall_DRAWTEXT_Act( String text, float x, float y, int paint );
    int canvascall_DRAWTEXTONPATH_Act( String text, int path, float hOffset, float vOffset, int paint );

    int pt_initcall_FILLTYPE_Act();
    int pt_initcall_CLOSE_Act();
    int pt_initcall_CUBICTO_Act( float x1, float y1, float x2, float y2, float x3, float y3 );
    int pt_initcall_RCUBICTO_Act( float x1, float y1, float x2, float y2, float x3, float y3 );
    int pt_initcall_LINETO_Act( float x, float y );
    int pt_initcall_RLINETO_Act( float dx, float dy );
    int pt_initcall_MOVETO_Act( float x, float y );
    int pt_initcall_RMOVETO_Act( float dx, float dy );
    int pt_initcall_QUADTO_Act( float x1, float y1, float x2, float y2 );
    int pt_initcall_RQUADTO_Act( float dx1, float dy1, float dx2, float dy2 );
    int pt_initcall_TRANSFORM_Act( float [] mat );

    int filltype_enum_EVEN_ODD_Act();
    int filltype_enum_INVERSE_EVEN_ODD_Act();
    int filltype_enum_INVERSE_WINDING_Act();
    int filltype_enum_WINDING_Act();

    int rf_modcall_INSET_Act( float dx, float dy );
    int rf_modcall_INTERSECT_Act( float left, float top, float right, float bottom );
    int rf_modcall_OFFSET_Act( float dx, float dy );
    int rf_modcall_SORT_Act();
    int rf_modcall_UNION_ffff_Act( float left, float top, float right, float bottom );
    int rf_modcall_UNION_ff_Act( float x, float y );

    int pa_fntcall_HINTING_Act( int bool );
    int pa_fntcall_TEXTALIGN_Act();
    int pa_fntcall_TEXTSCALEX_Act( float scaleX );
    int pa_fntcall_TEXTSIZE_Act( float textSize );
    int pa_fntcall_TEXTSKEWX_Act( float skewX );
    int pa_fntcall_SETTYPEFACE_Act();

    int pa_align_enum_CENTER_Act();
    int pa_align_enum_LEFT_Act();
    int pa_align_enum_RIGHT_Act();

    int typeface_DEFAULT_Act();
    int typeface_DEFAULT_BOLD_Act();
    int typeface_MONOSPACE_Act();
    int typeface_SANS_SERIF_Act();
    int typeface_SERIF_Act();

    int pa_strkcall_STROKECAP_Act();
    int pa_strkcall_STROKEJOIN_Act();
    int pa_strkcall_STROKEMITER_Act( float miter );
    int pa_strkcall_STROKEWIDTH_Act( float width );
    int pa_strkcall_PATHEFFECT_Act( int effect );

    int cap_enum_BUTT_Act();
    int cap_enum_ROUND_Act();
    int cap_enum_SQUARE_Act();

    int join_enum_BEVEL_Act();
    int join_enum_MITER_Act();
    int join_enum_ROUND_Act();

    int pa_fnt_init_Act();
    int pa_clr_init_Act( int color );
    int pa_pathmode_init_Act();
    int pa_stroke_init_Act();
    int pa_fill_init_Act();
    int pa_cmstmd_init_Act();
    int pa_mf_init_Act();
    int pa_cf_init_Act();

    int pa_style_enum_FILL_Act();
    int pa_style_enum_FILL_AND_STROKE_Act();
    int pa_style_enum_STROKE_Act();

    int pexSet_midAct( int effect );
    int pexSet_Act();

    int ptxInitial_midAct( int path );
    int ptxInitial_Act();

    int pixRecord_midAct( int picture, int width, int height );
    int pixRecord_Act();

    int bmxComposite_midAct( int bitmap );
    int bmxComposite_Act();

    int rfxMod_midAct( int rect );
    int rfxMod_Act();

    int paxInitial_midAct( int paint );
    int paxInitial_Act();

    int alloc_PE_ref_sv_Act( int effect );
    int alloc_PT_ref_sv_Act( int path );
    int alloc_PI_ref_sv_Act( int picture );
    int alloc_BM_ref_sv_Act( int bitmap, int width, int height );
    int alloc_PA_ref_sv_Act( int paint, int flags );
    int alloc_RF_ref_sv_Act( int rect, float left, float top, float right, float bottom );

    int declarations_midAct();
    int declarations_Act();

    int onDraw_midAct();
    int onDraw_Act();

    int start();
    int end();
}
