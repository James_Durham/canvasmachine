package Native.Machine;

import android.graphics.Canvas;
import android.graphics.Picture;

/**
 * Created by james on 1/29/2016.
 */
public class PI_Init implements Complete<Picture>{
    public Picture pi;
    public canvascalls_ calls = new canvascalls_();
    public Canvas canvas;

    public void set_PI(Picture pi, int width, int height) {
        this.pi = pi;
        canvas = pi.beginRecording(width, height);
    }

    @Override
    public Picture complete() {
        calls.runlist(canvas);
        //clear the list here.
        calls.list.clear();
        pi.endRecording();
        canvas = null;
        return pi;
    }
}
