package Native.Machine;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Picture;
import android.graphics.PorterDuff;
import android.graphics.RectF;

/**
 * Created by james on 1/20/2016.
 */
class canvascalls_ extends ReBindableRunList<Canvas> {

    @Override
    public void clear_list_option() {
        //do not clear list
        //manually clear the list in the *_Init's
    }

    public void make_canvascall_CLIPPATH(Path path)
    {(new canvascall_CLIPPATH(path)).add_tolist();}
    public void make_canvascall_CLIPRECT_ffff(float left, float top, float right, float bottom)
    {(new canvascall_CLIPRECT_ffff(left, top, right, bottom)).add_tolist();}
    public void make_canvascall_CLIPRECT_rf(RectF rect)
    {(new canvascall_CLIPRECT_rf(rect)).add_tolist();}
    public void make_canvascall_SAVE()
    {(new canvascall_SAVE()).add_tolist();}
    public void make_canvascall_RESTORE()
    {(new canvascall_RESTORE()).add_tolist();}
    public void make_canvascall_ROTATE_f(float degrees)
    {(new canvascall_ROTATE_f(degrees)).add_tolist();}
    public void make_canvascall_ROTATE_fff(float degrees, float px, float py)
    {(new canvascall_ROTATE_fff(degrees, px, py)).add_tolist();}
    public void make_canvascall_SCALE_ff(float sx, float sy)
    {(new canvascall_SCALE_ff(sx, sy)).add_tolist();}
    public void make_canvascall_SCALE_ffff(float sx, float sy, float px, float py)
    {(new canvascall_SCALE_ffff(sx, sy, px, py)).add_tolist();}
    public void make_canvascall_SKEW(float sx, float sy)
    {(new canvascall_SKEW(sx, sy)).add_tolist();}
    public void make_canvascall_TRANSLATE(float dx, float dy)
    {(new canvascall_TRANSLATE(dx, dy)).add_tolist();}
    public void make_canvascall_DRAWARC(RectF oval, float startAngle, float sweepAngle, boolean useCenter, Paint paint)
    {(new canvascall_DRAWARC(oval, startAngle, sweepAngle, useCenter, paint)).add_tolist();}
    public void make_canvascall_DRAWBITMAP(Bitmap bitmap, float left, float top, Paint paint)
    {(new canvascall_DRAWBITMAP(bitmap, left, top, paint)).add_tolist();}
    public void make_canvascall_DRAWBITMAPMESH(Bitmap bitmap, int meshWidth, int meshHeight, float[] verts, int vertOffset, int[] colors, int colorOffset, Paint paint)
    {(new canvascall_DRAWBITMAPMESH(bitmap, meshWidth, meshHeight, verts, vertOffset, colors, colorOffset, paint)).add_tolist();}
    public void make_canvascall_DRAWCIRCLE(float cx, float cy, float radius, Paint paint)
    {(new canvascall_DRAWCIRCLE(cx, cy, radius, paint)).add_tolist();}
    public void make_canvascall_DRAWCOLOR_i(int color)
    {(new canvascall_DRAWCOLOR_i(color)).add_tolist();}
    public void make_canvascall_DRAWCOLOR_ie(int color, PorterDuff.Mode mode)
    {(new canvascall_DRAWCOLOR_ie(color, mode)).add_tolist();}
    public void make_canvascall_DRAWLINES(float[] pts, Paint paint)
    {(new canvascall_DRAWLINES(pts, paint)).add_tolist();}
    public void make_canvascall_DRAWOVAL(RectF oval, Paint paint)
    {(new canvascall_DRAWOVAL(oval, paint)).add_tolist();}
    public void make_canvascall_DRAWPATH(Path path, Paint paint)
    {(new canvascall_DRAWPATH(path, paint)).add_tolist();}
    public void make_canvascall_DRAWPICTURE_pirf(Picture picture, RectF dst)
    {(new canvascall_DRAWPICTURE_pirf(picture, dst)).add_tolist();}
    public void make_canvascall_DRAWPICTURE_pi(Picture picture)
    {(new canvascall_DRAWPICTURE_pi(picture)).add_tolist();}
    public void make_canvascall_DRAWPOINTS(float[] pts, Paint paint)
    {(new canvascall_DRAWPOINTS(pts, paint)).add_tolist();}
    public void make_canvascall_DRAWRECT_ffff(float left, float top, float right, float bottom, Paint paint)
    {(new canvascall_DRAWRECT_ffff(left, top, right, bottom, paint)).add_tolist();}
    public void make_canvascall_DRAWRECT_rf(RectF rect, Paint paint)
    {(new canvascall_DRAWRECT_rf(rect, paint)).add_tolist();}
    public void make_canvascall_DRAWROUNDRECT(RectF rect, float rx, float ry, Paint paint)
    {(new canvascall_DRAWROUNDRECT(rect, rx, ry, paint)).add_tolist();}
    public void make_canvascall_DRAWTEXT(String text, float x, float y, Paint paint)
    {(new canvascall_DRAWTEXT(text, x, y, paint)).add_tolist();}
    public void make_canvascall_DRAWTEXTONPATH(String text, Path path, float hOffset, float vOffset, Paint paint)
    {(new canvascall_DRAWTEXTONPATH(text, path, hOffset, vOffset, paint)).add_tolist();}


    public class canvascall_CLIPPATH extends mini_sub {
        private final Path path;

        public canvascall_CLIPPATH(Path path) {
            this.path = path;
        }

        @Override
        public void run() {
            binding.clipPath(path);
        }
    }

    public class canvascall_CLIPRECT_ffff extends mini_sub {
        private final float left;
        private final float top;
        private final float right;
        private final float bottom;

        public canvascall_CLIPRECT_ffff(float left, float top, float right, float bottom) {
            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
        }

        @Override
        public void run() {
            binding.clipRect(left, top, right, bottom);
        }
    }

    public class canvascall_CLIPRECT_rf extends mini_sub {
        private final RectF rect;

        public canvascall_CLIPRECT_rf(RectF rect) {
            this.rect = rect;
        }

        @Override
        public void run() {
            binding.clipRect(rect);
        }
    }

    public class canvascall_SAVE extends mini_sub {
        public canvascall_SAVE() {
        }

        @Override
        public void run() {
            binding.save();
        }
    }

    public class canvascall_RESTORE extends mini_sub {
        public canvascall_RESTORE() {
        }

        @Override
        public void run() {
            binding.restore();
        }
    }

    public class canvascall_ROTATE_f extends mini_sub {
        private final float degrees;

        public canvascall_ROTATE_f(float degrees) {
            this.degrees = degrees;
        }

        @Override
        public void run() {
            binding.rotate(degrees);
        }
    }

    public class canvascall_ROTATE_fff extends mini_sub {
        private final float degrees;
        private final float px;
        private final float py;

        public canvascall_ROTATE_fff(float degrees, float px, float py) {
            this.degrees = degrees;
            this.px = px;
            this.py = py;
        }

        @Override
        public void run() {
            binding.rotate(degrees, px, py);
        }
    }

    public class canvascall_SCALE_ff extends mini_sub {
        private final float sx;
        private final float sy;

        public canvascall_SCALE_ff(float sx, float sy) {
            this.sx = sx;
            this.sy = sy;
        }

        @Override
        public void run() {
            binding.scale(sx, sy);
        }
    }

    public class canvascall_SCALE_ffff extends mini_sub {
        private final float sx;
        private final float sy;
        private final float px;
        private final float py;

        public canvascall_SCALE_ffff(float sx, float sy, float px, float py) {
            this.sx = sx;
            this.sy = sy;
            this.px = px;
            this.py = py;
        }

        @Override
        public void run() {
            binding.scale(sx, sy, px, py);
        }
    }

    public class canvascall_SKEW extends mini_sub {
        private final float sx;
        private final float sy;

        public canvascall_SKEW(float sx, float sy) {
            this.sx = sx;
            this.sy = sy;
        }

        @Override
        public void run() {
            binding.skew(sx, sy);
        }
    }

    public class canvascall_TRANSLATE extends mini_sub {
        private final float dx;
        private final float dy;

        public canvascall_TRANSLATE(float dx, float dy) {
            this.dx = dx;
            this.dy = dy;
        }

        @Override
        public void run() {
            binding.translate(dx, dy);
        }
    }

    public class canvascall_DRAWARC extends mini_sub {
        private final RectF oval;
        private final float startAngle;
        private final float sweepAngle;
        private final boolean useCenter;
        private final Paint paint;

        public canvascall_DRAWARC(RectF oval, float startAngle, float sweepAngle, boolean useCenter, Paint paint) {
            this.oval = oval;
            this.startAngle = startAngle;
            this.sweepAngle = sweepAngle;
            this.useCenter = useCenter;
            this.paint = paint;
        }

        @Override
        public void run() {
            binding.drawArc(oval, startAngle, sweepAngle, useCenter, paint);
        }
    }

    public class canvascall_DRAWBITMAP extends mini_sub {
        private final Bitmap bitmap;
        private final float left;
        private final float top;
        private final Paint paint;

        public canvascall_DRAWBITMAP(Bitmap bitmap, float left, float top, Paint paint) {
            this.bitmap = bitmap;
            this.left = left;
            this.top = top;
            this.paint = paint;
        }

        @Override
        public void run() {
            binding.drawBitmap(bitmap, left, top, paint);
        }
    }

    public class canvascall_DRAWBITMAPMESH extends mini_sub {
        private final Bitmap bitmap;
        private final int meshWidth;
        private final int meshHeight;
        private final float[] verts;
        private final int vertOffset;
        private final int[] colors;
        private final int colorOffset;
        private final Paint paint;

        public canvascall_DRAWBITMAPMESH(Bitmap bitmap, int meshWidth, int meshHeight, float[] verts, int vertOffset, int[] colors, int colorOffset, Paint paint) {
            this.bitmap = bitmap;
            this.meshWidth = meshWidth;
            this.meshHeight = meshHeight;
            this.verts = verts;
            this.vertOffset = vertOffset;
            this.colors = colors;
            this.colorOffset = colorOffset;
            this.paint = paint;
        }

        @Override
        public void run() {
            binding.drawBitmapMesh(bitmap, meshWidth, meshHeight, verts, vertOffset, colors, colorOffset, paint);
        }
    }

    public class canvascall_DRAWCIRCLE extends mini_sub {
        private final float cx;
        private final float cy;
        private final float radius;
        private final Paint paint;

        public canvascall_DRAWCIRCLE(float cx, float cy, float radius, Paint paint) {
            this.cx = cx;
            this.cy = cy;
            this.radius = radius;
            this.paint = paint;
        }

        @Override
        public void run() {
            binding.drawCircle(cx, cy, radius, paint);
        }
    }

    public class canvascall_DRAWCOLOR_i extends mini_sub {
        private final int color;

        public canvascall_DRAWCOLOR_i(int color) {
            this.color = color;
        }

        @Override
        public void run() {
            binding.drawColor(color);
        }
    }

    public class canvascall_DRAWCOLOR_ie extends mini_sub {
        private final int color;
        private final PorterDuff.Mode mode;

        public canvascall_DRAWCOLOR_ie(int color, PorterDuff.Mode mode) {
            this.color = color;
            this.mode = mode;
        }

        @Override
        public void run() {
            binding.drawColor(color, mode);
        }
    }

    public class canvascall_DRAWLINES extends mini_sub {
        private final float[] pts;
        private final Paint paint;

        public canvascall_DRAWLINES(float[] pts, Paint paint) {
            this.pts = pts;
            this.paint = paint;
        }

        @Override
        public void run() {
            binding.drawLines(pts, paint);
        }
    }

    public class canvascall_DRAWOVAL extends mini_sub {
        private final RectF oval;
        private final Paint paint;

        public canvascall_DRAWOVAL(RectF oval, Paint paint) {
            this.oval = oval;
            this.paint = paint;
        }

        @Override
        public void run() {
            binding.drawOval(oval, paint);
        }
    }

    public class canvascall_DRAWPATH extends mini_sub {
        private final Path path;
        private final Paint paint;

        public canvascall_DRAWPATH(Path path, Paint paint) {
            this.path = path;
            this.paint = paint;
        }

        @Override
        public void run() {
            binding.drawPath(path, paint);
        }
    }

    public class canvascall_DRAWPICTURE_pirf extends mini_sub {
        private final Picture picture;
        private final RectF dst;

        public canvascall_DRAWPICTURE_pirf(Picture picture, RectF dst) {
            this.picture = picture;
            this.dst = dst;
        }

        @Override
        public void run() {
            binding.drawPicture(picture, dst);
        }
    }

    public class canvascall_DRAWPICTURE_pi extends mini_sub {
        private final Picture picture;

        public canvascall_DRAWPICTURE_pi(Picture picture) {
            this.picture = picture;
        }

        @Override
        public void run() {
            binding.drawPicture(picture);
        }
    }

    public class canvascall_DRAWPOINTS extends mini_sub {
        private final float[] pts;
        private final Paint paint;

        public canvascall_DRAWPOINTS(float[] pts, Paint paint) {
            this.pts = pts;
            this.paint = paint;
        }

        @Override
        public void run() {
            binding.drawPoints(pts, paint);
        }
    }

    public class canvascall_DRAWRECT_ffff extends mini_sub {
        private final float left;
        private final float top;
        private final float right;
        private final float bottom;
        private final Paint paint;

        public canvascall_DRAWRECT_ffff(float left, float top, float right, float bottom, Paint paint) {
            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
            this.paint = paint;
        }

        @Override
        public void run() {
            binding.drawRect(left, top, right, bottom, paint);
        }
    }

    public class canvascall_DRAWRECT_rf extends mini_sub {
        private final RectF rect;
        private final Paint paint;

        public canvascall_DRAWRECT_rf(RectF rect, Paint paint) {
            this.rect = rect;
            this.paint = paint;
        }

        @Override
        public void run() {
            binding.drawRect(rect, paint);
        }
    }

    public class canvascall_DRAWROUNDRECT extends mini_sub {
        private final RectF rect;
        private final float rx;
        private final float ry;
        private final Paint paint;

        public canvascall_DRAWROUNDRECT(RectF rect, float rx, float ry, Paint paint) {
            this.rect = rect;
            this.rx = rx;
            this.ry = ry;
            this.paint = paint;
        }

        @Override
        public void run() {
            binding.drawRoundRect(rect, rx, ry, paint);
        }
    }

    public class canvascall_DRAWTEXT extends mini_sub {
        private final String text;
        private final float x;
        private final float y;
        private final Paint paint;

        public canvascall_DRAWTEXT(String text, float x, float y, Paint paint) {
            this.text = text;
            this.x = x;
            this.y = y;
            this.paint = paint;
        }

        @Override
        public void run() {
            binding.drawText(text, x, y, paint);
        }
    }

    public class canvascall_DRAWTEXTONPATH extends mini_sub {
        private final String text;
        private final Path path;
        private final float hOffset;
        private final float vOffset;
        private final Paint paint;

        public canvascall_DRAWTEXTONPATH(String text, Path path, float hOffset, float vOffset, Paint paint) {
            this.text = text;
            this.path = path;
            this.hOffset = hOffset;
            this.vOffset = vOffset;
            this.paint = paint;
        }

        @Override
        public void run() {
            binding.drawTextOnPath(text, path, hOffset, vOffset, paint);
        }
    }
}
