package Native.Machine;

import android.graphics.RectF;

/**
 * Created by james on 1/19/2016.
 */
class rfxMod extends dec_syntax_context<RectF> {
    public RF_Init rfinit = new RF_Init();
    public rfxMod(RectF[] regs) {
        super(regs);
    }

    @Override
    protected void onStart() {
        rfinit.set_RF(reg);
    }

    @Override
    protected void onStop() {
        rfinit.complete();
    }
}
