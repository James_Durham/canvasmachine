package Native.Machine;

import android.graphics.Paint;

/**
 * Created by james on 1/19/2016.
 */
class paxInitial extends dec_syntax_context<Paint> {
    public PA_Init painit = new PA_Init();

    public paxInitial(Paint[] regs) {
        super(regs);
    }

    @Override
    protected void onStart() {
        painit.set_PA(reg);
    }

    @Override
    protected void onStop() {
        painit.complete();
        painit.pa = null;
    }
}
