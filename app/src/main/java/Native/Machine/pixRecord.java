package Native.Machine;

import android.graphics.Picture;

/**
 * Created by james on 1/19/2016.
 */
class pixRecord extends dec_syntax_context<Picture> {

    public PI_Init piinit = new PI_Init();

    public pixRecord(Picture[] regs) {
        super(regs);
    }

    @Override
    protected void onStart() {
        A.assert_("pixRecord start: param one not an int",  params[0] instanceof Integer);
        A.assert_("pixRecord start: param two not an int",  params[1] instanceof Integer);
        piinit.set_PI(reg, (Integer) params[0], (Integer) params[1]);
    }

    @Override
    protected void onStop() {
        piinit.complete();
        piinit.canvas = null;
        piinit.pi = null;
        piinit.calls = null;
    }
}
