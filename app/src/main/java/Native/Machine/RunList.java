package Native.Machine;

import java.util.ArrayList;

/**
 * Created by james on 1/19/2016.
 */
public abstract class RunList {
    public ArrayList<Runnable> list = new ArrayList<Runnable>();

    public void add(Runnable item)
    {
        list.add(item);
    }
    public void runlist()
    {
        for (Runnable r : list)
        {
            r.run();
        }
        clear_list_option();
    }


    public abstract void clear_list_option();

    public abstract class mini_sub implements Runnable
    {
        void add_tolist()
        {
            list.add(this);
        }
    }

}
