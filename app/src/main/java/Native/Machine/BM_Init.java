package Native.Machine;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Created by james on 1/28/2016.
 */
public class BM_Init implements Complete<Bitmap>{
    public Bitmap bm;
    public canvascalls_ calls = new canvascalls_();
    public Canvas canvas;
    public void set_BM(Bitmap bm)
    {
        this.bm = bm;
        canvas = new Canvas(bm);
    }

    @Override
    public Bitmap complete() {
        calls.runlist(canvas);
        //clear the list here.
        calls.list.clear();
        return bm;
    }
}
