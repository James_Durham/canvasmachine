package Native.Machine;


import android.graphics.AvoidXfermode;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathDashPathEffect;
import android.graphics.PathEffect;
import android.graphics.Picture;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;


/**
 * Created by james on 1/13/2016.
 */
class ACB_imp implements ActionsCallBacks {

    private final int SUCCESS = 0;
    private final int RNA     = 1;
    private final int OTHER   = 2;

    public Throwable exception = null;
    public Machine mac_this;
    //parse context start
    pexSet       pexSet_context       ;
    ptxInitial   ptxInitial_context   ;
    pixRecord    pixRecord_context    ;
    bmxComposite bmxComposite_context ;
    rfxMod       rfxMod_context       ;
    paxInitial   paxInitial_context   ;
    canvascalls_ onDraw_runtime       ;
    private LinkedList<Object> acumulation = new LinkedList<Object>();
    //parse context end


    public ACB_imp(Machine mac_this) {
        this.mac_this = mac_this;
        pexSet_context = new pexSet(mac_this.PE_regs);
        ptxInitial_context = new ptxInitial(mac_this.PT_regs);
        pixRecord_context = new pixRecord(mac_this.PI_regs);
        bmxComposite_context = new bmxComposite(mac_this.BM_regs);
        rfxMod_context = new rfxMod(mac_this.RF_regs);
        paxInitial_context = new paxInitial(mac_this.PA_regs);
        onDraw_runtime = new canvascalls_();
    }

    @Override
    public int float_bin_cnst_Act(float a, float b) {
        try {
            acumulation.push(new FloatBinCnst(a, b));
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int float_tri_cnst_Act(float a, float b, float c) {
        try {
            acumulation.push(new FloatTriCnst(a, b, c));
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int float_quad_cnst_Act(float a, float b, float c, float d) {
        try {
            acumulation.push(new FloatQuadCnst(a, b, c, d));
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int float_list_empty_Act() {
        try {
            acumulation.push(new FloatList());
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int float_list_F_Act(float apnd) {
        try {
            FloatList fl = new FloatList();
            fl.add(apnd);
            acumulation.push(fl);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int float_list_FL_F_Act(float apnd) {
        try {
            FloatList fl = (FloatList) acumulation.pop();
            fl.add(apnd);
            acumulation.push(fl);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int float_bin_list_empty_Act() {
        try {
            acumulation.push(new FloatBinList());
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int float_bin_list_FBC_Act() {
        try {
            FloatBinCnst bin_cnst = (FloatBinCnst) acumulation.pop();
            FloatBinList fl = new FloatBinList();
            fl.addAll(Arrays.asList(bin_cnst.toArray()));
            acumulation.push(fl);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int float_bin_list_FBL_FBC_Act() {
        try {
            FloatBinCnst bin_cnst = (FloatBinCnst) acumulation.pop();
            FloatBinList fl = (FloatBinList) acumulation.pop();
            fl.addAll(Arrays.asList(bin_cnst.toArray()));
            acumulation.push(fl);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int float_quad_list_empty_Act() {
        try {
            acumulation.push(new FloatQuadList());
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int float_quad_list_FQC_Act() {
        try {
            FloatQuadCnst quad_cnst = (FloatQuadCnst) acumulation.pop();
            FloatQuadList fl = new FloatQuadList();
            fl.addAll(Arrays.asList(quad_cnst.toArray()));
            acumulation.push(fl);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int float_quad_list_FQL_FQC_Act() {
        try {
            FloatQuadCnst quad_cnst = (FloatQuadCnst) acumulation.pop();
            FloatQuadList fl = (FloatQuadList) acumulation.pop();
            fl.addAll(Arrays.asList(quad_cnst.toArray()));
            acumulation.push(fl);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int float_array_Act() {
        try {
            FloatList fl = (FloatList) acumulation.pop();
            FloatArray fa = new FloatArray(fl.size());
            fa.set(fl);
            acumulation.push(fa);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int float_bin_array_Act() {
        try {
            FloatBinList fl = (FloatBinList) acumulation.pop();
            FloatBinArray fba = new FloatBinArray(fl.size());
            fba.set(fl);
            acumulation.push(fba);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int float_quad_array_Act() {
        try {
            FloatQuadList fl = (FloatQuadList) acumulation.pop();
            FloatQuadArray fqa = new FloatQuadArray(fl.size());
            fqa.set(fl);
            acumulation.push(fqa);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int color_cnst_sv_color_string_sv_Act(int c) {
        try {
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int color_cnst_sv_color_hsv_sv_Act(int c) {
        try {
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int color_cnst_sv_color_argb_sv_Act(int c) {
        try {
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int color_cnst_list_empty_Act() {
        try {
            acumulation.push(new ColorCnstList());
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int color_cnst_list_CC_Act() {
        try {
            ColorCnstList cl = new ColorCnstList();
            cl.add((Integer) acumulation.pop());
            acumulation.push(cl);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int color_cnst_list_CCL_CC_Act() {
        try {
            int c = (Integer) acumulation.pop();
            ColorCnstList cl = (ColorCnstList) acumulation.pop();
            cl.add(c);
            acumulation.push(cl);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int color_cnst_array_Act() {
        try {
            ColorCnstList cl = (ColorCnstList) acumulation.pop();
            ColorCnstArray ca = new ColorCnstArray(cl.size());
            ca.set(cl);
            acumulation.push(ca);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pdm_enum_PDM_ADD_Act() {
        try {
            acumulation.push(PorterDuff.Mode.ADD);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pdm_enum_PDM_CLEAR_Act() {
        try {
            acumulation.push(PorterDuff.Mode.CLEAR);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pdm_enum_PDM_DARKEN_Act() {
        try {
            acumulation.push(PorterDuff.Mode.DARKEN);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pdm_enum_PDM_DST_Act() {
        try {
            acumulation.push(PorterDuff.Mode.DST);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pdm_enum_PDM_DST_ATOP_Act() {
        try {
            acumulation.push(PorterDuff.Mode.DST_ATOP);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pdm_enum_PDM_DST_IN_Act() {
        try {
            acumulation.push(PorterDuff.Mode.DST_IN);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pdm_enum_PDM_DST_OUT_Act() {
        try {
            acumulation.push(PorterDuff.Mode.DST_OUT);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pdm_enum_PDM_DST_OVER_Act() {
        try {
            acumulation.push(PorterDuff.Mode.DST_OVER);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pdm_enum_PDM_LIGHTEN_Act() {
        try {
            acumulation.push(PorterDuff.Mode.LIGHTEN);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pdm_enum_PDM_MULTIPLY_Act() {
        try {
            acumulation.push(PorterDuff.Mode.MULTIPLY);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pdm_enum_PDM_OVERLAY_Act() {
        try {
            acumulation.push(PorterDuff.Mode.OVERLAY);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pdm_enum_PDM_SCREEN_Act() {
        try {
            acumulation.push(PorterDuff.Mode.SCREEN);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pdm_enum_PDM_SRC_Act() {
        try {
            acumulation.push(PorterDuff.Mode.SRC);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pdm_enum_PDM_SRC_ATOP_Act() {
        try {
            acumulation.push(PorterDuff.Mode.SRC_ATOP);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pdm_enum_PDM_SRC_IN_Act() {
        try {
            acumulation.push(PorterDuff.Mode.SRC_IN);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pdm_enum_PDM_SRC_OUT_Act() {
        try {
            acumulation.push(PorterDuff.Mode.SRC_OUT);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pdm_enum_PDM_SRC_OVER_Act() {
        try {
            acumulation.push(PorterDuff.Mode.SRC_OVER);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pdm_enum_PDM_XOR_Act() {
        try {
            acumulation.push(PorterDuff.Mode.XOR);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    //Context marker pexSet start {
    @Override
    public int pe_element_PE_COMPOSE_Act() {
        try {
            pexSet_context.constructor._ComposePathEffect();
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pe_element_PE_CORNER_Act(float radius) {
        try {
            pexSet_context.constructor._CornerPathEffect(radius);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pe_element_PE_DASH_Act(float phase) {
        try {
            FloatBinArray fba = (FloatBinArray) acumulation.pop();
            pexSet_context.constructor._DashPathEffect(fba.vals, phase);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pe_element_PE_DISCRETE_Act(float segmentLength, float deviation) {
        try {
            pexSet_context.constructor._DiscretePathEffect(segmentLength, deviation);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pe_element_PE_PATHDASH_Act(int path, float advance, float phase) {
        try {
            PathDashPathEffect.Style style = (PathDashPathEffect.Style) acumulation.pop();
            Path p = reg_get(mac_this.PT_regs, path);
            if (p == null) return RNA;
            pexSet_context.constructor._PathDashPathEffect(p, advance, phase, style);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pe_element_PE_SUM_Act() {
        try {
            pexSet_context.constructor._SumPathEffect();
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }
    //} Context marker pexSet end

    @Override
    public int pathdash_style_enum_MORPH_Act() {
        try {
            acumulation.push(PathDashPathEffect.Style.MORPH);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pathdash_style_enum_ROTATE_Act() {
        try {
            acumulation.push(PathDashPathEffect.Style.ROTATE);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pathdash_style_enum_TRANSLATE_Act() {
        try {
            acumulation.push(PathDashPathEffect.Style.TRANSLATE);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    //Context marker paxInitial start {
    @Override
    public int shader_BITMAP_Act(int bitmap) {
        try {
            Shader.TileMode tiley = (Shader.TileMode) acumulation.pop();
            Shader.TileMode tilex = (Shader.TileMode) acumulation.pop();
            Bitmap b = reg_get(mac_this.BM_regs, bitmap);
            if (b == null) return RNA;
            paxInitial_context.painit.sh_acum._BitmapShader(b, tilex, tiley);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int shader_COMPOSE_Act() {
        try {
            paxInitial_context.painit.sh_acum._ComposeShader((PorterDuff.Mode) acumulation.pop());
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int shader_LINEAR_Act(float x0, float y0, float x1, float y1) {
        try {
            Shader.TileMode tile = (Shader.TileMode) acumulation.pop();
            float[] positions = ((FloatArray) acumulation.pop()).vals;
            int[] colors = ((ColorCnstArray) acumulation.pop()).vals;
            paxInitial_context.painit.sh_acum._LinearGradient(x0, y0, x1, y1, colors, positions, tile);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int shader_RADIAL_Act(float centerX, float centerY, float radius) {
        try {
            Shader.TileMode tileMode = (Shader.TileMode) acumulation.pop();
            float[] stops = ((FloatArray) acumulation.pop()).vals;
            int[] colors = ((ColorCnstArray) acumulation.pop()).vals;
            paxInitial_context.painit.sh_acum._RadialGradient(centerX, centerY, radius, colors, stops, tileMode);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int shader_SWEEP_Act(float cx, float cy) {
        try {
            float[] positions = ((FloatArray) acumulation.pop()).vals;
            int[] colors = ((ColorCnstArray) acumulation.pop()).vals;
            paxInitial_context.painit.sh_acum._SweepGradient(cx, cy, colors, positions);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }
    //} Context marker paxInitial end

    @Override
    public int shader_tile_CLAMP_Act() {
        try {
            acumulation.push(Shader.TileMode.CLAMP);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int shader_tile_MIRROR_Act() {
        try {
            acumulation.push(Shader.TileMode.MIRROR);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int shader_tile_REPEAT_Act() {
        try {
            acumulation.push(Shader.TileMode.REPEAT);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    //Context marker paxInitial start {
    @Override
    public int xfermode_Avoid_Act(int opColor, int tolerance) {
        try {
            paxInitial_context.painit.set_xfermode.addAvoidXfermode(opColor, tolerance,
                    (AvoidXfermode.Mode) acumulation.pop());
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int xfermode_PixelXor_Act(int opColor) {
        try {
            paxInitial_context.painit.set_xfermode.addPixelXorXfermode(opColor);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int xfermode_PorterDuff_Act() {
        try {
            paxInitial_context.painit.set_xfermode.addPorterDuffXfermode(
                    (PorterDuff.Mode) acumulation.pop()
            );
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }
    //} Context marker paxInitial end

    @Override
    public int xf_avoid_enum_AVOID_Act() {
        try {
            acumulation.push(AvoidXfermode.Mode.AVOID);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int xf_avoid_enum_TARGET_Act() {
        try {
            acumulation.push(AvoidXfermode.Mode.TARGET);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    //Context marker paxInitial start {
    @Override
    public int maskfilter_BLUR_Act(float radius) {
        try {
            paxInitial_context.painit.set_maskfilter.addBlurMaskFilter(radius,
                    (BlurMaskFilter.Blur) acumulation.pop());
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int maskfilter_EMBOSS_Act(float ambient, float specular, float blurRadius) {
        try {
            float[] direction = ((FloatTriCnst) acumulation.pop()).toArray();
            paxInitial_context.painit.set_maskfilter.addEmbossMaskFilter(direction, ambient, specular, blurRadius);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }
    //} Context marker paxInitial end

    @Override
    public int mf_blur_enum_INNER_Act() {
        try {
            acumulation.push(BlurMaskFilter.Blur.INNER);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int mf_blur_enum_NORMAL_Act() {
        try {
            acumulation.push(BlurMaskFilter.Blur.NORMAL);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int mf_blur_enum_OUTER_Act() {
        try {
            acumulation.push(BlurMaskFilter.Blur.OUTER);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int mf_blur_enum_SOLID_Act() {
        try {
            acumulation.push(BlurMaskFilter.Blur.SOLID);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    //Context marker paxInitial start {
    @Override
    public int colorfilter_ColorMatrix_Act() {
        try {
            ColorMatrix cm = (ColorMatrix) acumulation.pop();
            paxInitial_context.painit.set_colorfilter.addColorMatrixColorFilter(cm);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int colorfilter_Lighting_Act(int mul, int add) {
        try {
            paxInitial_context.painit.set_colorfilter.addLightingColorFilter(mul, add);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int colorfilter_PorterDuff_Act(int color) {
        try {
            paxInitial_context.painit.set_colorfilter.addPorterDuffColorFilter(color,
                    (PorterDuff.Mode) acumulation.pop());
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }
    // still inside paxInitial???
    @Override
    public int colormatrix_Scale_Act(float rScale, float gScale, float bScale, float aScale) {
        try {
            acumulation.push(paxInitial_context.painit.set_colorfilter.cm._Scale(rScale, gScale, bScale, aScale));
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }
    // still inside paxInitial???
    @Override
    public int colormatrix_Saturation_Act(float sat) {
        try {
            acumulation.push(paxInitial_context.painit.set_colorfilter.cm._Saturation(sat));
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }
    // still inside paxInitial???
    @Override
    public int colormatrix_YUV2RGB_Act() {
        try {
            acumulation.push(paxInitial_context.painit.set_colorfilter.cm._YUV2RGB());
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }
    // still inside paxInitial???
    @Override
    public int colormatrix_RGB2YUV_Act() {
        try {
            acumulation.push(paxInitial_context.painit.set_colorfilter.cm._RGB2YUV());
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }
    //} Context marker paxInitial end

    @Override
    public int canvascall_CLIPPATH_Act(int path) {
        try {
            Path p = reg_get(mac_this.PT_regs, path);
            if (p == null) return RNA;
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_CLIPPATH(p);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_CLIPRECT_ffff_Act(float left, float top, float right, float bottom) {
        try {
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_CLIPRECT_ffff(left, top, right, bottom);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_CLIPRECT_rf_Act(int rect) {
        try {
            RectF r = reg_get(mac_this.RF_regs, rect);
            if (r == null) return RNA;
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_CLIPRECT_rf(r);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_SAVE_Act() {

        try {
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_SAVE();
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_RESTORE_Act() {
        try {
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_RESTORE();
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_ROTATE_f_Act(float degrees) {
        try {
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_ROTATE_f(degrees);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_ROTATE_fff_Act(float degrees, float px, float py) {
        try {
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_ROTATE_fff(degrees, px, py);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_SCALE_ff_Act(float sx, float sy) {
        try {
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_SCALE_ff(sx, sy);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_SCALE_ffff_Act(float sx, float sy, float px, float py) {
        try {
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_SCALE_ffff(sx, sy, px, py);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_SKEW_Act(float sx, float sy) {
        try {
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_SKEW(sx, sy);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_TRANSLATE_Act(float dx, float dy) {
        try {
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_TRANSLATE(dx, dy);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_DRAWARC_Act(int oval, float startAngle, float sweepAngle, int useCenter, int paint) {
        try {
            Paint p = reg_get(mac_this.PA_regs, paint);
            RectF o = reg_get(mac_this.RF_regs, oval);
            if (p == null) return RNA;
            if (o == null) return RNA;
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_DRAWARC(o, startAngle, sweepAngle, useCenter == 1, p);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_DRAWBITMAP_Act(int bitmap, float left, float top, int paint) {
        try {
            Paint p = reg_get(mac_this.PA_regs, paint);
            Bitmap b = reg_get(mac_this.BM_regs, bitmap);
            if (p == null) return RNA;
            if (b == null) return RNA;
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_DRAWBITMAP(b, left, top, p);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_DRAWBITMAPMESH_Act(int bitmap, int meshWidth, int meshHeight, int vertOffset, int colorOffset, int paint) {
        try {
            Paint p = reg_get(mac_this.PA_regs, paint);
            Bitmap b = reg_get(mac_this.BM_regs, bitmap);
            if (p == null) return RNA;
            if (b == null) return RNA;
            int [] colors_ = ((ColorCnstArray)acumulation.pop()).vals;
            float [] verts = ((FloatArray)acumulation.pop()).vals;
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_DRAWBITMAPMESH(b, meshWidth, meshHeight, verts, vertOffset, colors_, colorOffset, p);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_DRAWCIRCLE_Act(float cx, float cy, float radius, int paint) {
        try {
            Paint p = reg_get(mac_this.PA_regs, paint);
            if (p == null) return RNA;
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_DRAWCIRCLE(cx, cy, radius, p);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_DRAWCOLOR_i_Act(int color) {
        try {
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_DRAWCOLOR_i(color);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_DRAWCOLOR_ie_Act(int color) {
        try {
            PorterDuff.Mode mode = (PorterDuff.Mode) acumulation.pop();
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_DRAWCOLOR_ie(color, mode);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_DRAWLINES_Act(int paint) {
        try {
            Paint p = reg_get(mac_this.PA_regs, paint);
            if (p == null) return RNA;
            float [] pts = ((FloatQuadArray) acumulation.pop()).vals;
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_DRAWLINES(pts, p);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_DRAWOVAL_Act(int oval, int paint) {
        try {
            Paint p = reg_get(mac_this.PA_regs, paint);
            RectF o = reg_get(mac_this.RF_regs, oval);
            if (p == null) return RNA;
            if (o == null) return RNA;
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_DRAWOVAL(o, p);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_DRAWPATH_Act(int path, int paint) {
        try {
            Paint p = reg_get(mac_this.PA_regs, paint);
            Path pt = reg_get(mac_this.PT_regs, path);
            if (p == null) return RNA;
            if (pt == null) return RNA;
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_DRAWPATH(pt, p);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_DRAWPICTURE_pirf_Act(int picture, int dst) {
        try {
            Picture pi = reg_get(mac_this.PI_regs, picture);
            RectF dst_ = reg_get(mac_this.RF_regs, dst);
            if (pi == null) return RNA;
            if (dst_ == null) return RNA;
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_DRAWPICTURE_pirf(pi, dst_);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_DRAWPICTURE_pi_Act(int picture) {
        try {
            Picture pi = reg_get(mac_this.PI_regs, picture);
            if (pi == null) return RNA;
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_DRAWPICTURE_pi(pi);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_DRAWPOINTS_Act(int paint) {
        try {
            Paint p = reg_get(mac_this.PA_regs, paint);
            if (p == null) return RNA;
            float [] pts = ((FloatBinArray)acumulation.pop()).vals;
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_DRAWPOINTS(pts, p);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_DRAWRECT_ffff_Act(float left, float top, float right, float bottom, int paint) {
        try {
            Paint p = reg_get(mac_this.PA_regs, paint);
            if (p == null) return RNA;
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_DRAWRECT_ffff(left, top, right, bottom, p);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_DRAWRECT_rf_Act(int rect, int paint) {
        try {
            Paint p = reg_get(mac_this.PA_regs, paint);
            RectF r = reg_get(mac_this.RF_regs, rect);
            if (p == null) return RNA;
            if (r == null) return RNA;
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_DRAWRECT_rf(r, p);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_DRAWROUNDRECT_Act(int rect, float rx, float ry, int paint) {
        try {
            Paint p = reg_get(mac_this.PA_regs, paint);
            RectF r = reg_get(mac_this.RF_regs, rect);
            if (p == null) return RNA;
            if (r == null) return RNA;
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_DRAWROUNDRECT(r, rx, ry, p);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_DRAWTEXT_Act(String text, float x, float y, int paint) {
        try {
            Paint p = reg_get(mac_this.PA_regs, paint);
            if (p == null) return RNA;
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_DRAWTEXT(text, x, y, p);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int canvascall_DRAWTEXTONPATH_Act(String text, int path, float hOffset, float vOffset, int paint) {
        try {
            Paint p = reg_get(mac_this.PA_regs, paint);
            Path pt = reg_get(mac_this.PT_regs, path);
            if (p == null) return RNA;
            if (pt == null) return RNA;
            canvascalls_ c = (canvascalls_) acumulation.pop();
            c.make_canvascall_DRAWTEXTONPATH(text, pt, hOffset, vOffset, p);
            acumulation.push(c);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    //Context marker ptxInitial start {
    @Override
    public int pt_initcall_FILLTYPE_Act() {
        try {
            Path.FillType ft = (Path.FillType) acumulation.pop();
            ptxInitial_context.ptinit.calls.make_PT_INIT_C_FILLTYPE(ft);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pt_initcall_CLOSE_Act() {
        try {
            ptxInitial_context.ptinit.calls.make_PT_INIT_C_CLOSE();
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pt_initcall_CUBICTO_Act(float x1, float y1, float x2, float y2, float x3, float y3) {
        try {
            ptxInitial_context.ptinit.calls.make_PT_INIT_C_CUBICTO(x1, y1, x2, y2, x3, y3);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pt_initcall_RCUBICTO_Act(float x1, float y1, float x2, float y2, float x3, float y3) {
        try {
            ptxInitial_context.ptinit.calls.make_PT_INIT_C_RCUBICTO(x1, y1, x2, y2, x3, y3);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pt_initcall_LINETO_Act(float x, float y) {
        try {
            ptxInitial_context.ptinit.calls.make_PT_INIT_C_LINETO(x, y);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pt_initcall_RLINETO_Act(float dx, float dy) {
        try {
            ptxInitial_context.ptinit.calls.make_PT_INIT_C_RLINETO(dx, dy);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pt_initcall_MOVETO_Act(float x, float y) {
        try {
            ptxInitial_context.ptinit.calls.make_PT_INIT_C_MOVETO(x, y);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pt_initcall_RMOVETO_Act(float dx, float dy) {
        try {
            ptxInitial_context.ptinit.calls.make_PT_INIT_C_RMOVETO(dx, dy);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pt_initcall_QUADTO_Act(float x1, float y1, float x2, float y2) {
        try {
            ptxInitial_context.ptinit.calls.make_PT_INIT_C_QUADTO(x1, y1, x2, y2);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pt_initcall_RQUADTO_Act(float dx1, float dy1, float dx2, float dy2) {
        try {
            ptxInitial_context.ptinit.calls.make_PT_INIT_C_RQUADTO(dx1, dy1, dx2, dy2);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pt_initcall_TRANSFORM_Act(float[] mat) {
        try {
            Matrix m = new Matrix();
            m.setValues(mat);
            ptxInitial_context.ptinit.calls.make_PT_INIT_C_TRANSFORM(m);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }
    //} Context marker ptxInitial end

    @Override
    public int filltype_enum_EVEN_ODD_Act() {
        try {
            acumulation.push(Path.FillType.EVEN_ODD);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int filltype_enum_INVERSE_EVEN_ODD_Act() {
        try {
            acumulation.push(Path.FillType.INVERSE_EVEN_ODD);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int filltype_enum_INVERSE_WINDING_Act() {
        try {
            acumulation.push(Path.FillType.INVERSE_WINDING);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int filltype_enum_WINDING_Act() {
        try {
            acumulation.push(Path.FillType.WINDING);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    //Context marker rfxMod start {
    @Override
    public int rf_modcall_INSET_Act(float dx, float dy) {
        try {
            rfxMod_context.rfinit.calls.make_RF_MOD_C_INSET(dx, dy);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int rf_modcall_INTERSECT_Act(float left, float top, float right, float bottom) {
        try {
            rfxMod_context.rfinit.calls.make_RF_MOD_C_INTERSECT(left, top, right, bottom);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int rf_modcall_OFFSET_Act(float dx, float dy) {
        try {
            rfxMod_context.rfinit.calls.make_RF_MOD_C_OFFSET(dx, dy);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int rf_modcall_SORT_Act() {
        try {
            rfxMod_context.rfinit.calls.make_RF_MOD_C_SORT();
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int rf_modcall_UNION_ffff_Act(float left, float top, float right, float bottom) {
        try {
            rfxMod_context.rfinit.calls.make_RF_MOD_C_UNION_ffff(left, top, right, bottom);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int rf_modcall_UNION_ff_Act(float x, float y) {
        try {
            rfxMod_context.rfinit.calls.make_RF_MOD_C_UNION_ff(x, y);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }
    //} Context marker rfxMod end

    //Context marker paxInitial start {
    @Override
    public int pa_fntcall_HINTING_Act(int bool) {
        try {
            paxInitial_context.painit.fnt_cls.make_PA_FNT_C_HINTING(bool);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pa_fntcall_TEXTALIGN_Act() {
        try {
            Paint.Align align = (Paint.Align) acumulation.pop();
            paxInitial_context.painit.fnt_cls.make_PA_FNT_C_ALIGN(align);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pa_fntcall_TEXTSCALEX_Act(float scaleX) {
        try {
            paxInitial_context.painit.fnt_cls.make_PA_FNT_C_SCALEX(scaleX);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pa_fntcall_TEXTSIZE_Act(float textSize) {
        try {
            paxInitial_context.painit.fnt_cls.make_PA_FNT_C_SIZE(textSize);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pa_fntcall_TEXTSKEWX_Act(float skewX) {
        try {
            paxInitial_context.painit.fnt_cls.make_PA_FNT_C_SKEWX(skewX);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pa_fntcall_SETTYPEFACE_Act() {
        try {
            Typeface typeface = (Typeface) acumulation.pop();
            paxInitial_context.painit.fnt_cls.make_PA_FNT_C_TYPEFACE(typeface);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }
    //} Context marker paxInitial end

    @Override
    public int pa_align_enum_CENTER_Act() {
        try {
            acumulation.push(Paint.Align.CENTER);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pa_align_enum_LEFT_Act() {
        try {
            acumulation.push(Paint.Align.LEFT);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pa_align_enum_RIGHT_Act() {
        try {
            acumulation.push(Paint.Align.RIGHT);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int typeface_DEFAULT_Act() {
        try {
            acumulation.push(Typeface.DEFAULT);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int typeface_DEFAULT_BOLD_Act() {
        try {
            acumulation.push(Typeface.DEFAULT_BOLD);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int typeface_MONOSPACE_Act() {
        try {
            acumulation.push(Typeface.MONOSPACE);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int typeface_SANS_SERIF_Act() {
        try {
            acumulation.push(Typeface.SANS_SERIF);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int typeface_SERIF_Act() {
        try {
            acumulation.push(Typeface.SERIF);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    //Context marker paxInitial start {
    @Override
    public int pa_strkcall_STROKECAP_Act() {
        try {
            Paint.Cap cap = (Paint.Cap) acumulation.pop();
            paxInitial_context.painit.ps_cls.make_PA_STRK_C_CAP(cap);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pa_strkcall_STROKEJOIN_Act() {
        try {
            Paint.Join join = (Paint.Join) acumulation.pop();
            paxInitial_context.painit.ps_cls.make_PA_STRK_C_JOIN(join);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pa_strkcall_STROKEMITER_Act(float miter) {
        try {
            paxInitial_context.painit.ps_cls.make_PA_STRK_C_MITER(miter);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pa_strkcall_STROKEWIDTH_Act(float width) {
        try {
            paxInitial_context.painit.ps_cls.make_PA_STRK_C_WIDTH(width);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pa_strkcall_PATHEFFECT_Act(int effect) {
        try {
            PathEffect pa = reg_get(mac_this.PE_regs, effect);
            if (pa == null) return RNA;
            paxInitial_context.painit.ps_cls.make_PA_STRK_C_PATHEFFECT(pa);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }
    //} Context marker paxInitial end

    @Override
    public int cap_enum_BUTT_Act() {
        try {
            acumulation.push(Paint.Cap.BUTT);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int cap_enum_ROUND_Act() {
        try {
            acumulation.push(Paint.Cap.ROUND);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int cap_enum_SQUARE_Act() {
        try {
            acumulation.push(Paint.Cap.SQUARE);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int join_enum_BEVEL_Act() {
        try {
            acumulation.push(Paint.Join.BEVEL);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int join_enum_MITER_Act() {
        try {
            acumulation.push(Paint.Join.MITER);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int join_enum_ROUND_Act() {
        try {
            acumulation.push(Paint.Join.ROUND);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pa_fnt_init_Act() {

        return SUCCESS;
    }

    @Override
    public int pa_clr_init_Act(int color) {
        try {
            paxInitial_context.painit.clr_set(color);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pa_pathmode_init_Act() {
        try {
            Paint.Style mode = (Paint.Style) acumulation.pop();
            paxInitial_context.painit.style_set(mode);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pa_stroke_init_Act() {

        return SUCCESS;
    }

    @Override
    public int pa_fill_init_Act() {

        return SUCCESS;
    }

    @Override
    public int pa_cmstmd_init_Act() {

        return SUCCESS;
    }

    @Override
    public int pa_mf_init_Act() {

        return SUCCESS;
    }

    @Override
    public int pa_cf_init_Act() {

        return SUCCESS;
    }

    @Override
    public int pa_style_enum_FILL_Act() {
        try {
            acumulation.push(Paint.Style.FILL);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pa_style_enum_FILL_AND_STROKE_Act() {
        try {
            acumulation.push(Paint.Style.FILL_AND_STROKE);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pa_style_enum_STROKE_Act() {
        try {
            acumulation.push(Paint.Style.STROKE);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pexSet_midAct(int effect) {
        try {
            pexSet_context.start(effect);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pexSet_Act() {
        try {
            pexSet_context.stop();
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int ptxInitial_midAct(int path) {
        try {
            ptxInitial_context.start(path);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int ptxInitial_Act() {
        ptxInitial_context.stop();
        return SUCCESS;
    }

    @Override
    public int pixRecord_midAct(int picture, int width, int height) {
        try {
            pixRecord_context.start(picture, width, height);
            canvascalls_ cc = pixRecord_context.piinit.calls;
            acumulation.push(cc);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int pixRecord_Act() {
        try {
            pixRecord_context.stop();
            acumulation.pop();
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int bmxComposite_midAct(int bitmap) {
        try {
            bmxComposite_context.start(bitmap);
            canvascalls_ cc = bmxComposite_context.bminit.calls;
            acumulation.push(cc);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int bmxComposite_Act() {
        try {
            bmxComposite_context.stop();
            acumulation.pop();
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int rfxMod_midAct(int rect) {
        try {
            rfxMod_context.start(rect);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int rfxMod_Act() {
        try {
            rfxMod_context.stop();
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int paxInitial_midAct(int paint) {
        try {
            paxInitial_context.start(paint);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int paxInitial_Act() {
        try {
            paxInitial_context.stop();
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int alloc_PE_ref_sv_Act(int effect) {
        try {
            mac_this.PE_regs[effect] = new PathEffect(); //marker
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int alloc_PT_ref_sv_Act(int path) {
        try {
            mac_this.PT_regs[path] = new Path();
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int alloc_PI_ref_sv_Act(int picture) {
        try {
            mac_this.PI_regs[picture] = new Picture();
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int alloc_BM_ref_sv_Act(int bitmap, int width, int height) {
        try {
            mac_this.BM_regs[bitmap] = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int alloc_PA_ref_sv_Act(int paint, int flags) {
        try {
            mac_this.PA_regs[paint] = new Paint(flags);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int alloc_RF_ref_sv_Act(int rect, float left, float top, float right, float bottom) {
        try {
            mac_this.RF_regs[rect] = new RectF(left, top, right, bottom);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int declarations_midAct() {

        return SUCCESS;
    }

    @Override
    public int declarations_Act() {

        return SUCCESS;
    }

    @Override
    public int onDraw_midAct() {
        try {
            acumulation.push(onDraw_runtime);
        } catch (Throwable t) {
            exception = t; return OTHER;
        }
        return SUCCESS;
    }

    @Override
    public int onDraw_Act() {
        acumulation.pop();
        return SUCCESS;
    }

    @Override
    public int start() {

        return SUCCESS;
    }

    @Override
    public int end() {

        return SUCCESS;
    }


    // utils
    public String register_not_allocated = null;
    private <T> T reg_get(T[] regs, int index)
    {
        T ret = regs[index];
        // maybe wrap regs in container to contain regs and the reg class
        register_not_allocated = A.reg_test(regs.getClass().getComponentType(), regs, index);
        return (register_not_allocated == null) ? ret  : null;
    }

    // marking types
    private static class FloatBinCnst {
        public Float a;
        public Float b;

        public FloatBinCnst(float a, float b) {
            this.a = a;
            this.b = b;
        }

        public Float[] toArray() {
            return new Float[]{a, b};
        }
    }

    private static class FloatTriCnst {
        public Float a;
        public Float b;
        public Float c;

        public FloatTriCnst(float a, float b, float c) {
            this.a = a;
            this.b = b;
            this.c = c;

        }

        public float[] toArray() {
            return new float[]{a, b, c};
        }
    }

    private static class FloatQuadCnst {
        public Float a;
        public Float b;
        public Float c;
        public Float d;

        public FloatQuadCnst(float a, float b, float c, float d) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }

        public Float[] toArray() {
            return new Float[]{a, b, c, d};
        }
    }

    private static class FloatList extends LinkedList<Float> {

    }

    private static class FloatBinList extends LinkedList<Float> {

    }

    private static class FloatQuadList extends LinkedList<Float> {

    }

    private static abstract class HAS_FLOAT_PRIM_ARRAY {
        public float[] vals;

        public HAS_FLOAT_PRIM_ARRAY(int size) {
            vals = new float[size];
        }

        public void set(Iterable<Float> source) {
            int pos = 0;
            for (float v : source) {
                vals[pos] = v;
                pos++;
            }
        }
    }

    private static class FloatArray extends HAS_FLOAT_PRIM_ARRAY {
        public FloatArray(int size) {
            super(size);
        }
    }

    private static class FloatBinArray extends HAS_FLOAT_PRIM_ARRAY {
        public FloatBinArray(int capacity) {
            super(capacity);
        }
    }

    private static class FloatQuadArray extends HAS_FLOAT_PRIM_ARRAY {
        public FloatQuadArray(int capacity) {
            super(capacity);
        }
    }

    private static abstract class HAS_INT_PRIM_ARRAY {
        public int[] vals;

        public HAS_INT_PRIM_ARRAY(int size) {
            vals = new int[size];
        }

        public void set(Iterable<Integer> source) {
            int pos = 0;
            for (int v : source) {
                vals[pos] = v;
                pos++;
            }
        }
    }

    private static class ColorCnstList extends LinkedList<Integer> {

    }

    private static class ColorCnstArray extends HAS_INT_PRIM_ARRAY {
        public ColorCnstArray(int capacity) {
            super(capacity);
        }
    }
}

