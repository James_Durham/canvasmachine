package Native.Machine;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;

/**
 * Created by james on 1/19/2016.
 */
class floats {
    LinkedList<Object> container = new LinkedList<Object>();

    void float_bin_cnst(Stack<Object> call_stack, float a, float b) {
        call_stack.push(new float[]{a, b});
    }

    void float_tri_cnst(Stack<Object> call_stack, float a, float b, float c) {
        call_stack.push(new float[]{a, b, c});
    }

    void float_quad_cnst(Stack<Object> call_stack, float a, float b, float c, float d) {
        call_stack.push(new float[]{a, b, c, d});
    }

    void float_list_start() {/*do nothing*/}

    void float_list_add(float a) {
        container.add(a);
    }

    void float_list_end(Stack<Object> call_stack) {
        float ret[] = new float[container.size()];
        int i = 0;
        Iterator it = container.iterator();
        while (it.hasNext()) {
            ret[i] = (Float) it.next();
            i++;
        }

        container.clear();
        call_stack.push(ret);
    }

    void float_bin_list_start() {/*do nothing*/}

    void float_bin_list_add(Stack<Object> call_stack) {
        float[] tmp = (float[]) call_stack.pop();
        container.add(tmp);
    }

    void float_bin_list_end(Stack<Object> call_stack) {
        float ret[] = new float[container.size() * 2];
        int i = 0;
        Iterator it = container.iterator();
        float tmp[];
        while (it.hasNext()) {
            tmp = (float[]) it.next();
            ret[i] = tmp[0];
            ret[i + 1] = tmp[1];
            i += 2;
        }
        container.clear();
        call_stack.push(ret);
    }

    void float_quad_list_start() {/*do nothing*/}

    void float_quad_list_add(Stack<Object> call_stack) {
        float[] tmp = (float[]) call_stack.pop();
        container.add(tmp);
    }

    void float_quad_list_end(Stack<Object> call_stack) {
        float ret[] = new float[container.size() * 4];
        int i = 0;
        Iterator it = container.iterator();
        float tmp[];
        while (it.hasNext()) {
            tmp = (float[]) it.next();
            ret[i] = tmp[0];
            ret[i + 1] = tmp[1];
            ret[i + 2] = tmp[2];
            ret[i + 3] = tmp[3];
            i += 4;
        }
        container.clear();
        call_stack.push(ret);
    }

    //THESE GUYS AREN'T BEING IMPLEMENTED
    //float_array:      FA  SQ_L float_list      SQ_R { float_array_Act(); };
    //float_bin_array:  FBA SQ_L float_bin_list  SQ_R { float_bin_array_Act(); };
    //float_quad_array: FQA SQ_L float_quad_list SQ_R { float_quad_array_Act(); };

}
