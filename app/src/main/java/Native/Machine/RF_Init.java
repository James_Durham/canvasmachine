package Native.Machine;

import android.graphics.RectF;

/**
 * Created by james on 1/26/2016.
 */
public class RF_Init implements Complete<RectF> {
    RectF rf;
    rect_calls calls = new rect_calls();

    public void set_RF(RectF rf) {
        this.rf = rf;
    }

    @Override
    public RectF complete() {
        calls.runlist();
        return rf;
    }

    public class rect_calls extends RunList {

        @Override
        public void clear_list_option() {
            list.clear();
        }

        public void make_RF_MOD_C_INSET(float dx, float dy)
        {(new RF_MOD_C_INSET(dx, dy)).add_tolist();}
        public void make_RF_MOD_C_INTERSECT(float left, float top, float right, float bottom)
        {(new RF_MOD_C_INTERSECT(left, top, right, bottom)).add_tolist();}
        public void make_RF_MOD_C_OFFSET(float dx, float dy)
        {(new RF_MOD_C_OFFSET(dx, dy)).add_tolist();}
        public void make_RF_MOD_C_SORT()
        {(new RF_MOD_C_SORT()).add_tolist();}
        public void make_RF_MOD_C_UNION_ffff(float left, float top, float right, float bottom)
        {(new RF_MOD_C_UNION_ffff(left, top, right, bottom)).add_tolist();}
        public void make_RF_MOD_C_UNION_ff(float x, float y)
        {(new RF_MOD_C_UNION_ff(x, y)).add_tolist();}

        public class RF_MOD_C_INSET extends mini_sub {
            private final float dx;
            private final float dy;


            public RF_MOD_C_INSET(float dx, float dy) {
                this.dx = dx;
                this.dy = dy;

            }

            @Override
            public void run() {
                rf.inset(dx, dy);
            }


        }

        public class RF_MOD_C_INTERSECT extends mini_sub {
            private final float left;
            private final float top;
            private final float right;
            private final float bottom;


            public RF_MOD_C_INTERSECT(float left, float top, float right, float bottom) {
                this.left = left;
                this.top = top;
                this.right = right;
                this.bottom = bottom;

            }

            @Override
            public void run() {
                rf.intersect(left, top, right, bottom);
            }


        }

        public class RF_MOD_C_OFFSET extends mini_sub {
            private final float dx;
            private final float dy;


            public RF_MOD_C_OFFSET(float dx, float dy) {
                this.dx = dx;
                this.dy = dy;
            }

            @Override
            public void run() {
                rf.offset(dx, dy);
            }

        }

        public class RF_MOD_C_SORT extends mini_sub {


            public RF_MOD_C_SORT() {

            }

            @Override
            public void run() {
                rf.sort();
            }

        }

        public class RF_MOD_C_UNION_ffff extends mini_sub {
            private final float left;
            private final float top;
            private final float right;
            private final float bottom;


            public RF_MOD_C_UNION_ffff(float left, float top, float right, float bottom) {
                this.left = left;
                this.top = top;
                this.right = right;
                this.bottom = bottom;
            }

            @Override
            public void run() {
                rf.union(left, top, right, bottom);
            }

        }

        public class RF_MOD_C_UNION_ff extends mini_sub {
            private final float x;
            private final float y;


            public RF_MOD_C_UNION_ff(float x, float y) {
                this.x = x;
                this.y = y;

            }

            @Override
            public void run() {
                rf.union(x, y);
            }

        }

    }

}
