package Native.Machine;

import android.graphics.ComposePathEffect;
import android.graphics.CornerPathEffect;
import android.graphics.DashPathEffect;
import android.graphics.DiscretePathEffect;
import android.graphics.Path;
import android.graphics.PathDashPathEffect;
import android.graphics.PathEffect;
import android.graphics.SumPathEffect;

import java.util.LinkedList;

/**
 * Created by james on 1/20/2016.
 */
class PE_Construct extends _acum<PathEffect> {

    public void _ComposePathEffect() {
        PathEffect innerpe = pop();
        PathEffect outerpe = pop();
        push(new ComposePathEffect(outerpe, innerpe));
    }

    public void _CornerPathEffect(float radius) {
        push(new CornerPathEffect(radius));
    }

    public void _DashPathEffect(float[] intervals, float phase) {
        push(new DashPathEffect(intervals, phase));
    }

    public void _DiscretePathEffect(float segmentLength, float deviation) {
        push(new DiscretePathEffect(segmentLength, deviation));
    }

    public void _PathDashPathEffect(Path shape, float advance, float phase, PathDashPathEffect.Style style) {
        push(new PathDashPathEffect(shape, advance, phase, style));
    }

    public void _SumPathEffect() {
        PathEffect second = pop();
        PathEffect first = pop();
        push(new SumPathEffect(first, second));
    }

}
