package Native.Machine;

import android.graphics.PathEffect;

/**
 * Created by james on 1/19/2016.
 */
class pexSet extends dec_syntax_context<PathEffect> {
    public PE_Construct constructor = new PE_Construct();

    public pexSet(PathEffect[] regs) {
        super(regs);
    }

    @Override
    protected void onStart() {
        constructor.sanity_check();
    }

    @Override
    protected void onStop() {
        reg = constructor.getresult();
        regs[reg_index] = reg;
    }
}
