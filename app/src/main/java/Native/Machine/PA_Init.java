package Native.Machine;

import android.graphics.AvoidXfermode;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.BlurMaskFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.ComposeShader;
import android.graphics.EmbossMaskFilter;
import android.graphics.LightingColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PathEffect;
import android.graphics.PixelXorXfermode;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuffXfermode;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.graphics.Typeface;

/**
 * Created by james on 1/21/2016.
 */
class PA_Init implements Complete<Paint> {
    Paint pa;

    fnt_calls fnt_cls = new fnt_calls();
    paint_stroke_calls ps_cls = new paint_stroke_calls();
    shader_accum sh_acum = new shader_accum();
    xfermode_add set_xfermode = new xfermode_add();
    maskfilter_add set_maskfilter = new maskfilter_add();
    colorfilter_add set_colorfilter = new colorfilter_add();

    public void set_PA(Paint pa) {
        this.pa = pa;
    }

    @Override
    public Paint complete() {
        fnt_cls.runlist();
        ps_cls.runlist();
        return pa;
    }

    public void clr_set(int color) {
        pa.setColor(color);
    }

    public void style_set(Paint.Style style) {
        pa.setStyle(style);
    }

    public class fnt_calls extends RunList {

        @Override
        public void clear_list_option() {
            list.clear();
        }

        public void make_PA_FNT_C_HINTING(int hinting)
        {(new PA_FNT_C_HINTING(hinting)).add_tolist();}
        public void make_PA_FNT_C_ALIGN(Paint.Align align)
        {(new PA_FNT_C_ALIGN(align)).add_tolist();}
        public void make_PA_FNT_C_SCALEX(float scalex)
        {(new PA_FNT_C_SCALEX(scalex)).add_tolist();}
        public void make_PA_FNT_C_SIZE(float size)
        {(new PA_FNT_C_SIZE(size)).add_tolist();}
        public void make_PA_FNT_C_SKEWX(float skewx)
        {(new PA_FNT_C_SKEWX(skewx)).add_tolist();}
        public void make_PA_FNT_C_TYPEFACE(Typeface typeface)
        {(new PA_FNT_C_TYPEFACE(typeface)).add_tolist();}

        public class PA_FNT_C_HINTING extends mini_sub {
            private final int hinting;

            public PA_FNT_C_HINTING(int hinting) {
                this.hinting = hinting;
            }

            @Override
            public void run() {
                pa.setHinting(hinting);
            }
        }

        public class PA_FNT_C_ALIGN extends mini_sub {
            private final Paint.Align align;

            public PA_FNT_C_ALIGN(Paint.Align align) {
                this.align = align;
            }

            @Override
            public void run() {
                pa.setTextAlign(align);
            }

        }

        public class PA_FNT_C_SCALEX extends mini_sub {
            private final float scalex;

            public PA_FNT_C_SCALEX(float scalex) {
                this.scalex = scalex;
            }

            @Override
            public void run() {
                pa.setTextScaleX(scalex);
            }

        }

        public class PA_FNT_C_SIZE extends mini_sub {
            private final float size;

            public PA_FNT_C_SIZE(float size) {
                this.size = size;
            }

            @Override
            public void run() {
                pa.setTextSize(size);
            }

        }

        public class PA_FNT_C_SKEWX extends mini_sub {
            private final float skewx;

            public PA_FNT_C_SKEWX(float skewx) {
                this.skewx = skewx;
            }

            @Override
            public void run() {
                pa.setTextSkewX(skewx);
            }

        }

        public class PA_FNT_C_TYPEFACE extends mini_sub {
            private final Typeface typeface;

            public PA_FNT_C_TYPEFACE(Typeface typeface) {

                this.typeface = typeface;
            }

            @Override
            public void run() {
                pa.setTypeface(typeface);
            }

        }
    }

    public class paint_stroke_calls extends RunList {

        @Override
        public void clear_list_option() {
            list.clear();
        }

        public void make_PA_STRK_C_CAP(Paint.Cap cap)
        {(new PA_STRK_C_CAP(cap)).add_tolist();}
        public void make_PA_STRK_C_JOIN(Paint.Join join)
        {(new PA_STRK_C_JOIN(join)).add_tolist();}
        public void make_PA_STRK_C_MITER(float miter)
        {(new PA_STRK_C_MITER(miter)).add_tolist();}
        public void make_PA_STRK_C_WIDTH(float width)
        {(new PA_STRK_C_WIDTH(width)).add_tolist();}
        public void make_PA_STRK_C_PATHEFFECT(PathEffect pe)
        {(new PA_STRK_C_PATHEFFECT(pe)).add_tolist();}

        public class PA_STRK_C_CAP extends mini_sub {
            private final Paint.Cap cap;

            public PA_STRK_C_CAP(Paint.Cap cap) {
                this.cap = cap;
            }

            @Override
            public void run() {
                pa.setStrokeCap(cap);
            }

        }

        public class PA_STRK_C_JOIN extends mini_sub {
            private final Paint.Join join;

            public PA_STRK_C_JOIN(Paint.Join join) {
                this.join = join;
            }

            @Override
            public void run() {
                pa.setStrokeJoin(join);
            }

        }

        public class PA_STRK_C_MITER extends mini_sub {
            private final float miter;

            public PA_STRK_C_MITER(float miter) {
                this.miter = miter;
            }

            @Override
            public void run() {
                pa.setStrokeMiter(miter);
            }

        }

        public class PA_STRK_C_WIDTH extends mini_sub {
            private final float width;

            public PA_STRK_C_WIDTH(float width) {
                this.width = width;
            }

            @Override
            public void run() {
                pa.setStrokeWidth(width);
            }

        }

        public class PA_STRK_C_PATHEFFECT extends mini_sub {
            private final PathEffect pe;

            public PA_STRK_C_PATHEFFECT(PathEffect pe) {
                this.pe = pe;
            }

            @Override
            public void run() {
                pa.setPathEffect(pe);
            }

        }


    }

    public class shader_accum extends _acum<Shader> {
        public void _BitmapShader(Bitmap bitmap, Shader.TileMode tileX, Shader.TileMode tileY) {
            push(new BitmapShader(bitmap, tileX, tileY));
        }

        public void _ComposeShader(PorterDuff.Mode mode) {
            Shader shaderB = pop();
            Shader shaderA = pop();
            push(new ComposeShader(shaderA, shaderB, mode));
        }

        public void _LinearGradient(float x0, float y0, float x1, float y1,
                                    int[] colors, float[] positions,
                                    Shader.TileMode tile) {
            push(new LinearGradient(x0, y0, x1, y1, colors, positions, tile));
        }

        public void _RadialGradient(float centerX, float centerY, float radius,
                                    int[] colors, float[] stops,
                                    Shader.TileMode tileMode) {
            push(new RadialGradient(centerX, centerY, radius, colors, stops, tileMode));
        }

        public void _SweepGradient(float cx, float cy,
                                   int[] colors, float[] positions) {
            push(new SweepGradient(cx, cy, colors, positions));
        }
    }

    public class xfermode_add {
        public void addAvoidXfermode(int opColor, int tolerance, AvoidXfermode.Mode mode) {
            pa.setXfermode(new AvoidXfermode(opColor, tolerance, mode));
        }

        public void addPixelXorXfermode(int opColor) {
            pa.setXfermode(new PixelXorXfermode(opColor));
        }

        public void addPorterDuffXfermode(PorterDuff.Mode mode) {
            pa.setXfermode(new PorterDuffXfermode(mode));
        }
    }

    public class maskfilter_add {
        public void addBlurMaskFilter(float radius, BlurMaskFilter.Blur style) {
            pa.setMaskFilter(new BlurMaskFilter(radius, style));
        }

        public void addEmbossMaskFilter(float[] direction, float ambient, float specular, float blurRadius) {
            pa.setMaskFilter(new EmbossMaskFilter(direction, ambient, specular, blurRadius));
        }
    }

    public class colorfilter_add {
        public colormatix_get cm = new colormatix_get();

        public void addColorMatrixColorFilter(ColorMatrix matrix) {
            pa.setColorFilter(new ColorMatrixColorFilter(matrix));
        }

        public void addLightingColorFilter(int mul, int add) {
            pa.setColorFilter(new LightingColorFilter(mul, add));
        }

        public void addPorterDuffColorFilter(int color, PorterDuff.Mode mode) {
            pa.setColorFilter(new PorterDuffColorFilter(color, mode));
        }

        public class colormatix_get {
            protected ColorMatrix setting = new ColorMatrix();

            public ColorMatrix _Scale(float rScale, float gScale, float bScale, float aScale) {
                setting.reset();
                setting.setScale(rScale, gScale, bScale, aScale);
                return setting;
            }

            public ColorMatrix _Saturation(float sat) {
                setting.reset();
                setting.setSaturation(sat);
                return setting;
            }

            public ColorMatrix _YUV2RGB() {
                setting.reset();
                setting.setYUV2RGB();
                return setting;
            }

            public ColorMatrix _RGB2YUV() {
                setting.reset();
                setting.setRGB2YUV();
                return setting;
            }
        }
    }

}
