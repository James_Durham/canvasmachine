package Native.Machine;

/**
 * Created by james on 1/26/2016.
 */
public interface Complete<T> {
    public T complete();
}
