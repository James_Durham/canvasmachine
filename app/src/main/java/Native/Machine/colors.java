package Native.Machine;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;

/**
 * Created by james on 1/19/2016.
 */
class colors {
    LinkedList<Object> container = new LinkedList<Object>();

    void color_cnst(Stack<Object> call_stack, int a) {
        call_stack.push(a);
    }

    void color_cnst_list_start() {/*do nothing*/}

    void color_cnst_list_add(Stack<Object> call_stack) {
        int tmp = (Integer) call_stack.pop();
        container.add(tmp);
    }

    void color_cnst_list_end(Stack<Object> call_stack) {
        int ret[] = new int[container.size()];
        int i = 0;
        Iterator it = container.iterator();
        int tmp;
        while (it.hasNext()) {
            tmp = (Integer) it.next();
            ret[i] = tmp;
            i += 1;
        }
        container.clear();
        call_stack.push(ret);
    }

    //THESE GUYS AREN'T BEING IMPLEMENTED
    //color_cnst_array: CCA SQ_L color_cnst_list SQ_R { color_cnst_array_Act(); };
}
