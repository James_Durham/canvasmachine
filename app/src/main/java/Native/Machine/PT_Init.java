package Native.Machine;

import android.graphics.Matrix;
import android.graphics.Path;

/**
 * Created by james on 1/26/2016.
 */
class PT_Init implements Complete<Path> {
    Path pt;
    path_calls calls = new path_calls();

    public void set_PT(Path pt) {
        this.pt = pt;
    }

    @Override
    public Path complete() {
        calls.runlist();
        return pt;
    }

    public class path_calls extends RunList {
        @Override

        public void clear_list_option() {
            list.clear();
        }
        public void make_PT_INIT_C_FILLTYPE(Path.FillType ft)
        {(new PT_INIT_C_FILLTYPE(ft)).add_tolist();}
        public void make_PT_INIT_C_CLOSE()
        {(new PT_INIT_C_CLOSE()).add_tolist();}
        public void make_PT_INIT_C_CUBICTO(float x1, float y1, float x2, float y2, float x3, float y3)
        {(new PT_INIT_C_CUBICTO(x1, y1, x2, y2, x3, y3)).add_tolist();}
        public void make_PT_INIT_C_RCUBICTO(float x1, float y1, float x2, float y2, float x3, float y3)
        {(new PT_INIT_C_RCUBICTO(x1, y1, x2, y2, x3, y3)).add_tolist();}
        public void make_PT_INIT_C_LINETO(float x, float y)
        {(new PT_INIT_C_LINETO(x, y)).add_tolist();}
        public void make_PT_INIT_C_RLINETO(float dx, float dy)
        {(new PT_INIT_C_RLINETO(dx, dy)).add_tolist();}
        public void make_PT_INIT_C_MOVETO(float x, float y)
        {(new PT_INIT_C_MOVETO(x, y)).add_tolist();}
        public void make_PT_INIT_C_RMOVETO(float dx, float dy)
        {(new PT_INIT_C_RMOVETO(dx, dy)).add_tolist();}
        public void make_PT_INIT_C_QUADTO(float x1, float y1, float x2, float y2)
        {(new PT_INIT_C_QUADTO(x1, y1, x2, y2)).add_tolist();}
        public void make_PT_INIT_C_RQUADTO(float dx1, float dy1, float dx2, float dy2)
        {(new PT_INIT_C_RQUADTO(dx1, dy1, dx2, dy2)).add_tolist();}
        public void make_PT_INIT_C_TRANSFORM(Matrix matrix)
        {(new PT_INIT_C_TRANSFORM(matrix)).add_tolist();}


        public class PT_INIT_C_FILLTYPE extends mini_sub {
            private final Path.FillType ft;

            public PT_INIT_C_FILLTYPE(Path.FillType ft) {
                this.ft = ft;
            }

            @Override
            public void run() {
                pt.setFillType(ft);
            }

        }

        public class PT_INIT_C_CLOSE extends mini_sub {

            public PT_INIT_C_CLOSE() {

            }

            @Override
            public void run() {
                pt.close();
            }

        }

        public class PT_INIT_C_CUBICTO extends mini_sub {
            private final float x1;
            private final float y1;
            private final float x2;
            private final float y2;
            private final float x3;
            private final float y3;

            public PT_INIT_C_CUBICTO(float x1, float y1, float x2, float y2, float x3, float y3) {
                this.x1 = x1;
                this.y1 = y1;
                this.x2 = x2;
                this.y2 = y2;
                this.x3 = x3;
                this.y3 = y3;
            }

            @Override
            public void run() {
                pt.cubicTo(x1, y1, x2, y2, x3, y3);
            }

        }

        public class PT_INIT_C_RCUBICTO extends mini_sub {
            private final float x1;
            private final float y1;
            private final float x2;
            private final float y2;
            private final float x3;
            private final float y3;

            public PT_INIT_C_RCUBICTO(float x1, float y1, float x2, float y2, float x3, float y3) {
                this.x1 = x1;
                this.y1 = y1;
                this.x2 = x2;
                this.y2 = y2;
                this.x3 = x3;
                this.y3 = y3;

            }

            @Override
            public void run() {
                pt.rCubicTo(x1, y1, x2, y2, x3, y3);
            }

        }

        public class PT_INIT_C_LINETO extends mini_sub {
            private final float x;
            private final float y;

            public PT_INIT_C_LINETO(float x, float y) {
                this.x = x;
                this.y = y;

            }

            @Override
            public void run() {
                pt.lineTo(x, y);
            }

        }

        public class PT_INIT_C_RLINETO extends mini_sub {
            private final float dx;
            private final float dy;

            public PT_INIT_C_RLINETO(float dx, float dy) {
                this.dx = dx;
                this.dy = dy;
            }

            @Override
            public void run() {
                pt.rLineTo(dx, dy);
            }

        }

        public class PT_INIT_C_MOVETO extends mini_sub {
            private final float x;
            private final float y;

            public PT_INIT_C_MOVETO(float x, float y) {
                this.x = x;
                this.y = y;
            }

            @Override
            public void run() {
                pt.moveTo(x, y);
            }

        }

        public class PT_INIT_C_RMOVETO extends mini_sub {
            private final float dx;
            private final float dy;

            public PT_INIT_C_RMOVETO(float dx, float dy) {
                this.dx = dx;
                this.dy = dy;
            }

            @Override
            public void run() {
                pt.rMoveTo(dx, dy);
            }

        }

        public class PT_INIT_C_QUADTO extends mini_sub {
            private final float x1;
            private final float y1;
            private final float x2;
            private final float y2;

            public PT_INIT_C_QUADTO(float x1, float y1, float x2, float y2) {
                this.x1 = x1;
                this.y1 = y1;
                this.x2 = x2;
                this.y2 = y2;
            }

            @Override
            public void run() {
                pt.quadTo(x1, y1, x2, y2);
            }

        }

        public class PT_INIT_C_RQUADTO extends mini_sub {
            private final float dx1;
            private final float dy1;
            private final float dx2;
            private final float dy2;

            public PT_INIT_C_RQUADTO(float dx1, float dy1, float dx2, float dy2) {
                this.dx1 = dx1;
                this.dy1 = dy1;
                this.dx2 = dx2;
                this.dy2 = dy2;
            }

            @Override
            public void run() {
                pt.rQuadTo(dx1, dy1, dx2, dy2);
            }

        }

        public class PT_INIT_C_TRANSFORM extends mini_sub {
            private final Matrix matrix;

            public PT_INIT_C_TRANSFORM(Matrix matrix) {
                this.matrix = matrix;
            }

            @Override
            public void run() {
                pt.transform(matrix);
            }

        }

    }
}
