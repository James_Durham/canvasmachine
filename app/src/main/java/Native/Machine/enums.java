package Native.Machine;

import android.graphics.AvoidXfermode;
import android.graphics.BlurMaskFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathDashPathEffect;
import android.graphics.PorterDuff;
import android.graphics.Shader;
import android.graphics.Typeface;

import java.util.Stack;

/**
 * Created by james on 1/19/2016.
 */
class enums {
    //Porter-Duff Mode Enumeration
    static void PDM_ADD(Stack<Object> call_stack) {
        call_stack.push(PorterDuff.Mode.ADD);
    }

    static void PDM_CLEAR(Stack<Object> call_stack) {
        call_stack.push(PorterDuff.Mode.CLEAR);
    }

    static void PDM_DARKEN(Stack<Object> call_stack) {
        call_stack.push(PorterDuff.Mode.DARKEN);
    }

    static void PDM_DST(Stack<Object> call_stack) {
        call_stack.push(PorterDuff.Mode.DST);
    }

    static void PDM_DST_ATOP(Stack<Object> call_stack) {
        call_stack.push(PorterDuff.Mode.DST_ATOP);
    }

    static void PDM_DST_IN(Stack<Object> call_stack) {
        call_stack.push(PorterDuff.Mode.DST_IN);
    }

    static void PDM_DST_OUT(Stack<Object> call_stack) {
        call_stack.push(PorterDuff.Mode.DST_OUT);
    }

    static void PDM_DST_OVER(Stack<Object> call_stack) {
        call_stack.push(PorterDuff.Mode.DST_OVER);
    }

    static void PDM_LIGHTEN(Stack<Object> call_stack) {
        call_stack.push(PorterDuff.Mode.LIGHTEN);
    }

    static void PDM_MULTIPLY(Stack<Object> call_stack) {
        call_stack.push(PorterDuff.Mode.MULTIPLY);
    }

    static void PDM_OVERLAY(Stack<Object> call_stack) {
        call_stack.push(PorterDuff.Mode.OVERLAY);
    }

    static void PDM_SCREEN(Stack<Object> call_stack) {
        call_stack.push(PorterDuff.Mode.SCREEN);
    }

    static void PDM_SRC(Stack<Object> call_stack) {
        call_stack.push(PorterDuff.Mode.SRC);
    }

    static void PDM_SRC_ATOP(Stack<Object> call_stack) {
        call_stack.push(PorterDuff.Mode.SRC_ATOP);
    }

    static void PDM_SRC_IN(Stack<Object> call_stack) {
        call_stack.push(PorterDuff.Mode.SRC_IN);
    }

    static void PDM_SRC_OUT(Stack<Object> call_stack) {
        call_stack.push(PorterDuff.Mode.SRC_OUT);
    }

    static void PDM_SRC_OVER(Stack<Object> call_stack) {
        call_stack.push(PorterDuff.Mode.SRC_OVER);
    }

    static void PDM_XOR(Stack<Object> call_stack) {
        call_stack.push(PorterDuff.Mode.XOR);
    }

    // pathdash_style_enum:
    static void PE_PATHDASH_MORPH(Stack<Object> call_stack) {
        call_stack.push(PathDashPathEffect.Style.MORPH);
    }

    static void PE_PATHDASH_ROTATE(Stack<Object> call_stack) {
        call_stack.push(PathDashPathEffect.Style.ROTATE);
    }

    static void PE_PATHDASH_TRANSLATE(Stack<Object> call_stack) {
        call_stack.push(PathDashPathEffect.Style.TRANSLATE);
    }

    // shader_tile:
    static void SH_TILE_CLAMP(Stack<Object> call_stack) {
        call_stack.push(Shader.TileMode.CLAMP);
    }

    static void SH_TILE_MIRROR(Stack<Object> call_stack) {
        call_stack.push(Shader.TileMode.MIRROR);
    }

    static void SH_TILE_REPEAT(Stack<Object> call_stack) {
        call_stack.push(Shader.TileMode.REPEAT);
    }

    // xf_avoid_enum:
    static void XF_AVOID_AVOID(Stack<Object> call_stack) {
        call_stack.push(AvoidXfermode.Mode.AVOID);
    }

    static void XF_AVOID_TARGET(Stack<Object> call_stack) {
        call_stack.push(AvoidXfermode.Mode.TARGET);
    }

    //mf_blur_enum:
    static void MF_BLUR_INNER(Stack<Object> call_stack) {
        call_stack.push(BlurMaskFilter.Blur.INNER);
    }

    static void MF_BLUR_NORMAL(Stack<Object> call_stack) {
        call_stack.push(BlurMaskFilter.Blur.NORMAL);
    }

    static void MF_BLUR_OUTER(Stack<Object> call_stack) {
        call_stack.push(BlurMaskFilter.Blur.OUTER);
    }

    static void MF_BLUR_SOLID(Stack<Object> call_stack) {
        call_stack.push(BlurMaskFilter.Blur.SOLID);
    }

    //filltype_enum:
    static void PT_FILLTYPE_EVEN_ODD(Stack<Object> call_stack) {
        call_stack.push(Path.FillType.EVEN_ODD);
    }

    static void PT_FILLTYPE_INVERSE_EVEN_ODD(Stack<Object> call_stack) {
        call_stack.push(Path.FillType.INVERSE_EVEN_ODD);
    }

    static void PT_FILLTYPE_INVERSE_WINDING(Stack<Object> call_stack) {
        call_stack.push(Path.FillType.INVERSE_WINDING);
    }

    static void PT_FILLTYPE_WINDING(Stack<Object> call_stack) {
        call_stack.push(Path.FillType.WINDING);
    }

    //pa_align_enum:
    static void PA_ALIGN_CENTER(Stack<Object> call_stack) {
        call_stack.push(Paint.Align.CENTER);
    }

    static void PA_ALIGN_LEFT(Stack<Object> call_stack) {
        call_stack.push(Paint.Align.LEFT);
    }

    static void PA_ALIGN_RIGHT(Stack<Object> call_stack) {
        call_stack.push(Paint.Align.RIGHT);
    }

    //typeface:
    static void PA_TYPEFACE_DEFAULT(Stack<Object> call_stack) {
        call_stack.push(Typeface.DEFAULT);
    }

    static void PA_TYPEFACE_DEFAULT_BOLD(Stack<Object> call_stack) {
        call_stack.push(Typeface.DEFAULT_BOLD);
    }

    static void PA_TYPEFACE_MONOSPACE(Stack<Object> call_stack) {
        call_stack.push(Typeface.MONOSPACE);
    }

    static void PA_TYPEFACE_SANS_SERIF(Stack<Object> call_stack) {
        call_stack.push(Typeface.SANS_SERIF);
    }

    static void PA_TYPEFACE_SERIF(Stack<Object> call_stack) {
        call_stack.push(Typeface.SERIF);
    }

    //cap_enum:
    static void PA_CAP_BUTT(Stack<Object> call_stack) {
        call_stack.push(Paint.Cap.BUTT);
    }

    static void PA_CAP_ROUND(Stack<Object> call_stack) {
        call_stack.push(Paint.Cap.ROUND);
    }

    static void PA_CAP_SQUARE(Stack<Object> call_stack) {
        call_stack.push(Paint.Cap.SQUARE);
    }

    //join_enum:
    static void PA_JOIN_BEVEL(Stack<Object> call_stack) {
        call_stack.push(Paint.Join.BEVEL);
    }

    static void PA_JOIN_MITER(Stack<Object> call_stack) {
        call_stack.push(Paint.Join.MITER);
    }

    static void PA_JOIN_ROUND(Stack<Object> call_stack) {
        call_stack.push(Paint.Join.ROUND);
    }

    //pa_style_enum:
    static void PA_STYLE_FILL(Stack<Object> call_stack) {
        call_stack.push(Paint.Style.FILL);
    }

    static void PA_STYLE_FILL_AND_STROKE(Stack<Object> call_stack) {
        call_stack.push(Paint.Style.FILL_AND_STROKE);
    }

    static void PA_STYLE_STROKE(Stack<Object> call_stack) {
        call_stack.push(Paint.Style.STROKE);
    }
}
