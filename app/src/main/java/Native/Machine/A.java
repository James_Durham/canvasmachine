package Native.Machine;

import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.graphics.Picture;
import android.graphics.RectF;

import java.util.HashMap;

import canvasmachine.dev.quarkworks.net.canvasmachine.BuildConfig;

/**
 * Created by james on 1/20/2016.
 */
class A {
    public static void assert_(String msg, boolean check)
    {
        if (BuildConfig.DEBUG)
        {
            if(!check) throw new AssertionError(msg);

        }
    }

    public static <T> String reg_test(Class T_class, T reg[], int index) {

        if (reg[index] == null) {
            return
              String.format("%s_regs[%d]",  A.XX_str.get(T_class), index);

        }
        return null;
    }

    public static final HashMap<Class, String> XX_str = new HashMap<Class, String>(10);
    static {
        XX_str.put(PathEffect.class, "PE");
        XX_str.put(Path.class,       "PT");
        XX_str.put(Picture.class,    "PI");
        XX_str.put(Bitmap.class,     "BM");
        XX_str.put(Paint.class,      "PA");
        XX_str.put(RectF.class,      "RF");
    }
}
