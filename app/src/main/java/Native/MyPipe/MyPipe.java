package Native.MyPipe;

import android.util.Log;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
// NOTE: Write side needs to close before read side.

public class MyPipe {
    public static class Tuple implements Closeable {
        public long Cptr_Cptr_FILE_file;
        public long Cptr_errno_errno;
        public Closeable stream;

        Tuple(long Cptr_Cptr_FILE_file, long Cptr_errno_errno, Closeable stream) {
            this.Cptr_Cptr_FILE_file = Cptr_Cptr_FILE_file;
            this.Cptr_errno_errno = Cptr_errno_errno;
            this.stream = stream;
        }

        @Override
        public void close() {
            try {
                stream.close();
            } catch (IOException e) {
                Log.e("MyPipe.Tuple",
                        String.format("%s close  IOException: %s",
                                stream.getClass().getSimpleName(), e.getMessage()));
            }
        }
    }

    public Tuple write_side;
    public Tuple read_side;
    private long Cptr_Cptr_FILE_read;
    private long Cptr_Cptr_FILE_write;
    private long Cptr_errno_read;
    private long Cptr_errno_write;
    private ByteBuffer read_buff = ByteBuffer.allocateDirect(7);
    private ByteBuffer write_buff = ByteBuffer.allocateDirect(11);

    public MyPipe() throws IOException {
        Cptr_Cptr_FILE_read = MyPipe_JNI.new_FILE_pp();
        Cptr_Cptr_FILE_write = MyPipe_JNI.new_FILE_pp();
        Cptr_errno_read = MyPipe_JNI.newErrno();
        Cptr_errno_write = MyPipe_JNI.newErrno();

        long Cptr_errno = MyPipe_JNI.newErrno();
        int tmp;
        if (0 != (tmp = MyPipe_JNI.fpipe(Cptr_Cptr_FILE_read, Cptr_Cptr_FILE_write, Cptr_errno))) {
            String erstr = MyPipe_JNI.errStr(Cptr_errno);
            MyPipe_JNI.deleteErrno(Cptr_errno);
            throw new IOException(String.format("fpipe failed %d, errstr \"%s\"", tmp, erstr));
        } else {
            MyPipe_JNI.deleteErrno(Cptr_errno);
        }
        MyPipeOutputStream out = new MyPipeOutputStream();
        write_side = new Tuple(Cptr_Cptr_FILE_write, Cptr_errno_write, out);
        MyPipeInputStream in = new MyPipeInputStream();
        read_side = new Tuple(Cptr_Cptr_FILE_read, Cptr_errno_read, in);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        MyPipe_JNI.deleteErrno(Cptr_errno_read);
        MyPipe_JNI.deleteErrno(Cptr_errno_write);
        MyPipe_JNI.delete_FILE_pp(Cptr_Cptr_FILE_read);
        MyPipe_JNI.delete_FILE_pp(Cptr_Cptr_FILE_write);
    }

    public class MyPipeInputStream extends InputStream {
        private boolean eof_ = false;
        private boolean buf_mode_fill = true;

        @Override
        public void close() throws IOException {
            if (!MyPipe_JNI.fclose(Cptr_Cptr_FILE_read, Cptr_errno_read)) {
                throw new IOException(String.format("MyPipeInputStream, fclose failed, errstr \"%s\"", MyPipe_JNI.errStr(Cptr_errno_read)));
            }
        }

        @Override
        public int read() throws IOException {
            if (buf_mode_fill) {
                if (!_fread()) {
                    if (MyPipe_JNI.feof(Cptr_Cptr_FILE_read)) {
                        eof_ = true;
                    } else if (MyPipe_JNI.ferror(Cptr_Cptr_FILE_read)) {
                        throw new IOException(String.format("MyPipeInputStream, _fread failed, errstr \"%s\"", MyPipe_JNI.errStr(Cptr_errno_read)));
                    } else {
                        throw new UnknownError("MyPipeInputStream, _fread failed--SHOULD NOT HAPPEN");
                    }

                }
                buf_mode_fill = false;
                read_buff.flip();
            }
            int ret;
            try {
                ret = read_buff.get();
            } catch (BufferUnderflowException e) {
                if (!eof_) {
                    read_buff.clear();
                    buf_mode_fill = true;
                    ret = read();
                } else {
                    return -1;
                }
            }
            return ret;
        }

        private boolean _fread() {
            int call_ret = MyPipe_JNI.fread(read_buff, read_buff.position(), read_buff.limit(), Cptr_errno_read, Cptr_Cptr_FILE_read);
            read_buff.position(call_ret);
            return (call_ret == read_buff.limit());
        }
    }

    public class MyPipeOutputStream extends OutputStream {

        @Override
        public void close() throws IOException {
            flush();
            if (!MyPipe_JNI.fclose(Cptr_Cptr_FILE_write, Cptr_errno_write)) {
                throw new IOException(String.format("MyPipeOutputStream, fclose failed, errstr \"%s\"", MyPipe_JNI.errStr(Cptr_errno_write)));
            }
        }

        @Override
        public void flush() throws IOException {
            write_buff.flip();
            if (!_fwrite()) {
                if (MyPipe_JNI.ferror(Cptr_Cptr_FILE_write)) {
                    throw new IOException
                            (String.format("MyPipeOutputStream, _fwrite failed, errstr \"%s\"", MyPipe_JNI.errStr(Cptr_errno_write)));
                } else {
                    throw new UnknownError("MyPipeOutputStream, _fwrite failed-- ????");
                }

            } else {
                write_buff.clear();
            }
            if (!MyPipe_JNI.fflush(Cptr_Cptr_FILE_write, Cptr_errno_write)) {
                throw new IOException
                        (String.format("MyPipeOutputStream, fflush failed, errstr \"%s\"", MyPipe_JNI.errStr(Cptr_errno_write)));
            }
        }

        @Override
        public void write(int oneByte) throws IOException {
            byte ob = (byte) oneByte;
            try {
                write_buff.put(ob);
            } catch (BufferOverflowException e) {
                write_buff.flip();
                if (!_fwrite()) {
                    if (MyPipe_JNI.ferror(Cptr_Cptr_FILE_write)) {
                        throw new IOException
                                (String.format("MyPipeOutputStream, _fwrite failed, errstr \"%s\"", MyPipe_JNI.errStr(Cptr_errno_write)));
                    } else {
                        throw new UnknownError("MyPipeOutputStream, _fwrite failed-- ????");
                    }

                } else {
                    write_buff.clear();
                    write_buff.put(ob);
                }
            }
        }


        private boolean _fwrite() {
            int call_ret = MyPipe_JNI.fwrite(write_buff, write_buff.position(), write_buff.limit(), Cptr_errno_write, Cptr_Cptr_FILE_write);
            write_buff.position(call_ret);
            return (call_ret == write_buff.limit());
        }
    }

}