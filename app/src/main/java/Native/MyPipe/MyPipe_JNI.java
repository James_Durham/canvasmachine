package Native.MyPipe;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Created by james on 12/17/2015.
 */
class MyPipe_JNI {

    //returns new position
    static native int fread(ByteBuffer buf, int pos, int limit, long errno_p, long read_pp);

    //returns new position
    static native int fwrite(ByteBuffer buf, int pos, int limit, long errno_p, long write_pp);

    static native long newErrno();

    static native void deleteErrno(long errno_p);

    static native long new_FILE_pp();

    static native void delete_FILE_pp(long file_pp);

    static native int fpipe(long read_pp, long write_pp, long errno_p) throws IOException;

    static native String errStr(long errno_p);

    static native boolean feof(long file_pp);

    static native boolean ferror(long file_pp);

    static native boolean fclose(long file_pp, long errno_p);

    static native boolean fflush(long file_pp, long errno_p);
}
