//package Native;
//
//import android.util.Log;
//import android.widget.TextView;
//
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.io.OutputStreamWriter;
//
///**
// * Created by james on 12/11/2015.
// */
//public class pipetest {
//
//    static Thread readside;
//    static Thread writeside;
//    static MyPipe mp;
//    static private void runPipeTest(final TextView mMessages) {
//        Log.i("runPipeTest", "Start");
//        try {
//            mp = new MyPipe();
//        }  catch (IOException e) {
//            Log.e("runPipeTest", e.toString() );
//            return;
//        }
//        readside = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                Log.i("readside", "start");
//                InputStreamReader r = new InputStreamReader(mp.out);
//                StringBuilder b = new StringBuilder();
//                int r_ret;
//                try {
//                    while (-1 != (r_ret =  r.read()))
//                    {
//                        b.append( (char) r_ret);
//                    }
//                } catch (IOException e)
//                {
//                    Log.e("readside", e.toString());
//                }
//                try {
//                    r.close();
//                } catch (IOException e) {
//                    Log.e("readside_close", e.toString());
//                }
//                final CharSequence finalb = b;
//                mMessages.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        mMessages.setText(finalb);
//                    }
//                });
//            }
//        });
//        writeside = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                Log.i("writeside", "start");
//                OutputStreamWriter osw = new OutputStreamWriter(mp.in);
//                try {
//                    osw.write(SampleData.sample_data);
//                    osw.flush();
//                } catch (IOException e) {
//                    Log.e("writeside", e.toString());
//                }
//                try {
//                    osw.close();
//                } catch (IOException e) {
//                    Log.e("writeside_close", e.toString());
//                }
//                Log.i("writeside", "end");
//            }
//        });
//        readside.start();
//        writeside.start();
//    }
//
//}
