package Native;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.graphics.Picture;
import android.graphics.RectF;
import android.os.HandlerThread;
import android.util.Log;

import java.util.ArrayList;
import java.util.logging.Handler;

import Native.Machine.ActionsCallBacks;
import Native.Machine.Machine;
import Native.Machine.ReBindableRunList;
import Util.F;
import canvasmachine.dev.quarkworks.net.canvasmachine.R;

/**
 * Created by james on 12/12/2015.
 */
public class CanvasMachine {

    public static HandlerThread parse_thread = new HandlerThread("CanvasMachine_parse_thread");
    public static android.os.Handler pt_hand;

    static {
        parse_thread.start();
        pt_hand = new android.os.Handler(parse_thread.getLooper());
    }

    public Done done = null;
    public ReBindableRunList<Canvas> onDraw = null;
    private Machine m = new Machine();
    public CanvasMachine(Done done)
    {
        done.ref = this;
        this.done = done;
    }

    // no done callback
    public CanvasMachine()
    {   }

    private static void exec(Context c, int res)
    {
        String s = F.InStream2String(c.getResources().openRawResource(res));
        CanvasMachine mac = new CanvasMachine();
        long container =  mac.container_scan_string(s);

        if (0 != mac.myparse(mac.m.getACB_imp()))
        {
            Log.i("CanvasMachine", "exec: syntax error");
            mac.container_delete_buffer(container);
            return;
        }

         mac.onDraw = mac.m.getOnDraw();
        // do somthing

        mac.container_delete_buffer(container);
    }

    public static void test(final Application ctx, final int res)
    {
        Runnable runnable = new Runnable() {
            public void run() {
                exec(ctx, res);
            }
        };
        pt_hand.post(runnable);
    }

    private native long container_scan_string(String str);
    private native void container_delete_buffer(long buffer_state);
    private native int myparse(ActionsCallBacks cb);

    public static abstract class Done {
        CanvasMachine ref = null;
        abstract void done();
    }
}
