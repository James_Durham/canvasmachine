/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_GRAMMER_TAB_H_INCLUDED
# define YY_YY_GRAMMER_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif
/* "%code requires" blocks.  */
#line 1 "grammer.y" /* yacc.c:1909  */

#include "../c_side/util/matix_const.h"

#line 48 "grammer.tab.h" /* yacc.c:1909  */

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    DIGIT_sv = 258,
    INT_sv = 259,
    FLOAT_sv = 260,
    BOOL_sv = 261,
    STRING_sv = 262,
    BR_L = 263,
    BR_R = 264,
    SQ_L = 265,
    SQ_R = 266,
    PRN_L = 267,
    PRN_R = 268,
    SMI = 269,
    COM = 270,
    OR = 271,
    NEW = 272,
    PE = 273,
    BM = 274,
    PA = 275,
    RF = 276,
    PT = 277,
    PI = 278,
    FA = 279,
    FBA = 280,
    FQA = 281,
    CCA = 282,
    COLOR_S_RED = 283,
    COLOR_S_BLUE = 284,
    COLOR_S_GREEN = 285,
    COLOR_S_BLACK = 286,
    COLOR_S_WHITE = 287,
    COLOR_S_GRAY = 288,
    COLOR_S_CYAN = 289,
    COLOR_S_MAGENTA = 290,
    COLOR_S_YELLOW = 291,
    COLOR_S_LIGHTGRAY = 292,
    COLOR_S_DARKGRAY = 293,
    COLOR_S_GREY = 294,
    COLOR_S_LIGHTGREY = 295,
    COLOR_S_DARKGREY = 296,
    COLOR_S_AQUA = 297,
    COLOR_S_FUCHSIA = 298,
    COLOR_S_LIME = 299,
    COLOR_S_MAROON = 300,
    COLOR_S_NAVY = 301,
    COLOR_S_OLIVE = 302,
    COLOR_S_PURPLE = 303,
    COLOR_S_SILVER = 304,
    COLOR_S_TEAL = 305,
    COLOR_S_TRANSPARENT = 306,
    COLOR_HSV = 307,
    COLOR_ARGB = 308,
    PE_ELEMENT_COMPOSE = 309,
    PE_ELEMENT_CORNER = 310,
    PE_ELEMENT_DASH = 311,
    PE_ELEMENT_DISCRETE = 312,
    PE_ELEMENT_PATHDASH = 313,
    PE_ELEMENT_SUM = 314,
    SHADER_BITMAP = 315,
    SHADER_COMPOSE = 316,
    SHADER_LINEAR = 317,
    SHADER_RADIAL = 318,
    SHADER_SWEEP = 319,
    XFERMODE_AVOID = 320,
    XFERMODE_PIXELXOR = 321,
    XFERMODE_PORTERDUFF = 322,
    MASKFILTER_BLUR = 323,
    MASKFILTER_EMBOSS = 324,
    COLOR_FILTER_COLORMATRIX = 325,
    COLOR_FILTER_LIGHTING = 326,
    COLOR_FILTER_PORTERDUFF = 327,
    COLORMATRIX_SCALE = 328,
    COLORMATRIX_SATURATION = 329,
    COLORMATRIX_YUV2RGB = 330,
    COLORMATRIX_RGB2YUV = 331,
    MATRIXLITERAL_ROTATE = 332,
    MATRIXLITERAL_SCALE = 333,
    MATRIXLITERAL_SKEW = 334,
    MATRIXLITERAL_TRANSLATE = 335,
    MATRIXLITERAL_MULTIPLY = 336,
    DOT_SET = 337,
    DOT_INITIAL = 338,
    DOT_RECORD = 339,
    DOT_COMPOSITE = 340,
    DOT_MOD = 341,
    CANVAS_C_CLIPPATH = 342,
    CANVAS_C_CLIPRECT = 343,
    CANVAS_C_SAVE = 344,
    CANVAS_C_RESTORE = 345,
    CANVAS_C_ROTATE = 346,
    CANVAS_C_SCALE = 347,
    CANVAS_C_SKEW = 348,
    CANVAS_C_TRANSLATE = 349,
    CANVAS_C_DRAWARC = 350,
    CANVAS_C_DRAWBITMAP = 351,
    CANVAS_C_DRAWBITMAPMESH = 352,
    CANVAS_C_DRAWCIRCLE = 353,
    CANVAS_C_DRAWCOLOR = 354,
    CANVAS_C_DRAWLINES = 355,
    CANVAS_C_DRAWOVAL = 356,
    CANVAS_C_DRAWPATH = 357,
    CANVAS_C_DRAWPICTURE = 358,
    CANVAS_C_DRAWPOINTS = 359,
    CANVAS_C_DRAWRECT = 360,
    CANVAS_C_DRAWROUNDRECT = 361,
    CANVAS_C_DRAWTEXT = 362,
    CANVAS_C_DRAWTEXTONPATH = 363,
    PT_INIT_C_FILLTYPE = 364,
    PT_INIT_C_CLOSE = 365,
    PT_INIT_C_CUBICTO = 366,
    PT_INIT_C_RCUBICTO = 367,
    PT_INIT_C_LINETO = 368,
    PT_INIT_C_RLINETO = 369,
    PT_INIT_C_MOVETO = 370,
    PT_INIT_C_RMOVETO = 371,
    PT_INIT_C_QUADTO = 372,
    PT_INIT_C_RQUADTO = 373,
    PT_INIT_C_TRANSFORM = 374,
    RF_MOD_C_INSET = 375,
    RF_MOD_C_INTERSECT = 376,
    RF_MOD_C_OFFSET = 377,
    RF_MOD_C_SORT = 378,
    RF_MOD_C_UNION = 379,
    PA_FNT_C_HINTING = 380,
    PA_FNT_C_ALIGN = 381,
    PA_FNT_C_SCALEX = 382,
    PA_FNT_C_SIZE = 383,
    PA_FNT_C_SKEWX = 384,
    PA_FNT_C_TYPEFACE = 385,
    PA_STRK_C_CAP = 386,
    PA_STRK_C_JOIN = 387,
    PA_STRK_C_MITER = 388,
    PA_STRK_C_WIDTH = 389,
    PA_STRK_C_PATHEFFECT = 390,
    PA_FLAG_ANTI_ALIAS = 391,
    PA_FLAG_DITHER = 392,
    PA_FLAG_FAKE_BOLD_TEXT = 393,
    PA_FLAG_FILTER_BITMAP = 394,
    PA_FLAG_LINEAR_TEXT = 395,
    PA_FLAG_STRIKE_THRU_TEXT = 396,
    PA_FLAG_SUBPIXEL_TEXT = 397,
    PA_FLAG_UNDERLINE_TEXT = 398,
    PA_STYLE_FILL = 399,
    PA_STYLE_FILL_AND_STROKE = 400,
    PA_STYLE_STROKE = 401,
    PA_TYPEFACE_DEFAULT = 402,
    PA_TYPEFACE_DEFAULT_BOLD = 403,
    PA_TYPEFACE_MONOSPACE = 404,
    PA_TYPEFACE_SANS_SERIF = 405,
    PA_TYPEFACE_SERIF = 406,
    PA_ALIGN_CENTER = 407,
    PA_ALIGN_LEFT = 408,
    PA_ALIGN_RIGHT = 409,
    PA_JOIN_BEVEL = 410,
    PA_JOIN_MITER = 411,
    PA_JOIN_ROUND = 412,
    PA_CAP_BUTT = 413,
    PA_CAP_ROUND = 414,
    PA_CAP_SQUARE = 415,
    PE_PATHDASH_MORPH = 416,
    PE_PATHDASH_ROTATE = 417,
    PE_PATHDASH_TRANSLATE = 418,
    SH_TILE_CLAMP = 419,
    SH_TILE_MIRROR = 420,
    SH_TILE_REPEAT = 421,
    PDM_ADD = 422,
    PDM_CLEAR = 423,
    PDM_DARKEN = 424,
    PDM_DST = 425,
    PDM_DST_ATOP = 426,
    PDM_DST_IN = 427,
    PDM_DST_OUT = 428,
    PDM_DST_OVER = 429,
    PDM_LIGHTEN = 430,
    PDM_MULTIPLY = 431,
    PDM_OVERLAY = 432,
    PDM_SCREEN = 433,
    PDM_SRC = 434,
    PDM_SRC_ATOP = 435,
    PDM_SRC_IN = 436,
    PDM_SRC_OUT = 437,
    PDM_SRC_OVER = 438,
    PDM_XOR = 439,
    XF_AVOID_AVOID = 440,
    XF_AVOID_TARGET = 441,
    MF_BLUR_INNER = 442,
    MF_BLUR_NORMAL = 443,
    MF_BLUR_OUTER = 444,
    MF_BLUR_SOLID = 445,
    PT_FILLTYPE_EVEN_ODD = 446,
    PT_FILLTYPE_INVERSE_EVEN_ODD = 447,
    PT_FILLTYPE_INVERSE_WINDING = 448,
    PT_FILLTYPE_WINDING = 449,
    DEC_SPEC_FONT = 450,
    DEC_SPEC_COLOR = 451,
    DEC_SPEC_PATHMODE = 452,
    DEC_SPEC_STROKE = 453,
    DEC_SPEC_FILL = 454,
    DEC_SPEC_COMPOSITEMODE = 455,
    DEC_SPEC_MASKFILTER = 456,
    DEC_SPEC_COLORFILTER = 457,
    DEC_SPEC_BODY = 458,
    DEC_SPEC_DECLARATION = 459,
    DEC_SPEC_ONDRAW = 460
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 23 "grammer.y" /* yacc.c:1909  */

	int i;
	float f;
	char *str;
	matrix *mat;

#line 273 "grammer.tab.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_GRAMMER_TAB_H_INCLUDED  */
