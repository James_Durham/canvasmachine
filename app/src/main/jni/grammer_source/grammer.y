%code requires {
#include "../c_side/util/matix_const.h"
}
%{
#include "lex.yy.h"

#include "../c_side/inside_actions.h"
#include "../egress/outside_actions.h"

// debug
#include "../utils/inline_LOGx.h"
// debug end

void yyerror (char const *);

#define SUCCESS  0
#define RNA      1
#define OTHER    2


%}

%union {
	int i;
	float f;
	char *str;
	matrix *mat;
}

%token <i>   DIGIT_sv
%token <i>   INT_sv
%token <f>   FLOAT_sv
%token <i>   BOOL_sv
%token <str> STRING_sv

%token BR_L  "{"
%token BR_R  "}"
%token SQ_L  "["
%token SQ_R  "]"
%token PRN_L "("
%token PRN_R ")"
%token SMI   ";"
%token COM   ","
%token OR    "|"

%token NEW "new"

%token PE "PE"
%token BM "BM"
%token PA "PA"
%token RF "RF"
%token PT "PT"
%token PI "PI"

%token FA   "FA"
%token FBA  "FBA"
%token FQA  "FQA"
%token CCA  "CCA"

%token COLOR_S_RED            "red"
%token COLOR_S_BLUE           "blue"
%token COLOR_S_GREEN          "green"
%token COLOR_S_BLACK          "black"
%token COLOR_S_WHITE          "white"
%token COLOR_S_GRAY           "gray"
%token COLOR_S_CYAN           "cyan"
%token COLOR_S_MAGENTA        "magenta"
%token COLOR_S_YELLOW         "yellow"
%token COLOR_S_LIGHTGRAY      "lightgray"
%token COLOR_S_DARKGRAY       "darkgray"
%token COLOR_S_GREY           "grey"
%token COLOR_S_LIGHTGREY      "lightgrey"
%token COLOR_S_DARKGREY       "darkgrey"
%token COLOR_S_AQUA           "aqua"
%token COLOR_S_FUCHSIA        "fuchsia"
%token COLOR_S_LIME           "lime"
%token COLOR_S_MAROON         "maroon"
%token COLOR_S_NAVY           "navy"
%token COLOR_S_OLIVE          "olive"
%token COLOR_S_PURPLE         "purple"
%token COLOR_S_SILVER         "silver"
%token COLOR_S_TEAL           "teal"
%token COLOR_S_TRANSPARENT    "TRANSPARENT"

%token COLOR_HSV              "HSV"
%token COLOR_ARGB             "ARGB"

%token PE_ELEMENT_COMPOSE       "Compose"
%token PE_ELEMENT_CORNER        "Corner"
%token PE_ELEMENT_DASH          "Dash"
%token PE_ELEMENT_DISCRETE      "Discrete"
%token PE_ELEMENT_PATHDASH      "PathDash"
%token PE_ELEMENT_SUM           "Sum"

%token SHADER_BITMAP            "Bitmap"
%token SHADER_COMPOSE           "Compose"
%token SHADER_LINEAR            "Linear"
%token SHADER_RADIAL            "Radial"
%token SHADER_SWEEP             "Sweep"

%token XFERMODE_AVOID           "Avoid"
%token XFERMODE_PIXELXOR        "PixelXOR"
%token XFERMODE_PORTERDUFF      "PorterDuff"

%token MASKFILTER_BLUR          "Blur"
%token MASKFILTER_EMBOSS        "Emboss"

%token COLOR_FILTER_COLORMATRIX "ColorMatrix"
%token COLOR_FILTER_LIGHTING    "Lighting"
%token COLOR_FILTER_PORTERDUFF  "PorterDuff"

%token COLORMATRIX_SCALE        "Scale"
%token COLORMATRIX_SATURATION   "Saturation"
%token COLORMATRIX_YUV2RGB      "YUV2RGB"
%token COLORMATRIX_RGB2YUV      "RGB2YUV"

%token MATRIXLITERAL_ROTATE     "Rotate"
%token MATRIXLITERAL_SCALE      "Scale"
%token MATRIXLITERAL_SKEW       "Skew"
%token MATRIXLITERAL_TRANSLATE  "Translate"
%token MATRIXLITERAL_MULTIPLY   "Multiply"

%token DOT_SET                  ".Set"
%token DOT_INITIAL              ".Initial"
%token DOT_RECORD               ".Record"
%token DOT_COMPOSITE            ".Composite"
%token DOT_MOD                  ".Mod"


%token CANVAS_C_CLIPPATH       "clipPath"
%token CANVAS_C_CLIPRECT       "clipRect"

%token CANVAS_C_SAVE           "save"
%token CANVAS_C_RESTORE        "restore"

%token CANVAS_C_ROTATE         "rotate"
%token CANVAS_C_SCALE          "scale"
%token CANVAS_C_SKEW           "skew"
%token CANVAS_C_TRANSLATE      "translate"

%token CANVAS_C_DRAWARC        "drawArc"
%token CANVAS_C_DRAWBITMAP     "drawBitmap"
%token CANVAS_C_DRAWBITMAPMESH "drawBitmapMesh"
%token CANVAS_C_DRAWCIRCLE     "drawCircle"
%token CANVAS_C_DRAWCOLOR      "drawColor"
%token CANVAS_C_DRAWLINES      "drawLines"
%token CANVAS_C_DRAWOVAL       "drawOval"
%token CANVAS_C_DRAWPATH       "drawPath"
%token CANVAS_C_DRAWPICTURE    "drawPicture"
%token CANVAS_C_DRAWPOINTS     "drawPoints"
%token CANVAS_C_DRAWRECT       "drawRect"
%token CANVAS_C_DRAWROUNDRECT  "drawRoundRect"
%token CANVAS_C_DRAWTEXT       "drawText"
%token CANVAS_C_DRAWTEXTONPATH "drawTextOnPath"

%token PT_INIT_C_FILLTYPE      "FillType"
%token PT_INIT_C_CLOSE         "close"
%token PT_INIT_C_CUBICTO       "cubicTo"
%token PT_INIT_C_RCUBICTO      "rCubicTo"
%token PT_INIT_C_LINETO        "lineTo"
%token PT_INIT_C_RLINETO       "rLineTo"
%token PT_INIT_C_MOVETO        "moveTo"
%token PT_INIT_C_RMOVETO       "rMoveTo"
%token PT_INIT_C_QUADTO        "quadTo"
%token PT_INIT_C_RQUADTO       "rQuadTo"
%token PT_INIT_C_TRANSFORM     "transform"

%token RF_MOD_C_INSET          "inset"
%token RF_MOD_C_INTERSECT      "intersect"
%token RF_MOD_C_OFFSET         "offset"
%token RF_MOD_C_SORT           "sort"
%token RF_MOD_C_UNION          "union"

%token PA_FNT_C_HINTING        "Hinting"
%token PA_FNT_C_ALIGN          "Align"
%token PA_FNT_C_SCALEX         "ScaleX"
%token PA_FNT_C_SIZE           "Size"
%token PA_FNT_C_SKEWX          "SkewX"
%token PA_FNT_C_TYPEFACE       "Typeface"

%token PA_STRK_C_CAP           "Cap"
%token PA_STRK_C_JOIN          "Join"
%token PA_STRK_C_MITER         "Miter"
%token PA_STRK_C_WIDTH         "Width"
%token PA_STRK_C_PATHEFFECT    "PathEffect"

%token PA_FLAG_ANTI_ALIAS           "ANTI_ALIAS_FLAG"
%token PA_FLAG_DITHER               "DITHER_FLAG"
%token PA_FLAG_FAKE_BOLD_TEXT       "FAKE_BOLD_TEXT_FLAG"
%token PA_FLAG_FILTER_BITMAP        "FILTER_BITMAP_FLAG"
%token PA_FLAG_LINEAR_TEXT          "LINEAR_TEXT_FLAG"
%token PA_FLAG_STRIKE_THRU_TEXT     "STRIKE_THRU_TEXT_FLAG"
%token PA_FLAG_SUBPIXEL_TEXT        "SUBPIXEL_TEXT_FLAG"
%token PA_FLAG_UNDERLINE_TEXT       "UNDERLINE_TEXT_FLAG"

%token PA_STYLE_FILL                "FILL"
%token PA_STYLE_FILL_AND_STROKE     "FILL_AND_STROKE"
%token PA_STYLE_STROKE              "STROKE"

%token PA_TYPEFACE_DEFAULT          "DEFAULT"
%token PA_TYPEFACE_DEFAULT_BOLD     "DEFAULT_BOLD"
%token PA_TYPEFACE_MONOSPACE        "MONOSPACE"
%token PA_TYPEFACE_SANS_SERIF       "SANS_SERIF"
%token PA_TYPEFACE_SERIF            "SERIF"

%token PA_ALIGN_CENTER              "CENTER"
%token PA_ALIGN_LEFT                "LEFT"
%token PA_ALIGN_RIGHT               "RIGHT"

%token PA_JOIN_BEVEL                "BEVEL"
%token PA_JOIN_MITER                "MITER"
%token PA_JOIN_ROUND                "ROUND"

%token PA_CAP_BUTT                  "BUTT"
%token PA_CAP_ROUND                 "ROUND"
%token PA_CAP_SQUARE                "SQUARE"

%token PE_PATHDASH_MORPH            "MORPH"
%token PE_PATHDASH_ROTATE           "ROTATE"
%token PE_PATHDASH_TRANSLATE        "TRANSLATE"

%token SH_TILE_CLAMP                "CLAMP"
%token SH_TILE_MIRROR               "MIRROR"
%token SH_TILE_REPEAT               "REPEAT"

%token PDM_ADD                      "ADD"
%token PDM_CLEAR                    "CLEAR"
%token PDM_DARKEN                   "DARKEN"
%token PDM_DST                      "DST"
%token PDM_DST_ATOP                 "DST_ATOP"
%token PDM_DST_IN                   "DST_IN"
%token PDM_DST_OUT                  "DST_OUT"
%token PDM_DST_OVER                 "DST_OVER"
%token PDM_LIGHTEN                  "LIGHTEN"
%token PDM_MULTIPLY                 "MULTIPLY"
%token PDM_OVERLAY                  "OVERLAY"
%token PDM_SCREEN                   "SCREEN"
%token PDM_SRC                      "SRC"
%token PDM_SRC_ATOP                 "SRC_ATOP"
%token PDM_SRC_IN                   "SRC_IN"
%token PDM_SRC_OUT                  "SRC_OUT"
%token PDM_SRC_OVER                 "SRC_OVER"
%token PDM_XOR                      "XOR"

%token XF_AVOID_AVOID               "AVOID"
%token XF_AVOID_TARGET              "TARGET"

%token MF_BLUR_INNER                "INNER"
%token MF_BLUR_NORMAL               "NORMAL"
%token MF_BLUR_OUTER                "OUTER"
%token MF_BLUR_SOLID                "SOLID"

%token PT_FILLTYPE_EVEN_ODD         "EVEN_ODD"
%token PT_FILLTYPE_INVERSE_EVEN_ODD "INVERSE_EVEN_ODD"
%token PT_FILLTYPE_INVERSE_WINDING  "INVERSE_WINDING"
%token PT_FILLTYPE_WINDING          "WINDING"

%token DEC_SPEC_FONT            "Font"
%token DEC_SPEC_COLOR           "Color"
%token DEC_SPEC_PATHMODE        "PathMode"
%token DEC_SPEC_STROKE          "Stroke"
%token DEC_SPEC_FILL            "Fill"
%token DEC_SPEC_COMPOSITEMODE   "CompositeMode"
%token DEC_SPEC_MASKFILTER      "MaskFilter"
%token DEC_SPEC_COLORFILTER     "ColorFilter"
%token DEC_SPEC_BODY            "Body"
%token DEC_SPEC_DECLARATION     "Declarations"
%token DEC_SPEC_ONDRAW          "onDraw"

%type <i>  PE_ref_sv BM_ref_sv PA_ref_sv RF_ref_sv PT_ref_sv PI_ref_sv  color_string_sv color_hsv_sv color_argb_sv color_cnst_sv  paint_flags_sv paint_flag_sv
%type <mat> matrixliteral
%%
 //Grammar rules
root: body { LOGI_puts("root"); };

PE_ref_sv: PE DIGIT_sv { $$ = PE_ref_sv_Act( $2 ); };  //xcpt
BM_ref_sv: BM DIGIT_sv { $$ = BM_ref_sv_Act( $2 ); };  //xcpt
PA_ref_sv: PA DIGIT_sv { $$ = PA_ref_sv_Act( $2 ); };  //xcpt
RF_ref_sv: RF DIGIT_sv { $$ = RF_ref_sv_Act( $2 ); };  //xcpt
PT_ref_sv: PT DIGIT_sv { $$ = PT_ref_sv_Act( $2 ); };  //xcpt
PI_ref_sv: PI DIGIT_sv { $$ = PI_ref_sv_Act( $2 ); };  //xcpt

float_bin_cnst: PRN_L FLOAT_sv COM FLOAT_sv PRN_R { switch (float_bin_cnst_Act( $2, $4 )) 
    { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
float_tri_cnst: PRN_L FLOAT_sv COM FLOAT_sv COM FLOAT_sv PRN_R { switch (float_tri_cnst_Act( $2, $4, $6 )) 
    { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
float_quad_cnst: PRN_L FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM FLOAT_sv PRN_R 
    { switch (float_quad_cnst_Act( $2, $4, $6, $8 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } } ;

float_list: %empty { switch (float_list_empty_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   FLOAT_sv { switch (float_list_F_Act( $1 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   float_list COM FLOAT_sv { switch (float_list_FL_F_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } } ;
float_bin_list: %empty { switch (float_bin_list_empty_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   float_bin_cnst { switch (float_bin_list_FBC_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   float_bin_list COM float_bin_cnst { switch (float_bin_list_FBL_FBC_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
float_quad_list: %empty { switch (float_quad_list_empty_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   float_quad_cnst { switch (float_quad_list_FQC_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   float_quad_list COM float_quad_cnst { switch (float_quad_list_FQL_FQC_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };

float_array:      FA  SQ_L float_list      SQ_R { switch (float_array_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
float_bin_array:  FBA SQ_L float_bin_list  SQ_R { switch (float_bin_array_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
float_quad_array: FQA SQ_L float_quad_list SQ_R { switch (float_quad_array_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };

 //Color
color_string_sv:  //xcpt
        COLOR_S_RED         { $$ = color_string_COLOR_S_RED_Act()        ; }
    |   COLOR_S_BLUE        { $$ = color_string_COLOR_S_BLUE_Act()       ; }
    |   COLOR_S_GREEN       { $$ = color_string_COLOR_S_GREEN_Act()      ; }
    |   COLOR_S_BLACK       { $$ = color_string_COLOR_S_BLACK_Act()      ; }
    |   COLOR_S_WHITE       { $$ = color_string_COLOR_S_WHITE_Act()      ; }
    |   COLOR_S_GRAY        { $$ = color_string_COLOR_S_GRAY_Act()       ; }
    |   COLOR_S_CYAN        { $$ = color_string_COLOR_S_CYAN_Act()       ; }
    |   COLOR_S_MAGENTA     { $$ = color_string_COLOR_S_MAGENTA_Act()    ; }
    |   COLOR_S_YELLOW      { $$ = color_string_COLOR_S_YELLOW_Act()     ; }
    |   COLOR_S_LIGHTGRAY   { $$ = color_string_COLOR_S_LIGHTGRAY_Act()  ; }
    |   COLOR_S_DARKGRAY    { $$ = color_string_COLOR_S_DARKGRAY_Act()   ; }
    |   COLOR_S_GREY        { $$ = color_string_COLOR_S_GREY_Act()       ; }
    |   COLOR_S_LIGHTGREY   { $$ = color_string_COLOR_S_LIGHTGREY_Act()  ; }
    |   COLOR_S_DARKGREY    { $$ = color_string_COLOR_S_DARKGREY_Act()   ; }
    |   COLOR_S_AQUA        { $$ = color_string_COLOR_S_AQUA_Act()       ; }
    |   COLOR_S_FUCHSIA     { $$ = color_string_COLOR_S_FUCHSIA_Act()    ; }
    |   COLOR_S_LIME        { $$ = color_string_COLOR_S_LIME_Act()       ; }
    |   COLOR_S_MAROON      { $$ = color_string_COLOR_S_MAROON_Act()     ; }
    |   COLOR_S_NAVY        { $$ = color_string_COLOR_S_NAVY_Act()       ; }
    |   COLOR_S_OLIVE       { $$ = color_string_COLOR_S_OLIVE_Act()      ; }
    |   COLOR_S_PURPLE      { $$ = color_string_COLOR_S_PURPLE_Act()     ; }
    |   COLOR_S_SILVER      { $$ = color_string_COLOR_S_SILVER_Act()     ; }
    |   COLOR_S_TEAL        { $$ = color_string_COLOR_S_TEAL_Act()       ; }
    |   COLOR_S_TRANSPARENT { $$ = color_string_COLOR_S_TRANSPARENT_Act(); };

color_hsv_sv: COLOR_HSV PRN_L INT_sv COM FLOAT_sv COM FLOAT_sv COM FLOAT_sv PRN_R {$$ = color_hsv_sv_Act( $3, $5, $7, $9 );}; //xcpt
color_argb_sv: COLOR_ARGB PRN_L INT_sv COM INT_sv COM INT_sv COM INT_sv PRN_R {$$ = color_argb_sv_Act( $3, $5, $7, $9 );}; //xcpt
color_cnst_sv: //xcpt
        color_string_sv { switch (color_cnst_sv_color_string_sv_Act( $1 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   color_hsv_sv { switch (color_cnst_sv_color_hsv_sv_Act( $1 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   color_argb_sv { switch (color_cnst_sv_color_argb_sv_Act( $1 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
color_cnst_list: %empty { switch (color_cnst_list_empty_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   color_cnst_sv { switch (color_cnst_list_CC_Act( )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   color_cnst_list COM color_cnst_sv { switch (color_cnst_list_CCL_CC_Act(  )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };

color_cnst_array: CCA SQ_L color_cnst_list SQ_R { switch (color_cnst_array_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };

 //Porter-Duff Mode Enumeration
pdm_enum:
        PDM_ADD         { switch (pdm_enum_PDM_ADD_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }         ; }
    |   PDM_CLEAR       { switch (pdm_enum_PDM_CLEAR_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }       ; }
    |   PDM_DARKEN      { switch (pdm_enum_PDM_DARKEN_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }      ; }
    |   PDM_DST         { switch (pdm_enum_PDM_DST_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }         ; }
    |   PDM_DST_ATOP    { switch (pdm_enum_PDM_DST_ATOP_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }    ; }
    |   PDM_DST_IN      { switch (pdm_enum_PDM_DST_IN_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }      ; }
    |   PDM_DST_OUT     { switch (pdm_enum_PDM_DST_OUT_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }     ; }
    |   PDM_DST_OVER    { switch (pdm_enum_PDM_DST_OVER_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }    ; }
    |   PDM_LIGHTEN     { switch (pdm_enum_PDM_LIGHTEN_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }     ; }
    |   PDM_MULTIPLY    { switch (pdm_enum_PDM_MULTIPLY_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }    ; }
    |   PDM_OVERLAY     { switch (pdm_enum_PDM_OVERLAY_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }     ; }
    |   PDM_SCREEN      { switch (pdm_enum_PDM_SCREEN_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }      ; }
    |   PDM_SRC         { switch (pdm_enum_PDM_SRC_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }         ; }
    |   PDM_SRC_ATOP    { switch (pdm_enum_PDM_SRC_ATOP_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }    ; }
    |   PDM_SRC_IN      { switch (pdm_enum_PDM_SRC_IN_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }      ; }
    |   PDM_SRC_OUT     { switch (pdm_enum_PDM_SRC_OUT_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }     ; }
    |   PDM_SRC_OVER    { switch (pdm_enum_PDM_SRC_OVER_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }    ; }
    |   PDM_XOR         { switch (pdm_enum_PDM_XOR_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }         ; };


 //PathEffect Element
pe_element:
        PE_ELEMENT_COMPOSE PRN_L pe_element COM pe_element PRN_R  
		{ switch (pe_element_PE_COMPOSE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PE_ELEMENT_CORNER PRN_L FLOAT_sv PRN_R                       
	    { switch (pe_element_PE_CORNER_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PE_ELEMENT_DASH PRN_L float_bin_array COM FLOAT_sv PRN_R     
	    { switch (pe_element_PE_DASH_Act( $5 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PE_ELEMENT_DISCRETE PRN_L FLOAT_sv COM FLOAT_sv PRN_R           
	    { switch (pe_element_PE_DISCRETE_Act( $3, $5 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PE_ELEMENT_PATHDASH PRN_L PT_ref_sv COM FLOAT_sv COM FLOAT_sv COM pathdash_style_enum PRN_R 
	    { switch (pe_element_PE_PATHDASH_Act( $3, $5, $7 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : ; }  /*YYABORT;*/ }
    |   PE_ELEMENT_SUM PRN_L pe_element COM pe_element PRN_R      
	    { switch (pe_element_PE_SUM_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    ;
pathdash_style_enum:
        PE_PATHDASH_MORPH       
		{ switch (pathdash_style_enum_MORPH_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PE_PATHDASH_ROTATE      
	    { switch (pathdash_style_enum_ROTATE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PE_PATHDASH_TRANSLATE   
	    { switch (pathdash_style_enum_TRANSLATE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };

 //Shader
shader:
        SHADER_BITMAP PRN_L BM_ref_sv COM shader_tile COM shader_tile PRN_R  
		{ switch(shader_BITMAP_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : ; } /*YYABORT;*/ }
    |   SHADER_COMPOSE PRN_L shader COM shader COM pdm_enum PRN_R  
	    { switch (shader_COMPOSE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   SHADER_LINEAR PRN_L FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM color_cnst_array COM float_array COM shader_tile PRN_R
        { switch (shader_LINEAR_Act( $3, $5, $7, $9 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   SHADER_RADIAL PRN_L FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM color_cnst_array COM float_array COM shader_tile PRN_R
        { switch (shader_RADIAL_Act( $3, $5, $7 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   SHADER_SWEEP PRN_L FLOAT_sv COM FLOAT_sv COM color_cnst_array COM float_array COM shader_tile PRN_R
        { switch (shader_SWEEP_Act( $3, $5 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
shader_tile:
        SH_TILE_CLAMP   { switch (shader_tile_CLAMP_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   SH_TILE_MIRROR  { switch (shader_tile_MIRROR_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   SH_TILE_REPEAT  { switch (shader_tile_REPEAT_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };

 //XferMode
xfermode:
        XFERMODE_AVOID PRN_L color_cnst_sv COM INT_sv COM xf_avoid_enum PRN_R 
		{ switch (xfermode_Avoid_Act( $3, $5 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   XFERMODE_PIXELXOR PRN_L color_cnst_sv PRN_R { switch (xfermode_PixelXor_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   XFERMODE_PORTERDUFF PRN_L pdm_enum PRN_R { switch (xfermode_PorterDuff_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
xf_avoid_enum:
        XF_AVOID_AVOID { switch (xf_avoid_enum_AVOID_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   XF_AVOID_TARGET { switch (xf_avoid_enum_TARGET_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };

 //MaskFilter
maskfilter:
        MASKFILTER_BLUR PRN_L FLOAT_sv COM mf_blur_enum PRN_R { switch (maskfilter_BLUR_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   MASKFILTER_EMBOSS PRN_L float_tri_cnst COM FLOAT_sv COM FLOAT_sv COM FLOAT_sv PRN_R 
	   { switch (maskfilter_EMBOSS_Act( $5, $7, $9 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
mf_blur_enum:
        MF_BLUR_INNER   { switch (mf_blur_enum_INNER_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   MF_BLUR_NORMAL  { switch (mf_blur_enum_NORMAL_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   MF_BLUR_OUTER   { switch (mf_blur_enum_OUTER_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   MF_BLUR_SOLID   { switch (mf_blur_enum_SOLID_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };

 //ColorFilter
colorfilter:
        COLOR_FILTER_COLORMATRIX PRN_L colormatrix PRN_R { switch (colorfilter_ColorMatrix_Act()) 
		{ case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   COLOR_FILTER_LIGHTING PRN_L INT_sv COM INT_sv PRN_R 
	    { switch (colorfilter_Lighting_Act( $3, $5 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   COLOR_FILTER_PORTERDUFF PRN_L color_cnst_sv COM pdm_enum PRN_R 
	    { switch (colorfilter_PorterDuff_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
colormatrix:
        COLORMATRIX_SCALE PRN_L FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM FLOAT_sv PRN_R 
        { switch (colormatrix_Scale_Act( $3, $5, $7, $9 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  }
    |   COLORMATRIX_SATURATION PRN_L FLOAT_sv PRN_R 
	    { switch (colormatrix_Saturation_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   COLORMATRIX_YUV2RGB { switch (colormatrix_YUV2RGB_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  }
    |   COLORMATRIX_RGB2YUV { switch (colormatrix_RGB2YUV_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  };

 //MatrixLiteral
matrixliteral: //xcpt
        MATRIXLITERAL_ROTATE PRN_L FLOAT_sv COM FLOAT_sv COM FLOAT_sv PRN_R
        { $$ = matrixliteral_Rotate_Act( $3, $5, $7 ); }
    |   MATRIXLITERAL_SCALE PRN_L FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM FLOAT_sv PRN_R
        { $$ = matrixliteral_Scale_Act( $3, $5, $7, $9 ); }
    |   MATRIXLITERAL_SKEW PRN_L FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM FLOAT_sv PRN_R
        { $$ = matrixliteral_Skew_Act( $3, $5, $7, $9 ); }
    |   MATRIXLITERAL_TRANSLATE PRN_L FLOAT_sv COM FLOAT_sv PRN_R
        { $$ = matrixliteral_Translate_Act( $3, $5 ); }
    |   MATRIXLITERAL_MULTIPLY PRN_L matrixliteral COM matrixliteral PRN_R
        { $$ =  matrixliteral_Concat_Act( $3, $5 ); };


 //CanvasCall
canvascall:
        CANVAS_C_CLIPPATH PRN_L PT_ref_sv PRN_R
        { switch (canvascall_CLIPPATH_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
    |   CANVAS_C_CLIPRECT PRN_L FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM FLOAT_sv PRN_R
        { switch (canvascall_CLIPRECT_ffff_Act( $3, $5, $7, $9 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   CANVAS_C_CLIPRECT PRN_L RF_ref_sv PRN_R
        { switch (canvascall_CLIPRECT_rf_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }

    |   CANVAS_C_SAVE PRN_L PRN_R
        { switch (canvascall_SAVE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   CANVAS_C_RESTORE PRN_L PRN_R
        { switch (canvascall_RESTORE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }

    |   CANVAS_C_ROTATE PRN_L FLOAT_sv PRN_R
        { switch (canvascall_ROTATE_f_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   CANVAS_C_ROTATE PRN_L FLOAT_sv COM FLOAT_sv COM FLOAT_sv PRN_R
        { switch (canvascall_ROTATE_fff_Act( $3, $5, $7 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   CANVAS_C_SCALE PRN_L FLOAT_sv COM FLOAT_sv PRN_R
        { switch (canvascall_SCALE_ff_Act( $3, $5 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   CANVAS_C_SCALE PRN_L FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM FLOAT_sv PRN_R
        { switch (canvascall_SCALE_ffff_Act( $3, $5, $7, $9 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   CANVAS_C_SKEW PRN_L FLOAT_sv COM FLOAT_sv PRN_R
        { switch (canvascall_SKEW_Act( $3, $5 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   CANVAS_C_TRANSLATE PRN_L FLOAT_sv COM FLOAT_sv PRN_R
        { switch (canvascall_TRANSLATE_Act( $3, $5 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }

    |   CANVAS_C_DRAWARC PRN_L RF_ref_sv COM FLOAT_sv COM FLOAT_sv COM BOOL_sv COM PA_ref_sv PRN_R
        { switch (canvascall_DRAWARC_Act( $3, $5, $7, $9, $11 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
    |   CANVAS_C_DRAWBITMAP PRN_L BM_ref_sv COM FLOAT_sv COM FLOAT_sv COM PA_ref_sv PRN_R
        { switch (canvascall_DRAWBITMAP_Act( $3, $5, $7, $9 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
    |   CANVAS_C_DRAWBITMAPMESH PRN_L BM_ref_sv COM INT_sv COM INT_sv COM float_array COM INT_sv COM color_cnst_array COM INT_sv COM PA_ref_sv PRN_R
        { switch (canvascall_DRAWBITMAPMESH_Act( $3, $5, $7, $11, $15, $17 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
    |   CANVAS_C_DRAWCIRCLE PRN_L FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM PA_ref_sv PRN_R
        { switch (canvascall_DRAWCIRCLE_Act( $3, $5, $7, $9 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }   /*YYABORT;*/ }
    |   CANVAS_C_DRAWCOLOR PRN_L color_cnst_sv PRN_R
        { switch (canvascall_DRAWCOLOR_i_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   CANVAS_C_DRAWCOLOR PRN_L color_cnst_sv COM pdm_enum PRN_R
        { switch (canvascall_DRAWCOLOR_ie_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   CANVAS_C_DRAWLINES PRN_L float_quad_array COM PA_ref_sv PRN_R
        { switch (canvascall_DRAWLINES_Act( $5 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }   /*YYABORT;*/ }
    |   CANVAS_C_DRAWOVAL PRN_L RF_ref_sv COM PA_ref_sv PRN_R
        { switch (canvascall_DRAWOVAL_Act( $3, $5 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
    |   CANVAS_C_DRAWPATH PRN_L PT_ref_sv COM PA_ref_sv PRN_R
        { switch (canvascall_DRAWPATH_Act( $3, $5 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
    |   CANVAS_C_DRAWPICTURE PRN_L PI_ref_sv COM RF_ref_sv PRN_R
        { switch (canvascall_DRAWPICTURE_pirf_Act( $3, $5 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
    |   CANVAS_C_DRAWPICTURE PRN_L PI_ref_sv PRN_R
        { switch (canvascall_DRAWPICTURE_pi_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
    |   CANVAS_C_DRAWPOINTS PRN_L float_bin_array COM PA_ref_sv PRN_R
        { switch (canvascall_DRAWPOINTS_Act( $5 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
    |   CANVAS_C_DRAWRECT PRN_L FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM PA_ref_sv PRN_R
        { switch (canvascall_DRAWRECT_ffff_Act( $3, $5, $7, $9, $11 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
    |   CANVAS_C_DRAWRECT PRN_L RF_ref_sv COM PA_ref_sv PRN_R
        { switch (canvascall_DRAWRECT_rf_Act( $3, $5 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
    |   CANVAS_C_DRAWROUNDRECT PRN_L RF_ref_sv COM FLOAT_sv COM FLOAT_sv COM PA_ref_sv PRN_R
        { switch (canvascall_DRAWROUNDRECT_Act( $3, $5, $7, $9 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
    |   CANVAS_C_DRAWTEXT PRN_L STRING_sv COM FLOAT_sv COM FLOAT_sv COM PA_ref_sv PRN_R
        { switch (canvascall_DRAWTEXT_Act( $3, $5, $7, $9 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
    |   CANVAS_C_DRAWTEXTONPATH PRN_L STRING_sv COM PT_ref_sv COM FLOAT_sv COM FLOAT_sv COM PA_ref_sv PRN_R
        { switch (canvascall_DRAWTEXTONPATH_Act( $3, $5, $7, $9, $11 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ };

 //Path Initial Calls
pt_initcall:
        PT_INIT_C_FILLTYPE PRN_L filltype_enum PRN_R
        { switch (pt_initcall_FILLTYPE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PT_INIT_C_CLOSE PRN_L PRN_R
        { switch (pt_initcall_CLOSE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PT_INIT_C_CUBICTO PRN_L FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM FLOAT_sv PRN_R
        { switch (pt_initcall_CUBICTO_Act( $3, $5, $7, $9, $11, $13 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PT_INIT_C_RCUBICTO PRN_L FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM FLOAT_sv PRN_R
        { switch (pt_initcall_RCUBICTO_Act( $3, $5, $7, $9, $11, $13 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PT_INIT_C_LINETO PRN_L FLOAT_sv COM FLOAT_sv PRN_R
        { switch (pt_initcall_LINETO_Act( $3, $5 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PT_INIT_C_RLINETO PRN_L FLOAT_sv COM FLOAT_sv PRN_R
        { switch (pt_initcall_RLINETO_Act( $3, $5 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PT_INIT_C_MOVETO PRN_L FLOAT_sv COM FLOAT_sv PRN_R
        { switch (pt_initcall_MOVETO_Act( $3, $5 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PT_INIT_C_RMOVETO PRN_L FLOAT_sv COM FLOAT_sv PRN_R
        { switch (pt_initcall_RMOVETO_Act( $3, $5 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PT_INIT_C_QUADTO PRN_L FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM FLOAT_sv PRN_R
        { switch (pt_initcall_QUADTO_Act( $3, $5, $7, $9 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PT_INIT_C_RQUADTO PRN_L FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM FLOAT_sv PRN_R
        { switch (pt_initcall_RQUADTO_Act( $3, $5, $7, $9 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PT_INIT_C_TRANSFORM PRN_L matrixliteral PRN_R
        { switch (pt_initcall_TRANSFORM_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };

filltype_enum:
        PT_FILLTYPE_EVEN_ODD         { switch (filltype_enum_EVEN_ODD_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PT_FILLTYPE_INVERSE_EVEN_ODD { switch (filltype_enum_INVERSE_EVEN_ODD_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PT_FILLTYPE_INVERSE_WINDING  { switch (filltype_enum_INVERSE_WINDING_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PT_FILLTYPE_WINDING          { switch (filltype_enum_WINDING_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };

 //RactF Modification Calls
rf_modcall:
        RF_MOD_C_INSET PRN_L FLOAT_sv COM FLOAT_sv PRN_R
        { switch (rf_modcall_INSET_Act( $3, $5 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   RF_MOD_C_INTERSECT PRN_L FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM FLOAT_sv PRN_R
        { switch (rf_modcall_INTERSECT_Act( $3, $5, $7, $9 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   RF_MOD_C_OFFSET PRN_L FLOAT_sv COM FLOAT_sv PRN_R
        { switch (rf_modcall_OFFSET_Act( $3, $5 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   RF_MOD_C_SORT PRN_L PRN_R
        { switch (rf_modcall_SORT_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   RF_MOD_C_UNION PRN_L FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM FLOAT_sv PRN_R
        { switch (rf_modcall_UNION_ffff_Act( $3, $5, $7, $9 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   RF_MOD_C_UNION PRN_L FLOAT_sv COM FLOAT_sv PRN_R
        { switch (rf_modcall_UNION_ff_Act( $3, $5 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } } ;

 // Paint Font Initial Calls
pa_fntcall:
        PA_FNT_C_HINTING PRN_L BOOL_sv PRN_R
        { switch (pa_fntcall_HINTING_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PA_FNT_C_ALIGN PRN_L pa_align_enum PRN_R
        { switch (pa_fntcall_TEXTALIGN_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PA_FNT_C_SCALEX PRN_L FLOAT_sv PRN_R
        { switch (pa_fntcall_TEXTSCALEX_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PA_FNT_C_SIZE PRN_L FLOAT_sv PRN_R
        { switch (pa_fntcall_TEXTSIZE_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PA_FNT_C_SKEWX PRN_L FLOAT_sv PRN_R
        { switch (pa_fntcall_TEXTSKEWX_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PA_FNT_C_TYPEFACE PRN_L typeface PRN_R 
        { switch (pa_fntcall_SETTYPEFACE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } } ;
pa_align_enum:
        PA_ALIGN_CENTER  { switch (pa_align_enum_CENTER_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PA_ALIGN_LEFT    { switch (pa_align_enum_LEFT_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PA_ALIGN_RIGHT   { switch (pa_align_enum_RIGHT_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
typeface:
        PA_TYPEFACE_DEFAULT       { switch (typeface_DEFAULT_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PA_TYPEFACE_DEFAULT_BOLD  { switch (typeface_DEFAULT_BOLD_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PA_TYPEFACE_MONOSPACE     { switch (typeface_MONOSPACE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PA_TYPEFACE_SANS_SERIF    { switch (typeface_SANS_SERIF_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PA_TYPEFACE_SERIF         { switch (typeface_SERIF_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } } ;

 // Paint Stroke Initial Calls
pa_strkcall:
        PA_STRK_C_CAP PRN_L cap_enum PRN_R
        { switch (pa_strkcall_STROKECAP_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PA_STRK_C_JOIN PRN_L join_enum PRN_R
        { switch (pa_strkcall_STROKEJOIN_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PA_STRK_C_MITER PRN_L FLOAT_sv PRN_R
        { switch (pa_strkcall_STROKEMITER_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PA_STRK_C_WIDTH PRN_L FLOAT_sv PRN_R
        { switch (pa_strkcall_STROKEWIDTH_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PA_STRK_C_PATHEFFECT PRN_L PE_ref_sv PRN_R
        { switch (pa_strkcall_PATHEFFECT_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ } ;
cap_enum:
        PA_CAP_BUTT   { switch (cap_enum_BUTT_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PA_CAP_ROUND  { switch (cap_enum_ROUND_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PA_CAP_SQUARE { switch (cap_enum_SQUARE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
join_enum:
        PA_JOIN_BEVEL { switch (join_enum_BEVEL_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PA_JOIN_MITER { switch (join_enum_MITER_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PA_JOIN_ROUND { switch (join_enum_ROUND_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };

 // Paint Initial Declarations
pa_fnt_init:
    DEC_SPEC_FONT BR_L pa_fntcalls BR_R { switch (pa_fnt_init_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
pa_clr_init:
    DEC_SPEC_COLOR BR_L color_cnst_sv BR_R { switch (pa_clr_init_Act( $3 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
pa_pathmode_init:
    DEC_SPEC_PATHMODE BR_L pa_style_enum BR_R { switch (pa_pathmode_init_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
pa_stroke_init:
    DEC_SPEC_STROKE BR_L pa_strkcalls BR_R { switch (pa_stroke_init_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
pa_fill_init:
    DEC_SPEC_FILL BR_L shader BR_R { switch (pa_fill_init_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
pa_cmstmd_init:
    DEC_SPEC_COMPOSITEMODE BR_L xfermode BR_R { switch (pa_cmstmd_init_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
pa_mf_init:
    DEC_SPEC_MASKFILTER BR_L maskfilter BR_R { switch (pa_mf_init_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
pa_cf_init:
    DEC_SPEC_COLORFILTER BR_L colorfilter BR_R { switch (pa_cf_init_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
pa_fntcalls:
        %empty
    |   pa_fntcall SMI
    |   pa_fntcalls pa_fntcall SMI ;
pa_style_enum:
        PA_STYLE_FILL             { switch (pa_style_enum_FILL_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PA_STYLE_FILL_AND_STROKE  { switch (pa_style_enum_FILL_AND_STROKE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   PA_STYLE_STROKE           { switch (pa_style_enum_STROKE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
pa_strkcalls:
        %empty
    |   pa_strkcall SMI
    |   pa_strkcalls pa_strkcall SMI ;

 //PExSet
pexSet:
    PE_ref_sv DOT_SET 
	{ switch (pexSet_midAct( $1 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/} 
	BR_L pe_element BR_R { switch (pexSet_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } } ;

 
 //PTxInitial
ptxInitial:
    PT_ref_sv DOT_INITIAL
    { switch (ptxInitial_midAct( $1 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
    BR_L pt_initcalls BR_R { switch (ptxInitial_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
pt_initcalls:
        %empty
    |   pt_initcall SMI
    |   pt_initcalls pt_initcall SMI ;

 //PIxRecord
pixRecord:
    PI_ref_sv DOT_RECORD PRN_L INT_sv COM INT_sv PRN_R 
	{ switch (pixRecord_midAct( $1, $4, $6 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ } 
	BR_L canvascalls BR_R { switch (pixRecord_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
canvascalls:
        %empty
    |   canvascall SMI
    |   canvascalls canvascall SMI ;

 //BMxComposite
bmxComposite:
    BM_ref_sv DOT_COMPOSITE 
	{ switch (bmxComposite_midAct( $1 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
	BR_L canvascalls BR_R { switch (bmxComposite_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };

 //RFxMod
rfxMod:
    RF_ref_sv DOT_MOD
    { switch (rfxMod_midAct( $1 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
    BR_L rf_modcalls BR_R { switch (rfxMod_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
rf_modcalls:
        %empty
    |   rf_modcall SMI
    |   rf_modcalls rf_modcall SMI ;
 //PAxInitial
paxInitial:
    PA_ref_sv DOT_INITIAL
    { switch (paxInitial_midAct( $1 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
    BR_L pa_init_decls BR_R { switch (paxInitial_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
pa_init_decl :
        pa_fnt_init
    |   pa_clr_init
    |   pa_pathmode_init
    |   pa_stroke_init
    |   pa_fill_init
    |   pa_cmstmd_init
    |   pa_mf_init
    |   pa_cf_init ;
pa_init_decls:
        %empty
    |   pa_init_decl
    |   pa_init_decls pa_init_decl ;

 // Allocations
alloc:
        NEW PE_ref_sv PRN_L PRN_R SMI
		{ switch (alloc_PE_ref_sv_Act( $2 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   NEW PT_ref_sv PRN_L PRN_R SMI 
		{ switch (alloc_PT_ref_sv_Act( $2 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   NEW PI_ref_sv PRN_L PRN_R SMI
		{ switch (alloc_PI_ref_sv_Act( $2 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   NEW BM_ref_sv PRN_L INT_sv COM INT_sv PRN_R SMI
		{ switch (alloc_BM_ref_sv_Act( $2, $4, $6 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   NEW PA_ref_sv PRN_L paint_flags_sv PRN_R SMI 
		{ switch (alloc_PA_ref_sv_Act( $2, $4)) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
    |   NEW RF_ref_sv PRN_L FLOAT_sv COM FLOAT_sv COM FLOAT_sv COM FLOAT_sv PRN_R SMI 
		{ switch (alloc_RF_ref_sv_Act( $2, $4, $6, $8, $10 )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } } ;
paint_flags_sv:  //xcpt
        %empty                    { $$ = paint_flags_sv_empty_Act();  }
    |   paint_flag_sv                { $$ = paint_flags_sv_PF_Act($1); }
    |   paint_flags_sv OR paint_flag_sv { $$ = paint_flags_sv_PFs_PF_Act( $1, $3 ); }
paint_flag_sv:  //xcpt
        PA_FLAG_ANTI_ALIAS       { $$ = paint_flag_sv_ANTI_ALIAS_Act(); }
    |   PA_FLAG_DITHER           { $$ = paint_flag_sv_DITHER_Act(); }
    |   PA_FLAG_FAKE_BOLD_TEXT   { $$ = paint_flag_sv_FAKE_BOLD_TEXT_Act(); }
    |   PA_FLAG_FILTER_BITMAP    { $$ = paint_flag_sv_FILTER_BITMAP_Act(); }
    |   PA_FLAG_LINEAR_TEXT      { $$ = paint_flag_sv_LINEAR_TEXT_Act(); }
    |   PA_FLAG_STRIKE_THRU_TEXT { $$ = paint_flag_sv_STRIKE_THRU_TEXT_Act(); }
    |   PA_FLAG_SUBPIXEL_TEXT    { $$ = paint_flag_sv_SUBPIXEL_TEXT_Act(); }
    |   PA_FLAG_UNDERLINE_TEXT   { $$ = paint_flag_sv_UNDERLINE_TEXT_Act(); } ;

 // Declaration Block
declarations:
    DEC_SPEC_DECLARATION 
	{ switch (declarations_midAct()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
	BR_L declaration_content BR_R { switch (declarations_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };
declaration_content:
        %empty
    |   declaration_item
    |   declaration_content declaration_item ;
declaration_item:
        alloc
    |   pexSet
    |   ptxInitial
    |   pixRecord
    |   bmxComposite
    |   rfxMod
    |   paxInitial ;

 // onDraw Block
onDraw:
    DEC_SPEC_ONDRAW 
	{ switch (onDraw_midAct()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
	BR_L canvascalls BR_R { switch (onDraw_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } };

 // Body
body:
    DEC_SPEC_BODY BR_L declarations onDraw BR_R { LOGI_puts("body"); };

%%

//Epilogue
/* Called by yyparse on error.  */
void
yyerror (char const *s)
{
    //fprintf (stderr, "yyerror: %s\n", s);
	// debug
	LOGE_printf("\nyyerror: %s\n", s);
	// debug end
}

