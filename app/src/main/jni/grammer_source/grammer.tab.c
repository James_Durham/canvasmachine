/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 4 "grammer.y" /* yacc.c:339  */

#include "lex.yy.h"

#include "../c_side/inside_actions.h"
#include "../egress/outside_actions.h"

// debug
#include "../utils/inline_LOGx.h"
// debug end

void yyerror (char const *);

#define SUCCESS  0
#define RNA      1
#define OTHER    2



#line 85 "grammer.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "grammer.tab.h".  */
#ifndef YY_YY_GRAMMER_TAB_H_INCLUDED
# define YY_YY_GRAMMER_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif
/* "%code requires" blocks.  */
#line 1 "grammer.y" /* yacc.c:355  */

#include "../c_side/util/matix_const.h"

#line 119 "grammer.tab.c" /* yacc.c:355  */

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    DIGIT_sv = 258,
    INT_sv = 259,
    FLOAT_sv = 260,
    BOOL_sv = 261,
    STRING_sv = 262,
    BR_L = 263,
    BR_R = 264,
    SQ_L = 265,
    SQ_R = 266,
    PRN_L = 267,
    PRN_R = 268,
    SMI = 269,
    COM = 270,
    OR = 271,
    NEW = 272,
    PE = 273,
    BM = 274,
    PA = 275,
    RF = 276,
    PT = 277,
    PI = 278,
    FA = 279,
    FBA = 280,
    FQA = 281,
    CCA = 282,
    COLOR_S_RED = 283,
    COLOR_S_BLUE = 284,
    COLOR_S_GREEN = 285,
    COLOR_S_BLACK = 286,
    COLOR_S_WHITE = 287,
    COLOR_S_GRAY = 288,
    COLOR_S_CYAN = 289,
    COLOR_S_MAGENTA = 290,
    COLOR_S_YELLOW = 291,
    COLOR_S_LIGHTGRAY = 292,
    COLOR_S_DARKGRAY = 293,
    COLOR_S_GREY = 294,
    COLOR_S_LIGHTGREY = 295,
    COLOR_S_DARKGREY = 296,
    COLOR_S_AQUA = 297,
    COLOR_S_FUCHSIA = 298,
    COLOR_S_LIME = 299,
    COLOR_S_MAROON = 300,
    COLOR_S_NAVY = 301,
    COLOR_S_OLIVE = 302,
    COLOR_S_PURPLE = 303,
    COLOR_S_SILVER = 304,
    COLOR_S_TEAL = 305,
    COLOR_S_TRANSPARENT = 306,
    COLOR_HSV = 307,
    COLOR_ARGB = 308,
    PE_ELEMENT_COMPOSE = 309,
    PE_ELEMENT_CORNER = 310,
    PE_ELEMENT_DASH = 311,
    PE_ELEMENT_DISCRETE = 312,
    PE_ELEMENT_PATHDASH = 313,
    PE_ELEMENT_SUM = 314,
    SHADER_BITMAP = 315,
    SHADER_COMPOSE = 316,
    SHADER_LINEAR = 317,
    SHADER_RADIAL = 318,
    SHADER_SWEEP = 319,
    XFERMODE_AVOID = 320,
    XFERMODE_PIXELXOR = 321,
    XFERMODE_PORTERDUFF = 322,
    MASKFILTER_BLUR = 323,
    MASKFILTER_EMBOSS = 324,
    COLOR_FILTER_COLORMATRIX = 325,
    COLOR_FILTER_LIGHTING = 326,
    COLOR_FILTER_PORTERDUFF = 327,
    COLORMATRIX_SCALE = 328,
    COLORMATRIX_SATURATION = 329,
    COLORMATRIX_YUV2RGB = 330,
    COLORMATRIX_RGB2YUV = 331,
    MATRIXLITERAL_ROTATE = 332,
    MATRIXLITERAL_SCALE = 333,
    MATRIXLITERAL_SKEW = 334,
    MATRIXLITERAL_TRANSLATE = 335,
    MATRIXLITERAL_MULTIPLY = 336,
    DOT_SET = 337,
    DOT_INITIAL = 338,
    DOT_RECORD = 339,
    DOT_COMPOSITE = 340,
    DOT_MOD = 341,
    CANVAS_C_CLIPPATH = 342,
    CANVAS_C_CLIPRECT = 343,
    CANVAS_C_SAVE = 344,
    CANVAS_C_RESTORE = 345,
    CANVAS_C_ROTATE = 346,
    CANVAS_C_SCALE = 347,
    CANVAS_C_SKEW = 348,
    CANVAS_C_TRANSLATE = 349,
    CANVAS_C_DRAWARC = 350,
    CANVAS_C_DRAWBITMAP = 351,
    CANVAS_C_DRAWBITMAPMESH = 352,
    CANVAS_C_DRAWCIRCLE = 353,
    CANVAS_C_DRAWCOLOR = 354,
    CANVAS_C_DRAWLINES = 355,
    CANVAS_C_DRAWOVAL = 356,
    CANVAS_C_DRAWPATH = 357,
    CANVAS_C_DRAWPICTURE = 358,
    CANVAS_C_DRAWPOINTS = 359,
    CANVAS_C_DRAWRECT = 360,
    CANVAS_C_DRAWROUNDRECT = 361,
    CANVAS_C_DRAWTEXT = 362,
    CANVAS_C_DRAWTEXTONPATH = 363,
    PT_INIT_C_FILLTYPE = 364,
    PT_INIT_C_CLOSE = 365,
    PT_INIT_C_CUBICTO = 366,
    PT_INIT_C_RCUBICTO = 367,
    PT_INIT_C_LINETO = 368,
    PT_INIT_C_RLINETO = 369,
    PT_INIT_C_MOVETO = 370,
    PT_INIT_C_RMOVETO = 371,
    PT_INIT_C_QUADTO = 372,
    PT_INIT_C_RQUADTO = 373,
    PT_INIT_C_TRANSFORM = 374,
    RF_MOD_C_INSET = 375,
    RF_MOD_C_INTERSECT = 376,
    RF_MOD_C_OFFSET = 377,
    RF_MOD_C_SORT = 378,
    RF_MOD_C_UNION = 379,
    PA_FNT_C_HINTING = 380,
    PA_FNT_C_ALIGN = 381,
    PA_FNT_C_SCALEX = 382,
    PA_FNT_C_SIZE = 383,
    PA_FNT_C_SKEWX = 384,
    PA_FNT_C_TYPEFACE = 385,
    PA_STRK_C_CAP = 386,
    PA_STRK_C_JOIN = 387,
    PA_STRK_C_MITER = 388,
    PA_STRK_C_WIDTH = 389,
    PA_STRK_C_PATHEFFECT = 390,
    PA_FLAG_ANTI_ALIAS = 391,
    PA_FLAG_DITHER = 392,
    PA_FLAG_FAKE_BOLD_TEXT = 393,
    PA_FLAG_FILTER_BITMAP = 394,
    PA_FLAG_LINEAR_TEXT = 395,
    PA_FLAG_STRIKE_THRU_TEXT = 396,
    PA_FLAG_SUBPIXEL_TEXT = 397,
    PA_FLAG_UNDERLINE_TEXT = 398,
    PA_STYLE_FILL = 399,
    PA_STYLE_FILL_AND_STROKE = 400,
    PA_STYLE_STROKE = 401,
    PA_TYPEFACE_DEFAULT = 402,
    PA_TYPEFACE_DEFAULT_BOLD = 403,
    PA_TYPEFACE_MONOSPACE = 404,
    PA_TYPEFACE_SANS_SERIF = 405,
    PA_TYPEFACE_SERIF = 406,
    PA_ALIGN_CENTER = 407,
    PA_ALIGN_LEFT = 408,
    PA_ALIGN_RIGHT = 409,
    PA_JOIN_BEVEL = 410,
    PA_JOIN_MITER = 411,
    PA_JOIN_ROUND = 412,
    PA_CAP_BUTT = 413,
    PA_CAP_ROUND = 414,
    PA_CAP_SQUARE = 415,
    PE_PATHDASH_MORPH = 416,
    PE_PATHDASH_ROTATE = 417,
    PE_PATHDASH_TRANSLATE = 418,
    SH_TILE_CLAMP = 419,
    SH_TILE_MIRROR = 420,
    SH_TILE_REPEAT = 421,
    PDM_ADD = 422,
    PDM_CLEAR = 423,
    PDM_DARKEN = 424,
    PDM_DST = 425,
    PDM_DST_ATOP = 426,
    PDM_DST_IN = 427,
    PDM_DST_OUT = 428,
    PDM_DST_OVER = 429,
    PDM_LIGHTEN = 430,
    PDM_MULTIPLY = 431,
    PDM_OVERLAY = 432,
    PDM_SCREEN = 433,
    PDM_SRC = 434,
    PDM_SRC_ATOP = 435,
    PDM_SRC_IN = 436,
    PDM_SRC_OUT = 437,
    PDM_SRC_OVER = 438,
    PDM_XOR = 439,
    XF_AVOID_AVOID = 440,
    XF_AVOID_TARGET = 441,
    MF_BLUR_INNER = 442,
    MF_BLUR_NORMAL = 443,
    MF_BLUR_OUTER = 444,
    MF_BLUR_SOLID = 445,
    PT_FILLTYPE_EVEN_ODD = 446,
    PT_FILLTYPE_INVERSE_EVEN_ODD = 447,
    PT_FILLTYPE_INVERSE_WINDING = 448,
    PT_FILLTYPE_WINDING = 449,
    DEC_SPEC_FONT = 450,
    DEC_SPEC_COLOR = 451,
    DEC_SPEC_PATHMODE = 452,
    DEC_SPEC_STROKE = 453,
    DEC_SPEC_FILL = 454,
    DEC_SPEC_COMPOSITEMODE = 455,
    DEC_SPEC_MASKFILTER = 456,
    DEC_SPEC_COLORFILTER = 457,
    DEC_SPEC_BODY = 458,
    DEC_SPEC_DECLARATION = 459,
    DEC_SPEC_ONDRAW = 460
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 23 "grammer.y" /* yacc.c:355  */

	int i;
	float f;
	char *str;
	matrix *mat;

#line 344 "grammer.tab.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_GRAMMER_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 361 "grammer.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  5
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   975

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  206
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  83
/* YYNRULES -- Number of rules.  */
#define YYNRULES  269
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  890

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   460

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,   171,   172,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
     185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,   197,   198,   199,   200,   201,   202,   203,   204,
     205
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   274,   274,   276,   277,   278,   279,   280,   281,   283,
     285,   287,   290,   291,   292,   293,   294,   295,   296,   297,
     298,   300,   301,   302,   306,   307,   308,   309,   310,   311,
     312,   313,   314,   315,   316,   317,   318,   319,   320,   321,
     322,   323,   324,   325,   326,   327,   328,   329,   331,   332,
     334,   335,   336,   337,   338,   339,   341,   345,   346,   347,
     348,   349,   350,   351,   352,   353,   354,   355,   356,   357,
     358,   359,   360,   361,   362,   367,   369,   371,   373,   375,
     377,   381,   383,   385,   390,   392,   394,   396,   398,   401,
     402,   403,   407,   409,   410,   412,   413,   417,   418,   421,
     422,   423,   424,   428,   430,   432,   435,   437,   439,   440,
     444,   446,   448,   450,   452,   458,   460,   462,   465,   467,
     470,   472,   474,   476,   478,   480,   483,   485,   487,   489,
     491,   493,   495,   497,   499,   501,   503,   505,   507,   509,
     511,   513,   515,   520,   522,   524,   526,   528,   530,   532,
     534,   536,   538,   540,   544,   545,   546,   547,   551,   553,
     555,   557,   559,   561,   566,   568,   570,   572,   574,   576,
     579,   580,   581,   583,   584,   585,   586,   587,   591,   593,
     595,   597,   599,   602,   603,   604,   606,   607,   608,   612,
     614,   616,   618,   620,   622,   624,   626,   628,   629,   630,
     632,   633,   634,   636,   637,   638,   643,   642,   650,   649,
     653,   654,   655,   660,   659,   663,   664,   665,   670,   669,
     676,   675,   679,   680,   681,   685,   684,   688,   689,   690,
     691,   692,   693,   694,   695,   697,   698,   699,   703,   705,
     707,   709,   711,   713,   716,   717,   718,   720,   721,   722,
     723,   724,   725,   726,   727,   732,   731,   735,   736,   737,
     739,   740,   741,   742,   743,   744,   745,   750,   749,   755
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "DIGIT_sv", "INT_sv", "FLOAT_sv",
  "BOOL_sv", "STRING_sv", "\"{\"", "\"}\"", "\"[\"", "\"]\"", "\"(\"",
  "\")\"", "\";\"", "\",\"", "\"|\"", "\"new\"", "\"PE\"", "\"BM\"",
  "\"PA\"", "\"RF\"", "\"PT\"", "\"PI\"", "\"FA\"", "\"FBA\"", "\"FQA\"",
  "\"CCA\"", "\"red\"", "\"blue\"", "\"green\"", "\"black\"", "\"white\"",
  "\"gray\"", "\"cyan\"", "\"magenta\"", "\"yellow\"", "\"lightgray\"",
  "\"darkgray\"", "\"grey\"", "\"lightgrey\"", "\"darkgrey\"", "\"aqua\"",
  "\"fuchsia\"", "\"lime\"", "\"maroon\"", "\"navy\"", "\"olive\"",
  "\"purple\"", "\"silver\"", "\"teal\"", "\"TRANSPARENT\"", "\"HSV\"",
  "\"ARGB\"", "\"Compose\"", "\"Corner\"", "\"Dash\"", "\"Discrete\"",
  "\"PathDash\"", "\"Sum\"", "\"Bitmap\"", "SHADER_COMPOSE", "\"Linear\"",
  "\"Radial\"", "\"Sweep\"", "\"Avoid\"", "\"PixelXOR\"", "\"PorterDuff\"",
  "\"Blur\"", "\"Emboss\"", "\"ColorMatrix\"", "\"Lighting\"",
  "COLOR_FILTER_PORTERDUFF", "\"Scale\"", "\"Saturation\"", "\"YUV2RGB\"",
  "\"RGB2YUV\"", "\"Rotate\"", "MATRIXLITERAL_SCALE", "\"Skew\"",
  "\"Translate\"", "\"Multiply\"", "\".Set\"", "\".Initial\"",
  "\".Record\"", "\".Composite\"", "\".Mod\"", "\"clipPath\"",
  "\"clipRect\"", "\"save\"", "\"restore\"", "\"rotate\"", "\"scale\"",
  "\"skew\"", "\"translate\"", "\"drawArc\"", "\"drawBitmap\"",
  "\"drawBitmapMesh\"", "\"drawCircle\"", "\"drawColor\"", "\"drawLines\"",
  "\"drawOval\"", "\"drawPath\"", "\"drawPicture\"", "\"drawPoints\"",
  "\"drawRect\"", "\"drawRoundRect\"", "\"drawText\"",
  "\"drawTextOnPath\"", "\"FillType\"", "\"close\"", "\"cubicTo\"",
  "\"rCubicTo\"", "\"lineTo\"", "\"rLineTo\"", "\"moveTo\"", "\"rMoveTo\"",
  "\"quadTo\"", "\"rQuadTo\"", "\"transform\"", "\"inset\"",
  "\"intersect\"", "\"offset\"", "\"sort\"", "\"union\"", "\"Hinting\"",
  "\"Align\"", "\"ScaleX\"", "\"Size\"", "\"SkewX\"", "\"Typeface\"",
  "\"Cap\"", "\"Join\"", "\"Miter\"", "\"Width\"", "\"PathEffect\"",
  "\"ANTI_ALIAS_FLAG\"", "\"DITHER_FLAG\"", "\"FAKE_BOLD_TEXT_FLAG\"",
  "\"FILTER_BITMAP_FLAG\"", "\"LINEAR_TEXT_FLAG\"",
  "\"STRIKE_THRU_TEXT_FLAG\"", "\"SUBPIXEL_TEXT_FLAG\"",
  "\"UNDERLINE_TEXT_FLAG\"", "\"FILL\"", "\"FILL_AND_STROKE\"",
  "\"STROKE\"", "\"DEFAULT\"", "\"DEFAULT_BOLD\"", "\"MONOSPACE\"",
  "\"SANS_SERIF\"", "\"SERIF\"", "\"CENTER\"", "\"LEFT\"", "\"RIGHT\"",
  "\"BEVEL\"", "\"MITER\"", "\"ROUND\"", "\"BUTT\"", "PA_CAP_ROUND",
  "\"SQUARE\"", "\"MORPH\"", "\"ROTATE\"", "\"TRANSLATE\"", "\"CLAMP\"",
  "\"MIRROR\"", "\"REPEAT\"", "\"ADD\"", "\"CLEAR\"", "\"DARKEN\"",
  "\"DST\"", "\"DST_ATOP\"", "\"DST_IN\"", "\"DST_OUT\"", "\"DST_OVER\"",
  "\"LIGHTEN\"", "\"MULTIPLY\"", "\"OVERLAY\"", "\"SCREEN\"", "\"SRC\"",
  "\"SRC_ATOP\"", "\"SRC_IN\"", "\"SRC_OUT\"", "\"SRC_OVER\"", "\"XOR\"",
  "\"AVOID\"", "\"TARGET\"", "\"INNER\"", "\"NORMAL\"", "\"OUTER\"",
  "\"SOLID\"", "\"EVEN_ODD\"", "\"INVERSE_EVEN_ODD\"",
  "\"INVERSE_WINDING\"", "\"WINDING\"", "\"Font\"", "\"Color\"",
  "\"PathMode\"", "\"Stroke\"", "\"Fill\"", "\"CompositeMode\"",
  "\"MaskFilter\"", "\"ColorFilter\"", "\"Body\"", "\"Declarations\"",
  "\"onDraw\"", "$accept", "root", "PE_ref_sv", "BM_ref_sv", "PA_ref_sv",
  "RF_ref_sv", "PT_ref_sv", "PI_ref_sv", "float_bin_cnst",
  "float_tri_cnst", "float_quad_cnst", "float_list", "float_bin_list",
  "float_quad_list", "float_array", "float_bin_array", "float_quad_array",
  "color_string_sv", "color_hsv_sv", "color_argb_sv", "color_cnst_sv",
  "color_cnst_list", "color_cnst_array", "pdm_enum", "pe_element",
  "pathdash_style_enum", "shader", "shader_tile", "xfermode",
  "xf_avoid_enum", "maskfilter", "mf_blur_enum", "colorfilter",
  "colormatrix", "matrixliteral", "canvascall", "pt_initcall",
  "filltype_enum", "rf_modcall", "pa_fntcall", "pa_align_enum", "typeface",
  "pa_strkcall", "cap_enum", "join_enum", "pa_fnt_init", "pa_clr_init",
  "pa_pathmode_init", "pa_stroke_init", "pa_fill_init", "pa_cmstmd_init",
  "pa_mf_init", "pa_cf_init", "pa_fntcalls", "pa_style_enum",
  "pa_strkcalls", "pexSet", "$@1", "ptxInitial", "$@2", "pt_initcalls",
  "pixRecord", "$@3", "canvascalls", "bmxComposite", "$@4", "rfxMod",
  "$@5", "rf_modcalls", "paxInitial", "$@6", "pa_init_decl",
  "pa_init_decls", "alloc", "paint_flags_sv", "paint_flag_sv",
  "declarations", "$@7", "declaration_content", "declaration_item",
  "onDraw", "$@8", "body", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,   389,   390,   391,   392,   393,   394,
     395,   396,   397,   398,   399,   400,   401,   402,   403,   404,
     405,   406,   407,   408,   409,   410,   411,   412,   413,   414,
     415,   416,   417,   418,   419,   420,   421,   422,   423,   424,
     425,   426,   427,   428,   429,   430,   431,   432,   433,   434,
     435,   436,   437,   438,   439,   440,   441,   442,   443,   444,
     445,   446,   447,   448,   449,   450,   451,   452,   453,   454,
     455,   456,   457,   458,   459,   460
};
# endif

#define YYPACT_NINF -810

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-810)))

#define YYTABLE_NINF -1

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
    -184,    19,    84,  -810,  -115,  -810,  -810,  -108,   121,  -810,
     127,   253,   134,  -810,     3,   140,   164,   168,   211,   248,
     274,   174,   260,   139,   286,   299,   310,  -810,  -810,  -810,
    -810,  -810,  -810,  -810,    52,  -810,   236,   384,   385,   386,
     387,   388,   389,  -810,  -810,  -810,  -810,  -810,  -810,  -810,
    -810,  -810,  -810,  -810,   390,  -810,  -810,   391,   392,   393,
     394,   395,   396,   397,   398,   399,   400,   401,   402,   403,
     404,   405,   406,   407,   408,   409,   410,   411,   412,   413,
      85,   415,   421,   -84,   424,   417,   418,   425,   426,   427,
     428,   429,   422,   416,    44,   419,   430,   434,   435,   436,
     437,   423,   431,   431,   440,   197,   420,   423,   416,   432,
     433,    45,   423,   441,   442,  -810,  -810,   438,   439,   444,
    -810,  -810,  -810,  -810,  -810,  -810,  -810,  -810,    83,  -810,
     445,   443,   447,    58,   236,  -119,    -1,   237,   448,   449,
     450,   451,  -810,  -810,   155,   452,   453,   454,   455,   456,
     457,   458,  -810,  -810,  -810,  -810,  -810,  -810,  -810,  -810,
    -810,  -810,  -810,  -810,  -810,  -810,  -810,  -810,  -810,  -810,
    -810,  -810,  -810,  -810,  -810,  -810,   462,   463,  -810,  -810,
    -810,   244,   446,   461,   464,   465,   376,   467,   466,   468,
     469,   471,   472,   475,  -810,  -810,   474,   477,   -84,   487,
    -810,  -810,   470,   481,   482,   483,   484,   485,   489,   192,
     491,   492,   493,   494,   495,   496,   497,   498,  -810,  -810,
    -810,  -810,  -810,  -810,  -810,  -810,  -810,    11,   499,   500,
     501,   502,   503,   504,     4,   505,   507,   508,   509,   510,
     511,   512,   513,   514,   515,   516,   517,    42,   506,  -810,
     524,  -810,  -810,   525,   527,   528,   529,   530,   532,   534,
     535,   537,   538,  -810,  -137,   531,   488,   488,   488,  -810,
     423,   533,   488,   539,   488,   541,   542,   416,   526,  -810,
    -810,   536,    58,   543,   433,   544,   416,    58,  -810,  -810,
      69,   197,    18,    86,   297,   137,    35,   191,  -810,  -810,
     545,   547,   548,   546,   549,  -810,  -810,   550,   -60,   552,
     551,   553,   555,   556,   557,   558,   561,   562,   285,  -810,
    -810,   554,   559,   540,   560,   377,   563,   564,   565,   566,
     567,   568,   569,   570,  -810,  -810,  -810,  -810,  -810,  -810,
    -810,  -810,  -810,  -810,  -810,  -810,  -810,  -810,  -810,  -810,
    -810,  -810,   573,   574,  -810,    53,   575,   576,   577,   578,
     582,  -810,    75,   579,   580,   581,   583,   584,   585,   587,
     588,   589,   590,   591,   592,   594,   595,   593,   600,   602,
     603,   604,   605,   607,    20,   609,  -810,  -810,  -810,   613,
     611,   612,   614,   615,   616,   617,     6,   618,   620,   621,
     622,   623,   627,   625,   626,   628,   630,   629,   631,   633,
     632,   634,   635,   636,   610,   637,   638,  -810,   639,  -810,
    -810,  -810,  -810,  -810,   642,  -810,   641,   643,   644,   645,
     646,   647,   648,   649,   653,   654,   655,   656,   657,   658,
    -810,  -810,   597,   624,  -810,   652,  -810,  -810,   665,   667,
     669,   670,   671,   673,  -810,   659,  -810,   531,  -810,  -810,
    -810,  -810,   663,  -810,   533,  -810,   674,  -810,   675,   676,
     677,  -810,   668,    58,  -810,   679,   680,   681,    58,   460,
     221,   682,   683,   684,   220,  -810,  -810,   678,  -810,  -810,
     218,   224,   685,   686,   687,  -810,  -810,   688,   431,   297,
     689,   690,   691,  -810,   197,   197,  -137,  -810,   692,   694,
    -810,    34,   695,   197,  -810,   693,   696,   698,   699,  -810,
     702,   703,   704,   705,   706,   707,   708,   709,   710,   711,
     712,   713,   285,  -810,   640,   714,   715,   716,   717,   719,
     720,   722,   723,   724,   718,  -810,   721,  -810,   725,   726,
     727,   728,   739,   732,   733,   734,   735,   736,   738,  -810,
    -810,  -810,   740,   741,   742,   743,  -810,  -810,  -810,  -810,
    -810,   744,  -810,  -810,  -810,  -810,   745,  -810,  -810,  -810,
     746,   747,   748,   749,  -810,   737,   750,   751,   752,   753,
     754,   757,   758,   759,   767,   760,   761,   764,  -810,  -810,
     765,   762,   766,   769,   768,   771,   380,   770,   772,   773,
     775,   776,   777,   778,   779,   780,   781,   782,   783,   784,
     236,   774,  -810,   786,   572,   488,   697,   488,   787,   796,
     788,   789,   799,   488,   488,   800,   793,  -810,  -810,  -810,
     802,  -810,  -810,  -810,  -810,  -810,  -810,  -810,  -810,  -810,
    -810,  -810,  -810,   219,   297,   803,   804,   805,   797,  -810,
    -810,    78,   798,   806,   807,   809,  -810,   811,  -137,  -810,
     812,  -810,  -810,   813,   814,   815,  -810,  -810,  -810,  -810,
     816,   817,   818,   819,   820,   821,   285,   214,   822,   823,
     801,   824,   586,   825,   826,   827,   828,   829,  -810,   830,
     831,   833,   832,   834,   835,  -810,  -810,  -810,   836,   837,
     838,   839,   840,   841,  -810,  -810,  -810,  -810,   844,   853,
     845,   847,   846,   850,   851,   852,   854,   855,   856,   857,
     858,   859,   860,   861,   864,   865,  -810,  -810,  -810,   488,
    -810,   863,   862,  -810,   874,   876,   866,   488,  -810,  -810,
     488,  -810,   225,   219,  -137,   877,   878,   480,  -123,  -810,
     869,   880,   881,  -810,  -810,  -810,   882,   883,   884,   885,
     886,   887,   888,   889,   890,  -810,  -810,   891,  -810,    76,
     892,   893,   895,   894,   896,   897,  -810,  -810,  -810,   898,
     899,   900,   901,   902,   904,   903,  -810,  -810,   906,   910,
     905,   907,   908,   911,   912,   913,   916,   917,   918,   919,
     920,  -810,  -810,   921,   480,  -810,  -810,   923,  -810,  -810,
    -810,  -810,  -810,   927,   480,   197,   697,  -810,   924,   928,
     933,  -810,  -810,   934,   935,  -810,  -810,  -810,   936,   937,
    -810,   929,  -810,   930,   931,  -810,    87,   932,  -810,   938,
     939,   940,   941,   944,   945,   946,   480,   697,  -810,   197,
     219,  -810,   943,   947,   948,  -810,  -810,   949,   950,   951,
    -810,   954,   955,   956,   957,   488,   697,   219,  -810,  -810,
    -810,  -810,   958,   959,   960,  -810,   219,  -810,   962,  -810
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       0,     0,     0,     2,     0,     1,   255,     0,     0,   267,
       0,   257,     0,   269,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   261,   262,   263,
     264,   265,   266,   260,     0,   258,   215,     0,     0,     0,
       0,     0,     0,     3,     4,     5,     6,     7,     8,   206,
     218,   225,   220,   208,     0,   256,   259,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   244,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   216,   268,     0,     0,     0,
     247,   248,   249,   250,   251,   252,   253,   254,     0,   245,
       0,     0,     0,     0,   215,   235,   222,   210,     0,     0,
       0,     0,   118,   119,     0,     0,     0,     0,     0,     0,
       0,     0,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,     0,     0,    50,    51,
      52,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   217,   238,     0,     0,     0,     0,
     239,   240,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   227,   228,
     229,   230,   231,   232,   233,   234,   236,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   115,
       0,   117,   120,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   130,     0,    18,     0,     0,     0,   136,
       0,    15,     0,     0,     0,     0,     0,     0,     0,   242,
     246,     0,     0,     0,     0,     0,     0,     0,   207,   219,
     197,     0,     0,   203,     0,     0,     0,     0,   226,   237,
       0,     0,     0,     0,     0,   223,   221,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   211,
     209,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,    71,    72,
      73,    74,     0,     0,    19,     0,     0,     0,     0,     0,
       0,    16,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   200,   201,   202,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   161,     0,   224,
     154,   155,   156,   157,     0,   144,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     212,   213,     0,     0,   122,     0,   124,   125,     0,     0,
       0,     0,     0,     0,   131,     0,    23,     0,   132,   133,
     134,   135,     0,    22,     0,   137,     0,   139,     0,     0,
       0,   241,     0,     0,    76,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   198,   189,     0,   190,   191,
       0,     0,     0,     0,     0,   204,   192,     0,     0,     0,
       0,     0,     0,   193,     0,     0,     0,   194,     0,     0,
     195,     0,     0,     0,   196,     0,     0,     0,     0,   143,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   153,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    20,     0,    17,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   170,
     171,   172,     0,     0,     0,     0,   173,   174,   175,   176,
     177,     0,   199,   183,   184,   185,     0,   186,   187,   188,
       0,     0,     0,     0,   205,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   108,   109,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     215,     0,   121,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    75,    77,    78,
       0,    80,   164,   165,   166,   167,   168,   169,   178,   179,
     180,   181,   182,     0,     0,     0,     0,     0,     0,    93,
      94,     0,     0,     0,     0,     0,   103,     0,     0,   158,
       0,   160,   163,     0,     0,     0,   147,   148,   149,   150,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     9,     0,
       0,     0,     0,     0,     0,    89,    90,    91,     0,     0,
       0,     0,     0,     0,    99,   100,   101,   102,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   214,   116,   123,     0,
     127,    12,     0,   129,     0,     0,     0,     0,   140,   141,
       0,   243,     0,     0,     0,     0,     0,     0,     0,    97,
       0,     0,     0,   107,   104,   105,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   113,   114,     0,    13,     0,
       0,     0,     0,     0,     0,     0,    81,    82,    83,     0,
       0,     0,     0,     0,     0,     0,    95,    96,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   126,    21,     0,     0,    48,    49,     0,   138,   142,
      79,    84,    85,     0,     0,    53,     0,    92,     0,     0,
       0,   159,   162,     0,     0,   151,   152,   110,     0,     0,
      14,     0,    11,     0,     0,    54,     0,     0,    10,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    56,     0,
       0,    98,     0,     0,     0,   111,   112,     0,     0,     0,
      55,     0,     0,     0,     0,     0,     0,     0,    88,   106,
     145,   146,     0,     0,     0,   128,     0,    87,     0,    86
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -810,  -810,    -9,   -10,   -14,    -6,    -8,     2,   -17,  -810,
      -3,  -810,  -810,  -810,  -809,   167,  -810,  -810,  -810,  -810,
    -289,  -810,  -796,  -499,  -273,  -810,  -489,  -742,  -810,  -810,
    -810,  -810,  -810,  -810,  -520,   -79,   262,  -810,   282,   173,
    -810,  -810,   175,  -810,  -810,  -810,  -810,  -810,  -810,  -810,
    -810,  -810,  -810,  -810,  -810,  -810,  -810,  -810,  -810,  -810,
    -810,  -810,  -810,  -131,  -810,  -810,  -810,  -810,  -810,  -810,
    -810,   342,  -810,  -810,  -810,   375,  -810,  -810,  -810,   666,
    -810,  -810,  -810
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,    21,    22,    23,    24,    25,    26,   361,   595,
     354,   779,   362,   355,   693,   188,   183,   178,   179,   180,
     181,   846,   795,   352,   208,   789,   402,   708,   406,   798,
     409,   718,   413,   600,   439,    79,   246,   424,   233,   383,
     562,   571,   395,   576,   580,   218,   219,   220,   221,   222,
     223,   224,   225,   384,   389,   396,    27,    87,    28,    91,
     247,    29,   534,    80,    30,    88,    31,    90,   234,    32,
      89,   226,   227,    33,   128,   129,     7,     8,    34,    35,
      10,    12,     3
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint16 yytable[] =
{
      39,   117,   385,   209,    38,    37,    41,   592,    40,   371,
     586,   790,   619,   306,   376,   496,    42,   847,   841,     1,
     298,    15,    16,    17,    18,    19,    20,     4,   844,   486,
     334,   335,   336,   337,   338,   339,   340,   341,   342,   343,
     344,   345,   346,   347,   348,   349,   350,   351,   869,   140,
     189,   320,   120,   121,   122,   123,   124,   125,   126,   127,
     868,    55,   796,   797,   456,    18,    18,   883,   457,    14,
      15,    16,    17,    18,    19,    20,   210,   211,   212,   213,
     214,   215,   216,   217,     5,   139,   463,   812,   141,     6,
     464,   813,   149,   150,   116,   148,   197,     9,   858,   198,
     185,   184,   859,   407,   408,   190,   191,   596,   597,   598,
     599,   186,   202,   203,   204,   205,   206,   207,   871,   228,
     229,   230,   231,   232,   228,   229,   230,   231,   232,    11,
     117,   420,   421,   422,   423,   884,    13,   390,   391,   392,
     393,   394,    36,    43,   888,   377,   378,   379,   380,   381,
     382,   235,   236,   237,   238,   239,   240,   241,   242,   243,
     244,   245,   386,   387,   388,   709,   735,    44,   252,   724,
     253,    45,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,   377,   378,   379,   380,   381,   382,
     553,   289,   403,   404,   405,   557,   210,   211,   212,   213,
     214,   215,   216,   217,    46,   590,   591,   390,   391,   392,
     393,   394,    51,   736,   602,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   164,   165,   166,
     167,   168,   169,   170,   171,   172,   173,   174,   175,   176,
     177,    47,   356,   357,   358,   791,    49,   263,   363,   264,
     365,   410,   411,   412,   359,   714,   715,   716,   717,   368,
      14,    15,    16,    17,    18,    19,    20,    48,   375,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    50,   235,   236,   237,   238,
     239,   240,   241,   242,   243,   244,   245,   397,   398,   399,
     400,   401,   434,   435,   436,   437,   438,   566,   567,   568,
     569,   570,    52,   559,   560,   561,   573,   574,   575,   577,
     578,   579,    53,   705,   706,   707,   786,   787,   788,   269,
     444,   270,   445,   672,    54,   673,    81,    82,    83,    84,
      85,    86,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   119,   138,   115,   118,   130,
     131,   132,   142,   133,   134,   135,   136,   137,    19,   144,
     145,   146,   147,   143,    18,   151,   182,   547,   192,   193,
      16,   373,   194,   195,   545,    20,   265,   200,   187,   196,
     199,   201,   249,   248,   251,   250,   558,   254,   255,   256,
     257,   258,   259,   260,   261,   262,   266,   271,   278,   267,
     268,   272,   282,   273,   274,   583,   275,   276,   585,   687,
     277,   279,   281,   283,   284,   285,   286,   287,   288,   290,
     291,   292,   293,   294,   295,   296,   297,   794,    17,   321,
     322,   300,   301,   302,   303,   304,   307,   308,   305,   309,
     310,   311,   312,   313,   314,   315,   316,   317,   318,   323,
     324,   319,   325,   326,   327,   328,   845,   329,   330,   369,
     331,   332,   333,   353,   364,   360,   366,   367,   372,   374,
     414,   370,   415,   416,   418,   442,   426,   487,   427,   417,
     428,   429,   430,   431,   419,   425,   432,   433,   440,   299,
     870,   497,   441,   280,     0,   443,   446,   447,   690,   455,
     448,   449,   450,   451,   452,   453,   454,   462,   458,   459,
     460,   461,   465,   472,   467,   466,   741,     0,   468,   469,
     470,   471,   535,   474,   473,   479,   475,   476,   117,   477,
     478,   691,   480,   694,   481,   482,   483,   484,   488,   700,
     701,   485,   489,   490,   491,   515,   492,   493,   494,   536,
     498,   495,   499,   500,   501,   502,   503,   504,   505,   507,
     506,   508,   510,   509,   511,   514,   512,   513,   620,     0,
       0,     0,   516,   517,   518,   519,   520,   537,   521,   522,
     523,   524,   525,   526,   527,   528,   529,   530,   531,   532,
     538,   533,   539,   540,   544,   541,   542,   543,   546,   548,
     549,   550,   551,   552,   554,   555,   556,   563,   564,   565,
     581,   582,   572,     0,   587,   588,   589,   593,   603,   601,
      56,   604,   584,   605,   606,    15,   594,   607,   608,   609,
     610,   611,   612,   613,   614,   615,   616,   617,   618,     0,
       0,   692,     0,   630,     0,   777,   631,     0,   622,   621,
       0,   623,   624,   784,   625,   626,   785,   627,   628,   629,
     632,   633,   634,   635,   636,   637,   638,   639,     0,   641,
     640,   642,   653,   643,   644,   645,   646,   647,   648,   649,
     650,   651,   652,     0,     0,   654,   655,   656,   657,   658,
     659,   660,   662,   664,   661,   663,   665,   667,   666,   688,
       0,   668,   669,   670,   671,   674,   676,   675,   677,   678,
     679,   689,   695,   680,   681,   682,   683,   684,   685,   686,
     696,   713,   698,   697,   699,   702,   703,   704,   710,   711,
     712,   720,   721,   719,   722,   723,   739,   725,   726,   727,
     728,   729,   730,   731,   732,   733,   734,     0,     0,     0,
       0,     0,     0,     0,   746,   737,   738,   740,     0,   743,
     742,     0,   744,   745,   748,   747,   749,   750,   751,     0,
     752,   753,   754,   755,   756,   757,   758,   759,   760,   763,
     761,   882,   762,   764,   765,     0,   780,   766,   778,   767,
     768,   769,   770,   771,   772,   773,   774,   775,   776,   781,
     782,   783,   792,   793,   799,   800,   801,   802,   803,   804,
     805,   806,   807,   808,   809,   810,     0,     0,     0,   817,
       0,     0,     0,     0,   811,     0,   815,   814,   816,   818,
     819,   820,   821,   822,   825,   828,   823,   824,   826,   827,
     829,   831,   830,     0,   832,     0,   840,   833,   834,   835,
     836,   837,   843,   849,   838,   839,   842,   848,   850,   851,
     852,   853,   854,     0,   855,   856,   857,   860,   872,     0,
     867,   861,   873,   874,   862,   863,   864,   865,   866,     0,
       0,     0,     0,     0,   875,   876,   877,   878,   879,   880,
     881,   885,     0,   887,   886,   889
};

static const yytype_int16 yycheck[] =
{
      14,    80,   291,   134,    14,    14,    14,   506,    14,   282,
     499,   753,   532,     9,   287,     9,    14,   826,   814,   203,
       9,    18,    19,    20,    21,    22,    23,     8,   824,     9,
     167,   168,   169,   170,   171,   172,   173,   174,   175,   176,
     177,   178,   179,   180,   181,   182,   183,   184,   857,     5,
       5,     9,   136,   137,   138,   139,   140,   141,   142,   143,
     856,     9,   185,   186,    11,    21,    21,   876,    15,    17,
      18,    19,    20,    21,    22,    23,   195,   196,   197,   198,
     199,   200,   201,   202,     0,    93,    11,    11,    94,   204,
      15,    15,   102,   103,     9,   101,    13,   205,    11,    16,
     108,   107,    15,    68,    69,   111,   112,    73,    74,    75,
      76,   109,    54,    55,    56,    57,    58,    59,   860,   120,
     121,   122,   123,   124,   120,   121,   122,   123,   124,     8,
     209,   191,   192,   193,   194,   877,     9,   131,   132,   133,
     134,   135,     8,     3,   886,   125,   126,   127,   128,   129,
     130,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   144,   145,   146,   654,   686,     3,    13,   668,
      15,     3,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   125,   126,   127,   128,   129,   130,
     473,     9,    65,    66,    67,   478,   195,   196,   197,   198,
     199,   200,   201,   202,     3,   504,   505,   131,   132,   133,
     134,   135,    83,     9,   513,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,     3,   266,   267,   268,   754,    82,    13,   272,    15,
     274,    70,    71,    72,   270,   187,   188,   189,   190,   277,
      17,    18,    19,    20,    21,    22,    23,     3,   286,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,    85,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,    60,    61,    62,
      63,    64,    77,    78,    79,    80,    81,   147,   148,   149,
     150,   151,    86,   152,   153,   154,   158,   159,   160,   155,
     156,   157,    83,   164,   165,   166,   161,   162,   163,    13,
      13,    15,    15,    13,    84,    15,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,     4,     4,    14,    13,     5,
      13,    13,    13,     8,     8,     8,     8,     8,    22,     5,
       5,     5,     5,    13,    21,     5,    26,   464,     7,     7,
      19,   284,    14,    14,   457,    23,    10,    14,    25,    15,
      15,    14,    13,    15,    13,    15,     6,    15,    15,    15,
      15,    15,    15,    15,    12,    12,    15,    10,     4,    15,
      15,    15,    12,    15,    15,   494,    15,    15,   498,   620,
      15,    14,     5,    12,    12,    12,    12,    12,     9,     8,
       8,     8,     8,     8,     8,     8,     8,    27,    20,   247,
       4,    12,    12,    12,    12,    12,   234,    12,    14,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,     5,
       5,    14,     5,     5,     5,     5,   825,     5,     4,    13,
       5,     4,     4,    12,     5,    12,     5,     5,     5,     5,
       5,    15,     5,     5,     5,    15,     5,   384,     5,    13,
       5,     5,     5,     5,    14,    13,     5,     5,    14,   227,
     859,   396,    13,   198,    -1,    15,    13,    13,     6,     5,
      15,    15,    15,    15,    15,    15,    13,     5,    13,    13,
      13,    13,    13,     5,    13,    15,    10,    -1,    15,    15,
      15,    14,     5,    13,    15,    12,    15,    15,   687,    15,
      15,   625,    12,   627,    12,    12,    12,    12,     9,   633,
     634,    14,     9,    12,    12,    15,    12,    12,    12,     5,
      12,    14,    12,    12,    12,    12,     9,    12,    12,     9,
      12,    12,     9,    12,    12,     9,    12,    12,     8,    -1,
      -1,    -1,    15,    15,    15,    13,    15,     5,    15,    15,
      15,    15,    15,    15,    15,    12,    12,    12,    12,    12,
       5,    13,     5,     4,    15,     5,     5,     4,    15,     5,
       5,     5,     5,    15,     5,     5,     5,     5,     5,     5,
       5,     5,    14,    -1,     5,     5,     5,     5,     5,     4,
      34,     5,    14,     5,     5,    18,    12,     5,     5,     5,
       5,     5,     5,     5,     5,     5,     5,     5,     5,    -1,
      -1,    24,    -1,     5,    -1,   739,     5,    -1,    13,    15,
      -1,    15,    15,   747,    15,    15,   750,    15,    15,    15,
      15,    15,    15,    15,     5,    13,    13,    13,    -1,    13,
      15,    13,    15,    13,    13,    13,    13,    13,    13,    13,
      13,    13,    13,    -1,    -1,    15,    15,    15,    15,    15,
      13,    13,     5,    12,    15,    15,    12,    15,    13,     5,
      -1,    15,    13,    15,    13,    15,    13,    15,    13,    13,
      13,     5,     5,    15,    15,    15,    15,    15,    15,    15,
       4,     4,    13,    15,     5,     5,    13,     5,     5,     5,
       5,     5,     5,    15,     5,     4,    15,     5,     5,     5,
       5,     5,     5,     5,     5,     5,     5,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,     5,    13,    13,    13,    -1,    13,
      15,    -1,    15,    15,    13,    15,    13,    15,    14,    -1,
      15,    15,    15,    15,    15,    15,    15,    13,     5,    13,
      15,   875,    15,    13,    13,    -1,     4,    15,     5,    15,
      15,    15,    15,    15,    15,    15,    15,    13,    13,     5,
       4,    15,     5,     5,    15,     5,     5,     5,     5,     5,
       5,     5,     5,     5,     5,     5,    -1,    -1,    -1,     5,
      -1,    -1,    -1,    -1,    13,    -1,    13,    15,    13,    13,
      13,    13,    13,    13,    10,     5,    15,    15,    15,    13,
      15,    13,    15,    -1,    13,    -1,     5,    15,    15,    13,
      13,    13,     5,     5,    15,    15,    13,    13,     5,     5,
       5,     5,     5,    -1,    15,    15,    15,    15,     5,    -1,
       4,    13,     5,     5,    15,    15,    15,    13,    13,    -1,
      -1,    -1,    -1,    -1,    15,    15,    15,    13,    13,    13,
      13,    13,    -1,    13,    15,    13
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint16 yystos[] =
{
       0,   203,   207,   288,     8,     0,   204,   282,   283,   205,
     286,     8,   287,     9,    17,    18,    19,    20,    21,    22,
      23,   208,   209,   210,   211,   212,   213,   262,   264,   267,
     270,   272,   275,   279,   284,   285,     8,   208,   209,   210,
     211,   212,   213,     3,     3,     3,     3,     3,     3,    82,
      85,    83,    86,    83,    84,     9,   285,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   241,
     269,    12,    12,    12,    12,    12,    12,   263,   271,   276,
     273,   265,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    14,     9,   241,    13,     4,
     136,   137,   138,   139,   140,   141,   142,   143,   280,   281,
       5,    13,    13,     8,     8,     8,     8,     8,     4,   212,
       5,   211,    13,    13,     5,     5,     5,     5,   211,   209,
     209,     5,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,   223,   224,
     225,   226,    26,   222,   211,   212,   213,    25,   221,     5,
     211,   211,     7,     7,    14,    14,    15,    13,    16,    15,
      14,    14,    54,    55,    56,    57,    58,    59,   230,   269,
     195,   196,   197,   198,   199,   200,   201,   202,   251,   252,
     253,   254,   255,   256,   257,   258,   277,   278,   120,   121,
     122,   123,   124,   244,   274,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   242,   266,    15,    13,
      15,    13,    13,    15,    15,    15,    15,    15,    15,    15,
      15,    12,    12,    13,    15,    10,    15,    15,    15,    13,
      15,    10,    15,    15,    15,    15,    15,    15,     4,    14,
     281,     5,    12,    12,    12,    12,    12,    12,     9,     9,
       8,     8,     8,     8,     8,     8,     8,     8,     9,   277,
      12,    12,    12,    12,    12,    14,     9,   244,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    14,
       9,   242,     4,     5,     5,     5,     5,     5,     5,     5,
       4,     5,     4,     4,   167,   168,   169,   170,   171,   172,
     173,   174,   175,   176,   177,   178,   179,   180,   181,   182,
     183,   184,   229,    12,   216,   219,   210,   210,   210,   211,
      12,   214,   218,   210,     5,   210,     5,     5,   212,    13,
      15,   230,     5,   221,     5,   212,   230,   125,   126,   127,
     128,   129,   130,   245,   259,   226,   144,   145,   146,   260,
     131,   132,   133,   134,   135,   248,   261,    60,    61,    62,
      63,    64,   232,    65,    66,    67,   234,    68,    69,   236,
      70,    71,    72,   238,     5,     5,     5,    13,     5,    14,
     191,   192,   193,   194,   243,    13,     5,     5,     5,     5,
       5,     5,     5,     5,    77,    78,    79,    80,    81,   240,
      14,    13,    15,    15,    13,    15,    13,    13,    15,    15,
      15,    15,    15,    15,    13,     5,    11,    15,    13,    13,
      13,    13,     5,    11,    15,    13,    15,    13,    15,    15,
      15,    14,     5,    15,    13,    15,    15,    15,    15,    12,
      12,    12,    12,    12,    12,    14,     9,   245,     9,     9,
      12,    12,    12,    12,    12,    14,     9,   248,    12,    12,
      12,    12,    12,     9,    12,    12,    12,     9,    12,    12,
       9,    12,    12,    12,     9,    15,    15,    15,    15,    13,
      15,    15,    15,    15,    15,    15,    15,    15,    12,    12,
      12,    12,    12,    13,   268,     5,     5,     5,     5,     5,
       4,     5,     5,     4,    15,   216,    15,   214,     5,     5,
       5,     5,    15,   230,     5,     5,     5,   230,     6,   152,
     153,   154,   246,     5,     5,     5,   147,   148,   149,   150,
     151,   247,    14,   158,   159,   160,   249,   155,   156,   157,
     250,     5,     5,   208,    14,   209,   232,     5,     5,     5,
     226,   226,   229,     5,    12,   215,    73,    74,    75,    76,
     239,     4,   226,     5,     5,     5,     5,     5,     5,     5,
       5,     5,     5,     5,     5,     5,     5,     5,     5,   240,
       8,    15,    13,    15,    15,    15,    15,    15,    15,    15,
       5,     5,    15,    15,    15,    15,     5,    13,    13,    13,
      15,    13,    13,    13,    13,    13,    13,    13,    13,    13,
      13,    13,    13,    15,    15,    15,    15,    15,    15,    13,
      13,    15,     5,    15,    12,    12,    13,    15,    15,    13,
      15,    13,    13,    15,    15,    15,    13,    13,    13,    13,
      15,    15,    15,    15,    15,    15,    15,   269,     5,     5,
       6,   210,    24,   220,   210,     5,     4,    15,    13,     5,
     210,   210,     5,    13,     5,   164,   165,   166,   233,   232,
       5,     5,     5,     4,   187,   188,   189,   190,   237,    15,
       5,     5,     5,     4,   229,     5,     5,     5,     5,     5,
       5,     5,     5,     5,     5,   240,     9,    13,    13,    15,
      13,    10,    15,    13,    15,    15,     5,    15,    13,    13,
      15,    14,    15,    15,    15,    15,    15,    15,    15,    13,
       5,    15,    15,    13,    13,    13,    15,    15,    15,    15,
      15,    15,    15,    15,    15,    13,    13,   210,     5,   217,
       4,     5,     4,    15,   210,   210,   161,   162,   163,   231,
     233,   229,     5,     5,    27,   228,   185,   186,   235,    15,
       5,     5,     5,     5,     5,     5,     5,     5,     5,     5,
       5,    13,    11,    15,    15,    13,    13,     5,    13,    13,
      13,    13,    13,    15,    15,    10,    15,    13,     5,    15,
      15,    13,    13,    15,    15,    13,    13,    13,    15,    15,
       5,   228,    13,     5,   228,   226,   227,   220,    13,     5,
       5,     5,     5,     5,     5,    15,    15,    15,    11,    15,
      15,    13,    15,    15,    15,    13,    13,     4,   228,   220,
     226,   233,     5,     5,     5,    15,    15,    15,    13,    13,
      13,    13,   210,   220,   233,    13,    15,    13,   233,    13
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint16 yyr1[] =
{
       0,   206,   207,   208,   209,   210,   211,   212,   213,   214,
     215,   216,   217,   217,   217,   218,   218,   218,   219,   219,
     219,   220,   221,   222,   223,   223,   223,   223,   223,   223,
     223,   223,   223,   223,   223,   223,   223,   223,   223,   223,
     223,   223,   223,   223,   223,   223,   223,   223,   224,   225,
     226,   226,   226,   227,   227,   227,   228,   229,   229,   229,
     229,   229,   229,   229,   229,   229,   229,   229,   229,   229,
     229,   229,   229,   229,   229,   230,   230,   230,   230,   230,
     230,   231,   231,   231,   232,   232,   232,   232,   232,   233,
     233,   233,   234,   234,   234,   235,   235,   236,   236,   237,
     237,   237,   237,   238,   238,   238,   239,   239,   239,   239,
     240,   240,   240,   240,   240,   241,   241,   241,   241,   241,
     241,   241,   241,   241,   241,   241,   241,   241,   241,   241,
     241,   241,   241,   241,   241,   241,   241,   241,   241,   241,
     241,   241,   241,   242,   242,   242,   242,   242,   242,   242,
     242,   242,   242,   242,   243,   243,   243,   243,   244,   244,
     244,   244,   244,   244,   245,   245,   245,   245,   245,   245,
     246,   246,   246,   247,   247,   247,   247,   247,   248,   248,
     248,   248,   248,   249,   249,   249,   250,   250,   250,   251,
     252,   253,   254,   255,   256,   257,   258,   259,   259,   259,
     260,   260,   260,   261,   261,   261,   263,   262,   265,   264,
     266,   266,   266,   268,   267,   269,   269,   269,   271,   270,
     273,   272,   274,   274,   274,   276,   275,   277,   277,   277,
     277,   277,   277,   277,   277,   278,   278,   278,   279,   279,
     279,   279,   279,   279,   280,   280,   280,   281,   281,   281,
     281,   281,   281,   281,   281,   283,   282,   284,   284,   284,
     285,   285,   285,   285,   285,   285,   285,   287,   286,   288
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     2,     2,     2,     2,     2,     2,     5,
       7,     9,     0,     1,     3,     0,     1,     3,     0,     1,
       3,     4,     4,     4,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,    10,    10,
       1,     1,     1,     0,     1,     3,     4,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     6,     4,     6,     6,    10,
       6,     1,     1,     1,     8,     8,    16,    14,    12,     1,
       1,     1,     8,     4,     4,     1,     1,     6,    10,     1,
       1,     1,     1,     4,     6,     6,    10,     4,     1,     1,
       8,    10,    10,     6,     6,     4,    10,     4,     3,     3,
       4,     8,     6,    10,     6,     6,    12,    10,    18,    10,
       4,     6,     6,     6,     6,     6,     4,     6,    12,     6,
      10,    10,    12,     4,     3,    14,    14,     6,     6,     6,
       6,    10,    10,     4,     1,     1,     1,     1,     6,    10,
       6,     3,    10,     6,     4,     4,     4,     4,     4,     4,
       1,     1,     1,     1,     1,     1,     1,     1,     4,     4,
       4,     4,     4,     1,     1,     1,     1,     1,     1,     4,
       4,     4,     4,     4,     4,     4,     4,     0,     2,     3,
       1,     1,     1,     0,     2,     3,     0,     6,     0,     6,
       0,     2,     3,     0,    11,     0,     2,     3,     0,     6,
       0,     6,     0,     2,     3,     0,     6,     1,     1,     1,
       1,     1,     1,     1,     1,     0,     1,     2,     5,     5,
       5,     8,     6,    12,     0,     1,     3,     1,     1,     1,
       1,     1,     1,     1,     1,     0,     5,     0,     1,     2,
       1,     1,     1,     1,     1,     1,     1,     0,     5,     5
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 274 "grammer.y" /* yacc.c:1646  */
    { LOGI_puts("root"); }
#line 2081 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 3:
#line 276 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = PE_ref_sv_Act( (yyvsp[0].i) ); }
#line 2087 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 4:
#line 277 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = BM_ref_sv_Act( (yyvsp[0].i) ); }
#line 2093 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 5:
#line 278 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = PA_ref_sv_Act( (yyvsp[0].i) ); }
#line 2099 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 6:
#line 279 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = RF_ref_sv_Act( (yyvsp[0].i) ); }
#line 2105 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 7:
#line 280 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = PT_ref_sv_Act( (yyvsp[0].i) ); }
#line 2111 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 8:
#line 281 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = PI_ref_sv_Act( (yyvsp[0].i) ); }
#line 2117 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 9:
#line 283 "grammer.y" /* yacc.c:1646  */
    { switch (float_bin_cnst_Act( (yyvsp[-3].f), (yyvsp[-1].f) )) 
    { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2124 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 10:
#line 285 "grammer.y" /* yacc.c:1646  */
    { switch (float_tri_cnst_Act( (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].f) )) 
    { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2131 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 11:
#line 288 "grammer.y" /* yacc.c:1646  */
    { switch (float_quad_cnst_Act( (yyvsp[-7].f), (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2137 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 12:
#line 290 "grammer.y" /* yacc.c:1646  */
    { switch (float_list_empty_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2143 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 13:
#line 291 "grammer.y" /* yacc.c:1646  */
    { switch (float_list_F_Act( (yyvsp[0].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2149 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 14:
#line 292 "grammer.y" /* yacc.c:1646  */
    { switch (float_list_FL_F_Act( (yyvsp[0].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2155 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 15:
#line 293 "grammer.y" /* yacc.c:1646  */
    { switch (float_bin_list_empty_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2161 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 294 "grammer.y" /* yacc.c:1646  */
    { switch (float_bin_list_FBC_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2167 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 17:
#line 295 "grammer.y" /* yacc.c:1646  */
    { switch (float_bin_list_FBL_FBC_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2173 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 296 "grammer.y" /* yacc.c:1646  */
    { switch (float_quad_list_empty_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2179 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 19:
#line 297 "grammer.y" /* yacc.c:1646  */
    { switch (float_quad_list_FQC_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2185 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 20:
#line 298 "grammer.y" /* yacc.c:1646  */
    { switch (float_quad_list_FQL_FQC_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2191 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 21:
#line 300 "grammer.y" /* yacc.c:1646  */
    { switch (float_array_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2197 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 301 "grammer.y" /* yacc.c:1646  */
    { switch (float_bin_array_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2203 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 23:
#line 302 "grammer.y" /* yacc.c:1646  */
    { switch (float_quad_array_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2209 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 24:
#line 306 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_RED_Act()        ; }
#line 2215 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 25:
#line 307 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_BLUE_Act()       ; }
#line 2221 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 26:
#line 308 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_GREEN_Act()      ; }
#line 2227 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 27:
#line 309 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_BLACK_Act()      ; }
#line 2233 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 28:
#line 310 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_WHITE_Act()      ; }
#line 2239 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 29:
#line 311 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_GRAY_Act()       ; }
#line 2245 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 30:
#line 312 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_CYAN_Act()       ; }
#line 2251 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 31:
#line 313 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_MAGENTA_Act()    ; }
#line 2257 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 32:
#line 314 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_YELLOW_Act()     ; }
#line 2263 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 315 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_LIGHTGRAY_Act()  ; }
#line 2269 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 34:
#line 316 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_DARKGRAY_Act()   ; }
#line 2275 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 35:
#line 317 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_GREY_Act()       ; }
#line 2281 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 36:
#line 318 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_LIGHTGREY_Act()  ; }
#line 2287 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 37:
#line 319 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_DARKGREY_Act()   ; }
#line 2293 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 38:
#line 320 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_AQUA_Act()       ; }
#line 2299 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 39:
#line 321 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_FUCHSIA_Act()    ; }
#line 2305 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 40:
#line 322 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_LIME_Act()       ; }
#line 2311 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 41:
#line 323 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_MAROON_Act()     ; }
#line 2317 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 42:
#line 324 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_NAVY_Act()       ; }
#line 2323 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 43:
#line 325 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_OLIVE_Act()      ; }
#line 2329 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 44:
#line 326 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_PURPLE_Act()     ; }
#line 2335 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 45:
#line 327 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_SILVER_Act()     ; }
#line 2341 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 46:
#line 328 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_TEAL_Act()       ; }
#line 2347 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 47:
#line 329 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = color_string_COLOR_S_TRANSPARENT_Act(); }
#line 2353 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 48:
#line 331 "grammer.y" /* yacc.c:1646  */
    {(yyval.i) = color_hsv_sv_Act( (yyvsp[-7].i), (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].f) );}
#line 2359 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 49:
#line 332 "grammer.y" /* yacc.c:1646  */
    {(yyval.i) = color_argb_sv_Act( (yyvsp[-7].i), (yyvsp[-5].i), (yyvsp[-3].i), (yyvsp[-1].i) );}
#line 2365 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 50:
#line 334 "grammer.y" /* yacc.c:1646  */
    { switch (color_cnst_sv_color_string_sv_Act( (yyvsp[0].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2371 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 51:
#line 335 "grammer.y" /* yacc.c:1646  */
    { switch (color_cnst_sv_color_hsv_sv_Act( (yyvsp[0].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2377 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 52:
#line 336 "grammer.y" /* yacc.c:1646  */
    { switch (color_cnst_sv_color_argb_sv_Act( (yyvsp[0].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2383 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 53:
#line 337 "grammer.y" /* yacc.c:1646  */
    { switch (color_cnst_list_empty_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2389 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 54:
#line 338 "grammer.y" /* yacc.c:1646  */
    { switch (color_cnst_list_CC_Act( )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2395 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 55:
#line 339 "grammer.y" /* yacc.c:1646  */
    { switch (color_cnst_list_CCL_CC_Act(  )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2401 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 56:
#line 341 "grammer.y" /* yacc.c:1646  */
    { switch (color_cnst_array_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2407 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 57:
#line 345 "grammer.y" /* yacc.c:1646  */
    { switch (pdm_enum_PDM_ADD_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }         ; }
#line 2413 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 58:
#line 346 "grammer.y" /* yacc.c:1646  */
    { switch (pdm_enum_PDM_CLEAR_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }       ; }
#line 2419 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 59:
#line 347 "grammer.y" /* yacc.c:1646  */
    { switch (pdm_enum_PDM_DARKEN_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }      ; }
#line 2425 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 60:
#line 348 "grammer.y" /* yacc.c:1646  */
    { switch (pdm_enum_PDM_DST_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }         ; }
#line 2431 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 61:
#line 349 "grammer.y" /* yacc.c:1646  */
    { switch (pdm_enum_PDM_DST_ATOP_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }    ; }
#line 2437 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 62:
#line 350 "grammer.y" /* yacc.c:1646  */
    { switch (pdm_enum_PDM_DST_IN_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }      ; }
#line 2443 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 63:
#line 351 "grammer.y" /* yacc.c:1646  */
    { switch (pdm_enum_PDM_DST_OUT_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }     ; }
#line 2449 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 64:
#line 352 "grammer.y" /* yacc.c:1646  */
    { switch (pdm_enum_PDM_DST_OVER_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }    ; }
#line 2455 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 65:
#line 353 "grammer.y" /* yacc.c:1646  */
    { switch (pdm_enum_PDM_LIGHTEN_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }     ; }
#line 2461 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 66:
#line 354 "grammer.y" /* yacc.c:1646  */
    { switch (pdm_enum_PDM_MULTIPLY_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }    ; }
#line 2467 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 67:
#line 355 "grammer.y" /* yacc.c:1646  */
    { switch (pdm_enum_PDM_OVERLAY_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }     ; }
#line 2473 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 68:
#line 356 "grammer.y" /* yacc.c:1646  */
    { switch (pdm_enum_PDM_SCREEN_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }      ; }
#line 2479 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 69:
#line 357 "grammer.y" /* yacc.c:1646  */
    { switch (pdm_enum_PDM_SRC_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }         ; }
#line 2485 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 70:
#line 358 "grammer.y" /* yacc.c:1646  */
    { switch (pdm_enum_PDM_SRC_ATOP_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }    ; }
#line 2491 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 71:
#line 359 "grammer.y" /* yacc.c:1646  */
    { switch (pdm_enum_PDM_SRC_IN_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }      ; }
#line 2497 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 72:
#line 360 "grammer.y" /* yacc.c:1646  */
    { switch (pdm_enum_PDM_SRC_OUT_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }     ; }
#line 2503 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 73:
#line 361 "grammer.y" /* yacc.c:1646  */
    { switch (pdm_enum_PDM_SRC_OVER_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }    ; }
#line 2509 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 74:
#line 362 "grammer.y" /* yacc.c:1646  */
    { switch (pdm_enum_PDM_XOR_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }         ; }
#line 2515 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 75:
#line 368 "grammer.y" /* yacc.c:1646  */
    { switch (pe_element_PE_COMPOSE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2521 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 76:
#line 370 "grammer.y" /* yacc.c:1646  */
    { switch (pe_element_PE_CORNER_Act( (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2527 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 77:
#line 372 "grammer.y" /* yacc.c:1646  */
    { switch (pe_element_PE_DASH_Act( (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2533 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 78:
#line 374 "grammer.y" /* yacc.c:1646  */
    { switch (pe_element_PE_DISCRETE_Act( (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2539 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 79:
#line 376 "grammer.y" /* yacc.c:1646  */
    { switch (pe_element_PE_PATHDASH_Act( (yyvsp[-7].i), (yyvsp[-5].f), (yyvsp[-3].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : ; }  /*YYABORT;*/ }
#line 2545 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 80:
#line 378 "grammer.y" /* yacc.c:1646  */
    { switch (pe_element_PE_SUM_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2551 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 81:
#line 382 "grammer.y" /* yacc.c:1646  */
    { switch (pathdash_style_enum_MORPH_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2557 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 82:
#line 384 "grammer.y" /* yacc.c:1646  */
    { switch (pathdash_style_enum_ROTATE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2563 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 83:
#line 386 "grammer.y" /* yacc.c:1646  */
    { switch (pathdash_style_enum_TRANSLATE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2569 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 84:
#line 391 "grammer.y" /* yacc.c:1646  */
    { switch(shader_BITMAP_Act( (yyvsp[-5].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : ; } /*YYABORT;*/ }
#line 2575 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 85:
#line 393 "grammer.y" /* yacc.c:1646  */
    { switch (shader_COMPOSE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2581 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 86:
#line 395 "grammer.y" /* yacc.c:1646  */
    { switch (shader_LINEAR_Act( (yyvsp[-13].f), (yyvsp[-11].f), (yyvsp[-9].f), (yyvsp[-7].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2587 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 87:
#line 397 "grammer.y" /* yacc.c:1646  */
    { switch (shader_RADIAL_Act( (yyvsp[-11].f), (yyvsp[-9].f), (yyvsp[-7].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2593 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 88:
#line 399 "grammer.y" /* yacc.c:1646  */
    { switch (shader_SWEEP_Act( (yyvsp[-9].f), (yyvsp[-7].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2599 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 89:
#line 401 "grammer.y" /* yacc.c:1646  */
    { switch (shader_tile_CLAMP_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2605 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 90:
#line 402 "grammer.y" /* yacc.c:1646  */
    { switch (shader_tile_MIRROR_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2611 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 91:
#line 403 "grammer.y" /* yacc.c:1646  */
    { switch (shader_tile_REPEAT_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2617 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 92:
#line 408 "grammer.y" /* yacc.c:1646  */
    { switch (xfermode_Avoid_Act( (yyvsp[-5].i), (yyvsp[-3].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2623 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 93:
#line 409 "grammer.y" /* yacc.c:1646  */
    { switch (xfermode_PixelXor_Act( (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2629 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 94:
#line 410 "grammer.y" /* yacc.c:1646  */
    { switch (xfermode_PorterDuff_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2635 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 95:
#line 412 "grammer.y" /* yacc.c:1646  */
    { switch (xf_avoid_enum_AVOID_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2641 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 96:
#line 413 "grammer.y" /* yacc.c:1646  */
    { switch (xf_avoid_enum_TARGET_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2647 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 97:
#line 417 "grammer.y" /* yacc.c:1646  */
    { switch (maskfilter_BLUR_Act( (yyvsp[-3].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2653 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 98:
#line 419 "grammer.y" /* yacc.c:1646  */
    { switch (maskfilter_EMBOSS_Act( (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2659 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 99:
#line 421 "grammer.y" /* yacc.c:1646  */
    { switch (mf_blur_enum_INNER_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2665 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 100:
#line 422 "grammer.y" /* yacc.c:1646  */
    { switch (mf_blur_enum_NORMAL_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2671 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 101:
#line 423 "grammer.y" /* yacc.c:1646  */
    { switch (mf_blur_enum_OUTER_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2677 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 102:
#line 424 "grammer.y" /* yacc.c:1646  */
    { switch (mf_blur_enum_SOLID_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2683 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 103:
#line 428 "grammer.y" /* yacc.c:1646  */
    { switch (colorfilter_ColorMatrix_Act()) 
		{ case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2690 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 104:
#line 431 "grammer.y" /* yacc.c:1646  */
    { switch (colorfilter_Lighting_Act( (yyvsp[-3].i), (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2696 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 105:
#line 433 "grammer.y" /* yacc.c:1646  */
    { switch (colorfilter_PorterDuff_Act( (yyvsp[-3].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2702 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 106:
#line 436 "grammer.y" /* yacc.c:1646  */
    { switch (colormatrix_Scale_Act( (yyvsp[-7].f), (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  }
#line 2708 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 107:
#line 438 "grammer.y" /* yacc.c:1646  */
    { switch (colormatrix_Saturation_Act( (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2714 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 108:
#line 439 "grammer.y" /* yacc.c:1646  */
    { switch (colormatrix_YUV2RGB_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  }
#line 2720 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 109:
#line 440 "grammer.y" /* yacc.c:1646  */
    { switch (colormatrix_RGB2YUV_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  }
#line 2726 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 110:
#line 445 "grammer.y" /* yacc.c:1646  */
    { (yyval.mat) = matrixliteral_Rotate_Act( (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].f) ); }
#line 2732 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 111:
#line 447 "grammer.y" /* yacc.c:1646  */
    { (yyval.mat) = matrixliteral_Scale_Act( (yyvsp[-7].f), (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].f) ); }
#line 2738 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 112:
#line 449 "grammer.y" /* yacc.c:1646  */
    { (yyval.mat) = matrixliteral_Skew_Act( (yyvsp[-7].f), (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].f) ); }
#line 2744 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 113:
#line 451 "grammer.y" /* yacc.c:1646  */
    { (yyval.mat) = matrixliteral_Translate_Act( (yyvsp[-3].f), (yyvsp[-1].f) ); }
#line 2750 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 114:
#line 453 "grammer.y" /* yacc.c:1646  */
    { (yyval.mat) =  matrixliteral_Concat_Act( (yyvsp[-3].mat), (yyvsp[-1].mat) ); }
#line 2756 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 115:
#line 459 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_CLIPPATH_Act( (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
#line 2762 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 116:
#line 461 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_CLIPRECT_ffff_Act( (yyvsp[-7].f), (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2768 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 117:
#line 463 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_CLIPRECT_rf_Act( (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
#line 2774 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 118:
#line 466 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_SAVE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2780 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 119:
#line 468 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_RESTORE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2786 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 120:
#line 471 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_ROTATE_f_Act( (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2792 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 121:
#line 473 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_ROTATE_fff_Act( (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2798 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 122:
#line 475 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_SCALE_ff_Act( (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2804 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 123:
#line 477 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_SCALE_ffff_Act( (yyvsp[-7].f), (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2810 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 124:
#line 479 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_SKEW_Act( (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2816 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 125:
#line 481 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_TRANSLATE_Act( (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2822 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 126:
#line 484 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_DRAWARC_Act( (yyvsp[-9].i), (yyvsp[-7].f), (yyvsp[-5].f), (yyvsp[-3].i), (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
#line 2828 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 127:
#line 486 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_DRAWBITMAP_Act( (yyvsp[-7].i), (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
#line 2834 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 128:
#line 488 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_DRAWBITMAPMESH_Act( (yyvsp[-15].i), (yyvsp[-13].i), (yyvsp[-11].i), (yyvsp[-7].i), (yyvsp[-3].i), (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
#line 2840 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 129:
#line 490 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_DRAWCIRCLE_Act( (yyvsp[-7].f), (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }   /*YYABORT;*/ }
#line 2846 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 130:
#line 492 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_DRAWCOLOR_i_Act( (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2852 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 131:
#line 494 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_DRAWCOLOR_ie_Act( (yyvsp[-3].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2858 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 132:
#line 496 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_DRAWLINES_Act( (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }   /*YYABORT;*/ }
#line 2864 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 133:
#line 498 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_DRAWOVAL_Act( (yyvsp[-3].i), (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
#line 2870 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 134:
#line 500 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_DRAWPATH_Act( (yyvsp[-3].i), (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
#line 2876 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 135:
#line 502 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_DRAWPICTURE_pirf_Act( (yyvsp[-3].i), (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
#line 2882 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 136:
#line 504 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_DRAWPICTURE_pi_Act( (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
#line 2888 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 137:
#line 506 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_DRAWPOINTS_Act( (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
#line 2894 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 138:
#line 508 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_DRAWRECT_ffff_Act( (yyvsp[-9].f), (yyvsp[-7].f), (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
#line 2900 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 139:
#line 510 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_DRAWRECT_rf_Act( (yyvsp[-3].i), (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
#line 2906 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 140:
#line 512 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_DRAWROUNDRECT_Act( (yyvsp[-7].i), (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
#line 2912 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 141:
#line 514 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_DRAWTEXT_Act( (yyvsp[-7].str), (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
#line 2918 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 142:
#line 516 "grammer.y" /* yacc.c:1646  */
    { switch (canvascall_DRAWTEXTONPATH_Act( (yyvsp[-9].str), (yyvsp[-7].i), (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
#line 2924 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 143:
#line 521 "grammer.y" /* yacc.c:1646  */
    { switch (pt_initcall_FILLTYPE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2930 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 144:
#line 523 "grammer.y" /* yacc.c:1646  */
    { switch (pt_initcall_CLOSE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2936 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 145:
#line 525 "grammer.y" /* yacc.c:1646  */
    { switch (pt_initcall_CUBICTO_Act( (yyvsp[-11].f), (yyvsp[-9].f), (yyvsp[-7].f), (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2942 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 146:
#line 527 "grammer.y" /* yacc.c:1646  */
    { switch (pt_initcall_RCUBICTO_Act( (yyvsp[-11].f), (yyvsp[-9].f), (yyvsp[-7].f), (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2948 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 147:
#line 529 "grammer.y" /* yacc.c:1646  */
    { switch (pt_initcall_LINETO_Act( (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2954 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 148:
#line 531 "grammer.y" /* yacc.c:1646  */
    { switch (pt_initcall_RLINETO_Act( (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2960 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 149:
#line 533 "grammer.y" /* yacc.c:1646  */
    { switch (pt_initcall_MOVETO_Act( (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2966 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 150:
#line 535 "grammer.y" /* yacc.c:1646  */
    { switch (pt_initcall_RMOVETO_Act( (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2972 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 151:
#line 537 "grammer.y" /* yacc.c:1646  */
    { switch (pt_initcall_QUADTO_Act( (yyvsp[-7].f), (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2978 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 152:
#line 539 "grammer.y" /* yacc.c:1646  */
    { switch (pt_initcall_RQUADTO_Act( (yyvsp[-7].f), (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2984 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 153:
#line 541 "grammer.y" /* yacc.c:1646  */
    { switch (pt_initcall_TRANSFORM_Act( (yyvsp[-1].mat) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2990 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 154:
#line 544 "grammer.y" /* yacc.c:1646  */
    { switch (filltype_enum_EVEN_ODD_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 2996 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 155:
#line 545 "grammer.y" /* yacc.c:1646  */
    { switch (filltype_enum_INVERSE_EVEN_ODD_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3002 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 156:
#line 546 "grammer.y" /* yacc.c:1646  */
    { switch (filltype_enum_INVERSE_WINDING_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3008 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 157:
#line 547 "grammer.y" /* yacc.c:1646  */
    { switch (filltype_enum_WINDING_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3014 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 158:
#line 552 "grammer.y" /* yacc.c:1646  */
    { switch (rf_modcall_INSET_Act( (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3020 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 159:
#line 554 "grammer.y" /* yacc.c:1646  */
    { switch (rf_modcall_INTERSECT_Act( (yyvsp[-7].f), (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3026 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 160:
#line 556 "grammer.y" /* yacc.c:1646  */
    { switch (rf_modcall_OFFSET_Act( (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3032 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 161:
#line 558 "grammer.y" /* yacc.c:1646  */
    { switch (rf_modcall_SORT_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3038 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 162:
#line 560 "grammer.y" /* yacc.c:1646  */
    { switch (rf_modcall_UNION_ffff_Act( (yyvsp[-7].f), (yyvsp[-5].f), (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3044 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 163:
#line 562 "grammer.y" /* yacc.c:1646  */
    { switch (rf_modcall_UNION_ff_Act( (yyvsp[-3].f), (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3050 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 164:
#line 567 "grammer.y" /* yacc.c:1646  */
    { switch (pa_fntcall_HINTING_Act( (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3056 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 165:
#line 569 "grammer.y" /* yacc.c:1646  */
    { switch (pa_fntcall_TEXTALIGN_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3062 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 166:
#line 571 "grammer.y" /* yacc.c:1646  */
    { switch (pa_fntcall_TEXTSCALEX_Act( (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3068 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 167:
#line 573 "grammer.y" /* yacc.c:1646  */
    { switch (pa_fntcall_TEXTSIZE_Act( (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3074 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 168:
#line 575 "grammer.y" /* yacc.c:1646  */
    { switch (pa_fntcall_TEXTSKEWX_Act( (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3080 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 169:
#line 577 "grammer.y" /* yacc.c:1646  */
    { switch (pa_fntcall_SETTYPEFACE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3086 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 170:
#line 579 "grammer.y" /* yacc.c:1646  */
    { switch (pa_align_enum_CENTER_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3092 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 171:
#line 580 "grammer.y" /* yacc.c:1646  */
    { switch (pa_align_enum_LEFT_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3098 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 172:
#line 581 "grammer.y" /* yacc.c:1646  */
    { switch (pa_align_enum_RIGHT_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3104 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 173:
#line 583 "grammer.y" /* yacc.c:1646  */
    { switch (typeface_DEFAULT_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3110 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 174:
#line 584 "grammer.y" /* yacc.c:1646  */
    { switch (typeface_DEFAULT_BOLD_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3116 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 175:
#line 585 "grammer.y" /* yacc.c:1646  */
    { switch (typeface_MONOSPACE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3122 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 176:
#line 586 "grammer.y" /* yacc.c:1646  */
    { switch (typeface_SANS_SERIF_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3128 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 177:
#line 587 "grammer.y" /* yacc.c:1646  */
    { switch (typeface_SERIF_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3134 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 178:
#line 592 "grammer.y" /* yacc.c:1646  */
    { switch (pa_strkcall_STROKECAP_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3140 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 179:
#line 594 "grammer.y" /* yacc.c:1646  */
    { switch (pa_strkcall_STROKEJOIN_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3146 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 180:
#line 596 "grammer.y" /* yacc.c:1646  */
    { switch (pa_strkcall_STROKEMITER_Act( (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3152 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 181:
#line 598 "grammer.y" /* yacc.c:1646  */
    { switch (pa_strkcall_STROKEWIDTH_Act( (yyvsp[-1].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3158 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 182:
#line 600 "grammer.y" /* yacc.c:1646  */
    { switch (pa_strkcall_PATHEFFECT_Act( (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
#line 3164 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 183:
#line 602 "grammer.y" /* yacc.c:1646  */
    { switch (cap_enum_BUTT_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3170 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 184:
#line 603 "grammer.y" /* yacc.c:1646  */
    { switch (cap_enum_ROUND_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3176 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 185:
#line 604 "grammer.y" /* yacc.c:1646  */
    { switch (cap_enum_SQUARE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3182 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 186:
#line 606 "grammer.y" /* yacc.c:1646  */
    { switch (join_enum_BEVEL_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3188 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 187:
#line 607 "grammer.y" /* yacc.c:1646  */
    { switch (join_enum_MITER_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3194 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 188:
#line 608 "grammer.y" /* yacc.c:1646  */
    { switch (join_enum_ROUND_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3200 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 189:
#line 612 "grammer.y" /* yacc.c:1646  */
    { switch (pa_fnt_init_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3206 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 190:
#line 614 "grammer.y" /* yacc.c:1646  */
    { switch (pa_clr_init_Act( (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3212 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 191:
#line 616 "grammer.y" /* yacc.c:1646  */
    { switch (pa_pathmode_init_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3218 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 192:
#line 618 "grammer.y" /* yacc.c:1646  */
    { switch (pa_stroke_init_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3224 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 193:
#line 620 "grammer.y" /* yacc.c:1646  */
    { switch (pa_fill_init_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3230 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 194:
#line 622 "grammer.y" /* yacc.c:1646  */
    { switch (pa_cmstmd_init_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3236 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 195:
#line 624 "grammer.y" /* yacc.c:1646  */
    { switch (pa_mf_init_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3242 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 196:
#line 626 "grammer.y" /* yacc.c:1646  */
    { switch (pa_cf_init_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3248 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 200:
#line 632 "grammer.y" /* yacc.c:1646  */
    { switch (pa_style_enum_FILL_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3254 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 201:
#line 633 "grammer.y" /* yacc.c:1646  */
    { switch (pa_style_enum_FILL_AND_STROKE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3260 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 202:
#line 634 "grammer.y" /* yacc.c:1646  */
    { switch (pa_style_enum_STROKE_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3266 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 206:
#line 643 "grammer.y" /* yacc.c:1646  */
    { switch (pexSet_midAct( (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/}
#line 3272 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 207:
#line 644 "grammer.y" /* yacc.c:1646  */
    { switch (pexSet_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3278 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 208:
#line 650 "grammer.y" /* yacc.c:1646  */
    { switch (ptxInitial_midAct( (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
#line 3284 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 209:
#line 651 "grammer.y" /* yacc.c:1646  */
    { switch (ptxInitial_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3290 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 213:
#line 660 "grammer.y" /* yacc.c:1646  */
    { switch (pixRecord_midAct( (yyvsp[-6].i), (yyvsp[-3].i), (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
#line 3296 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 214:
#line 661 "grammer.y" /* yacc.c:1646  */
    { switch (pixRecord_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3302 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 218:
#line 670 "grammer.y" /* yacc.c:1646  */
    { switch (bmxComposite_midAct( (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
#line 3308 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 219:
#line 671 "grammer.y" /* yacc.c:1646  */
    { switch (bmxComposite_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3314 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 220:
#line 676 "grammer.y" /* yacc.c:1646  */
    { switch (rfxMod_midAct( (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
#line 3320 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 221:
#line 677 "grammer.y" /* yacc.c:1646  */
    { switch (rfxMod_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3326 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 225:
#line 685 "grammer.y" /* yacc.c:1646  */
    { switch (paxInitial_midAct( (yyvsp[-1].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); }  /*YYABORT;*/ }
#line 3332 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 226:
#line 686 "grammer.y" /* yacc.c:1646  */
    { switch (paxInitial_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3338 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 238:
#line 704 "grammer.y" /* yacc.c:1646  */
    { switch (alloc_PE_ref_sv_Act( (yyvsp[-3].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3344 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 239:
#line 706 "grammer.y" /* yacc.c:1646  */
    { switch (alloc_PT_ref_sv_Act( (yyvsp[-3].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3350 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 240:
#line 708 "grammer.y" /* yacc.c:1646  */
    { switch (alloc_PI_ref_sv_Act( (yyvsp[-3].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3356 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 241:
#line 710 "grammer.y" /* yacc.c:1646  */
    { switch (alloc_BM_ref_sv_Act( (yyvsp[-6].i), (yyvsp[-4].i), (yyvsp[-2].i) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3362 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 242:
#line 712 "grammer.y" /* yacc.c:1646  */
    { switch (alloc_PA_ref_sv_Act( (yyvsp[-4].i), (yyvsp[-2].i))) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3368 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 243:
#line 714 "grammer.y" /* yacc.c:1646  */
    { switch (alloc_RF_ref_sv_Act( (yyvsp[-10].i), (yyvsp[-8].f), (yyvsp[-6].f), (yyvsp[-4].f), (yyvsp[-2].f) )) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3374 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 244:
#line 716 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = paint_flags_sv_empty_Act();  }
#line 3380 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 245:
#line 717 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = paint_flags_sv_PF_Act((yyvsp[0].i)); }
#line 3386 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 246:
#line 718 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = paint_flags_sv_PFs_PF_Act( (yyvsp[-2].i), (yyvsp[0].i) ); }
#line 3392 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 247:
#line 720 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = paint_flag_sv_ANTI_ALIAS_Act(); }
#line 3398 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 248:
#line 721 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = paint_flag_sv_DITHER_Act(); }
#line 3404 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 249:
#line 722 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = paint_flag_sv_FAKE_BOLD_TEXT_Act(); }
#line 3410 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 250:
#line 723 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = paint_flag_sv_FILTER_BITMAP_Act(); }
#line 3416 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 251:
#line 724 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = paint_flag_sv_LINEAR_TEXT_Act(); }
#line 3422 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 252:
#line 725 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = paint_flag_sv_STRIKE_THRU_TEXT_Act(); }
#line 3428 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 253:
#line 726 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = paint_flag_sv_SUBPIXEL_TEXT_Act(); }
#line 3434 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 254:
#line 727 "grammer.y" /* yacc.c:1646  */
    { (yyval.i) = paint_flag_sv_UNDERLINE_TEXT_Act(); }
#line 3440 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 255:
#line 732 "grammer.y" /* yacc.c:1646  */
    { switch (declarations_midAct()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3446 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 256:
#line 733 "grammer.y" /* yacc.c:1646  */
    { switch (declarations_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3452 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 267:
#line 750 "grammer.y" /* yacc.c:1646  */
    { switch (onDraw_midAct()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3458 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 268:
#line 751 "grammer.y" /* yacc.c:1646  */
    { switch (onDraw_Act()) { case SUCCESS : ; break; case RNA : YYABORT; break; default : abort(); } }
#line 3464 "grammer.tab.c" /* yacc.c:1646  */
    break;

  case 269:
#line 755 "grammer.y" /* yacc.c:1646  */
    { LOGI_puts("body"); }
#line 3470 "grammer.tab.c" /* yacc.c:1646  */
    break;


#line 3474 "grammer.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 757 "grammer.y" /* yacc.c:1906  */


//Epilogue
/* Called by yyparse on error.  */
void
yyerror (char const *s)
{
    //fprintf (stderr, "yyerror: %s\n", s);
	// debug
	LOGE_printf("\nyyerror: %s\n", s);
	// debug end
}

