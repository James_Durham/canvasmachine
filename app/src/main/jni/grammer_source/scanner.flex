%option noyywrap nodefault stack

%{
// #include "../action.h"
#include "grammer.tab.h"
// debug
#include "../utils/inline_LOGx.h"
// debug end

#include <stdio.h>
#include <string.h>
static int pexset = 0;
%}

 //"CA"|
WSA_PrefixStart "PE"|"BM"|"PA"|"RF"|"PT"|"PI"|"FA"|"FBA"|"FQA"|"CCA"

DIGIT_ [0-9]
INT_ [+-]?[0-9]+
FLOAT_ [-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?
BOOL_ "true"|"false"
STRING_S ['][^'\n]*[']
STRING_D ["][^"\n]*["]

%s PathEffect Shader XferMode ColorFilter ColorMatrix MatrixLiteral PaJoin PaCap 
%x StringS StringD
%x WhiteSpaceAware
%%
[ \t\n]+ { /* consume */ }
[#][^\n]*[\n] { /* consume comment */ }
{WSA_PrefixStart} { pexset = 0; yyless(0); yy_push_state(WhiteSpaceAware); }
PE{DIGIT_}[.]Set { pexset = 1; yyless(0); yy_push_state(WhiteSpaceAware); }


{STRING_S}  { yyless(1); yy_push_state(StringS); }
{STRING_D}  { yyless(1); yy_push_state(StringD); }
	/* TODO: make xstrdup */
<StringS>[^'\n]* { yylval.str = strdup(yytext); /*printf("<scanner StringS:·%s·>\n", yytext);*/ return STRING_sv; }
<StringS>[']	 { yy_pop_state(); }
<StringD>[^"\n]* { yylval.str = strdup(yytext); /*printf("<scanner StringD:·%s·>\n", yytext);*/ return STRING_sv; }
<StringD>["]	 { yy_pop_state(); }
{DIGIT_}  { yylval.i = atoi(yytext); return DIGIT_sv; }
{INT_}    { yylval.i = atoi(yytext); return INT_sv; }
{FLOAT_}  { yylval.f = atof(yytext); return FLOAT_sv; }

{BOOL_}   { yylval.i = (*yytext == 't') ? 1 : 0; return BOOL_sv; }


<WhiteSpaceAware>{DIGIT_}"." {  yylval.i = atoi(yytext); yyless(1); return DIGIT_sv; }
<WhiteSpaceAware>{DIGIT_} { yylval.i = atoi(yytext);  yy_pop_state(); return DIGIT_sv; }


"{"		                 { return BR_L ; }
 /* move down to bottom "}"		                 { return BR_R ; } */
"]"                      { return SQ_R ; }
"("                      { return PRN_L; }
")"                      { return PRN_R; }
";"                      { return SMI  ; }
","                      { return COM  ; }
"|"                      { return OR;    }
<WhiteSpaceAware>"["     { yy_pop_state(); return SQ_L ; }

<WhiteSpaceAware>"PE"    { return PE; }
<WhiteSpaceAware>"BM"    { return BM; }
<WhiteSpaceAware>"PA"    { return PA; }
<WhiteSpaceAware>"RF"    { return RF; }
<WhiteSpaceAware>"PT"    { return PT; }
 /* <WhiteSpaceAware>"CA"    { return CA; } */
<WhiteSpaceAware>"PI"    { return PI; }

<WhiteSpaceAware>"FA"    { return FA ; }
<WhiteSpaceAware>"FBA"   { return FBA; }
<WhiteSpaceAware>"FQA"   { return FQA; }
<WhiteSpaceAware>"CCA"   { return CCA; }

<WhiteSpaceAware>".Set"       { yy_pop_state(); if (pexset) {pexset = 0; yy_push_state(PathEffect);} return DOT_SET; }
<WhiteSpaceAware>".Initial"   { yy_pop_state(); return DOT_INITIAL  ; }
<WhiteSpaceAware>".Record"    { yy_pop_state(); return DOT_RECORD   ; }
<WhiteSpaceAware>".Composite" { yy_pop_state(); return DOT_COMPOSITE; }
<WhiteSpaceAware>".Mod"       { yy_pop_state(); return DOT_MOD      ; }
"new"   { return NEW ;}

"red"         { return COLOR_S_RED        ; }
"blue"        { return COLOR_S_BLUE       ; }
"green"       { return COLOR_S_GREEN      ; }
"black"       { return COLOR_S_BLACK      ; }
"white"       { return COLOR_S_WHITE      ; }
"gray"        { return COLOR_S_GRAY       ; }
"cyan"        { return COLOR_S_CYAN       ; }
"magenta"     { return COLOR_S_MAGENTA    ; }
"yellow"      { return COLOR_S_YELLOW     ; }
"lightgray"   { return COLOR_S_LIGHTGRAY  ; }
"darkgray"    { return COLOR_S_DARKGRAY   ; }
"grey"        { return COLOR_S_GREY       ; }
"lightgrey"   { return COLOR_S_LIGHTGREY  ; }
"darkgrey"    { return COLOR_S_DARKGREY   ; }
"aqua"        { return COLOR_S_AQUA       ; }
"fuchsia"     { return COLOR_S_FUCHSIA    ; }
"lime"        { return COLOR_S_LIME       ; }
"maroon"      { return COLOR_S_MAROON     ; }
"navy"        { return COLOR_S_NAVY       ; }
"olive"       { return COLOR_S_OLIVE      ; }
"purple"      { return COLOR_S_PURPLE     ; }
"silver"      { return COLOR_S_SILVER     ; }
"teal"        { return COLOR_S_TEAL       ; }
"TRANSPARENT" { return COLOR_S_TRANSPARENT; }

"HSV"         { return COLOR_HSV ; }
"ARGB"        { return COLOR_ARGB; }

<PathEffect>"Compose"  { return PE_ELEMENT_COMPOSE ; }
<PathEffect>"Corner"   { return PE_ELEMENT_CORNER  ; }
<PathEffect>"Dash"     { return PE_ELEMENT_DASH    ; }
<PathEffect>"Discrete" { return PE_ELEMENT_DISCRETE; }
<PathEffect>"PathDash" { return PE_ELEMENT_PATHDASH; }
<PathEffect>"Sum"      { return PE_ELEMENT_SUM     ; }
<PathEffect>"}"		   { yyless(0); yy_pop_state();  }

<Shader>"Bitmap"  { return SHADER_BITMAP ; }
<Shader>"Compose" { return SHADER_COMPOSE; }
<Shader>"Linear"  { return SHADER_LINEAR ; }
<Shader>"Radial"  { return SHADER_RADIAL ; }
<Shader>"Sweep"   { return SHADER_SWEEP  ; }
<Shader>"}"		   { yyless(0); yy_pop_state(); }

<XferMode>"Avoid"      { yy_pop_state(); return XFERMODE_AVOID     ; }
<XferMode>"PixelXOR"   { yy_pop_state(); return XFERMODE_PIXELXOR  ; }
<XferMode>"PorterDuff" { yy_pop_state(); return XFERMODE_PORTERDUFF; }

"Blur"    { return MASKFILTER_BLUR  ; }
"Emboss"  { return MASKFILTER_EMBOSS; }

<ColorFilter>"ColorMatrix" { yy_pop_state(); yy_push_state(ColorMatrix); return COLOR_FILTER_COLORMATRIX; }
<ColorFilter>"Lighting"    { yy_pop_state(); return COLOR_FILTER_LIGHTING   ; }
<ColorFilter>"PorterDuff"  { yy_pop_state(); return COLOR_FILTER_PORTERDUFF ; }

<ColorMatrix>"Scale"      { yy_pop_state(); return COLORMATRIX_SCALE     ; }
<ColorMatrix>"Saturation" { yy_pop_state(); return COLORMATRIX_SATURATION; }
<ColorMatrix>"YUV2RGB"    { yy_pop_state(); return COLORMATRIX_YUV2RGB   ; }
<ColorMatrix>"RGB2YUV"    { yy_pop_state(); return COLORMATRIX_RGB2YUV   ; }

<MatrixLiteral>"Rotate"    { yy_pop_state(); return MATRIXLITERAL_ROTATE   ; }
<MatrixLiteral>"Scale"     { yy_pop_state(); return MATRIXLITERAL_SCALE    ; }
<MatrixLiteral>"Skew"      { yy_pop_state(); return MATRIXLITERAL_SKEW     ; }
<MatrixLiteral>"Translate" { yy_pop_state(); return MATRIXLITERAL_TRANSLATE; }
<MatrixLiteral>"Multiply"  { yy_push_state(MatrixLiteral); return MATRIXLITERAL_MULTIPLY ; }



"clipPath"   { return CANVAS_C_CLIPRECT; }
"clipRect"   { return CANVAS_C_CLIPPATH; }

"save"       { return CANVAS_C_SAVE   ; }
"restore"    { return CANVAS_C_RESTORE; }

"rotate"     { return CANVAS_C_ROTATE   ; }
"scale"      { return CANVAS_C_SCALE    ; }
"skew"       { return CANVAS_C_SKEW     ; }
"translate"  { return CANVAS_C_TRANSLATE; }

"drawArc"         { return CANVAS_C_DRAWARC       ; }
"drawBitmap"      { return CANVAS_C_DRAWBITMAP    ; }
"drawBitmapMesh"  { return CANVAS_C_DRAWBITMAPMESH; }
"drawCircle"      { return CANVAS_C_DRAWCIRCLE    ; }
"drawColor"       { return CANVAS_C_DRAWCOLOR     ; }
"drawLines"       { return CANVAS_C_DRAWLINES     ; }
"drawOval"        { return CANVAS_C_DRAWOVAL      ; }
"drawPath"        { return CANVAS_C_DRAWPATH      ; }
"drawPicture"     { return CANVAS_C_DRAWPICTURE   ; }
"drawPoints"      { return CANVAS_C_DRAWPOINTS    ; }
"drawRect"        { return CANVAS_C_DRAWRECT      ; }
"drawRoundRect"   { return CANVAS_C_DRAWROUNDRECT ; }
"drawText"        { return CANVAS_C_DRAWTEXT      ; }
"drawTextOnPath"  { return CANVAS_C_DRAWTEXTONPATH; }

"FillType"        { return PT_INIT_C_FILLTYPE ; }
"close"           { return PT_INIT_C_CLOSE    ; }
"cubicTo"         { return PT_INIT_C_CUBICTO  ; }
"rCubicTo"        { return PT_INIT_C_RCUBICTO ; }
"lineTo"          { return PT_INIT_C_LINETO   ; }
"rLineTo"         { return PT_INIT_C_RLINETO  ; }
"moveTo"          { return PT_INIT_C_MOVETO   ; }
"rMoveTo"         { return PT_INIT_C_RMOVETO  ; }
"quadTo"          { return PT_INIT_C_QUADTO   ; }
"rQuadTo"         { return PT_INIT_C_RQUADTO  ; }
"transform"       { yy_push_state(MatrixLiteral); return PT_INIT_C_TRANSFORM; }

"inset"           { return RF_MOD_C_INSET    ; }
"intersect"       { return RF_MOD_C_INTERSECT; }
"offset"          { return RF_MOD_C_OFFSET   ; }
"sort"            { return RF_MOD_C_SORT     ; }
"union"           { return RF_MOD_C_UNION    ; }

"Hinting"         { return PA_FNT_C_HINTING ; }
"Align"           { return PA_FNT_C_ALIGN   ; }
"ScaleX"          { return PA_FNT_C_SCALEX  ; }
"Size"            { return PA_FNT_C_SIZE    ; }
"SkewX"           { return PA_FNT_C_SKEWX   ; }
"Typeface"        { return PA_FNT_C_TYPEFACE; }

"Cap"             { yy_push_state(PaCap);  return PA_STRK_C_CAP      ; }
"Join"            { yy_push_state(PaJoin); return PA_STRK_C_JOIN     ; }
"Miter"           { return PA_STRK_C_MITER     ; }
"Width"           { return PA_STRK_C_WIDTH     ; }
"PathEffect"      { return PA_STRK_C_PATHEFFECT; }

"ANTI_ALIAS_FLAG"        { return PA_FLAG_ANTI_ALIAS      ; }
"DITHER_FLAG"            { return PA_FLAG_DITHER          ; }
"FAKE_BOLD_TEXT_FLAG"    { return PA_FLAG_FAKE_BOLD_TEXT  ; }
"FILTER_BITMAP_FLAG"     { return PA_FLAG_FILTER_BITMAP   ; }
"LINEAR_TEXT_FLAG"       { return PA_FLAG_LINEAR_TEXT     ; }
"STRIKE_THRU_TEXT_FLAG"  { return PA_FLAG_STRIKE_THRU_TEXT; }
"SUBPIXEL_TEXT_FLAG"     { return PA_FLAG_SUBPIXEL_TEXT   ; }
"UNDERLINE_TEXT_FLAG"    { return PA_FLAG_UNDERLINE_TEXT  ; }

"FILL"              { return PA_STYLE_FILL           ; }
"FILL_AND_STROKE"   { return PA_STYLE_FILL_AND_STROKE; }
"STROKE"            { return PA_STYLE_STROKE         ; }

"DEFAULT"           { return PA_TYPEFACE_DEFAULT     ; }
"DEFAULT_BOLD"      { return PA_TYPEFACE_DEFAULT_BOLD; }
"MONOSPACE"         { return PA_TYPEFACE_MONOSPACE   ; }
"SANS_SERIF"        { return PA_TYPEFACE_SANS_SERIF  ; }
"SERIF"             { return PA_TYPEFACE_SERIF       ; }

"CENTER"            { return PA_ALIGN_CENTER; }
"LEFT"              { return PA_ALIGN_LEFT  ; }
"RIGHT"             { return PA_ALIGN_RIGHT ; }

<PaJoin>"BEVEL"   { yy_pop_state(); return PA_JOIN_BEVEL; }
<PaJoin>"MITER"   { yy_pop_state(); return PA_JOIN_MITER; }
<PaJoin>"ROUND"   { yy_pop_state(); return PA_JOIN_ROUND; }

<PaCap>"BUTT"     { yy_pop_state(); return PA_CAP_BUTT  ; }
<PaCap>"ROUND"    { yy_pop_state(); return PA_CAP_ROUND ; }
<PaCap>"SQUARE"   { yy_pop_state(); return PA_CAP_SQUARE; }

"MORPH"      { return PE_PATHDASH_MORPH    ; }
"ROTATE"     { return PE_PATHDASH_ROTATE   ; }
"TRANSLATE"  { return PE_PATHDASH_TRANSLATE; }

"CLAMP"      { return SH_TILE_CLAMP ; }
"MIRROR"     { return SH_TILE_MIRROR; }
"REPEAT"     { return SH_TILE_REPEAT; }

"ADD"        { return PDM_ADD     ; }
"CLEAR"      { return PDM_CLEAR   ; }
"DARKEN"     { return PDM_DARKEN  ; }
"DST"        { return PDM_DST     ; }
"DST_ATOP"   { return PDM_DST_ATOP; }
"DST_IN"     { return PDM_DST_IN  ; }
"DST_OUT"    { return PDM_DST_OUT ; }
"DST_OVER"   { return PDM_DST_OVER; }
"LIGHTEN"    { return PDM_LIGHTEN ; }
"MULTIPLY"   { return PDM_MULTIPLY; }
"OVERLAY"    { return PDM_OVERLAY ; }
"SCREEN"     { return PDM_SCREEN  ; }
"SRC"        { return PDM_SRC     ; }
"SRC_ATOP"   { return PDM_SRC_ATOP; }
"SRC_IN"     { return PDM_SRC_IN  ; }
"SRC_OUT"    { return PDM_SRC_OUT ; }
"SRC_OVER"   { return PDM_SRC_OVER; }
"XOR"        { return PDM_XOR     ; }

"AVOID"      { return XF_AVOID_AVOID ; }
"TARGET"     { return XF_AVOID_TARGET; }

"INNER"      { return MF_BLUR_INNER ; }
"NORMAL"     { return MF_BLUR_NORMAL; }
"OUTER"      { return MF_BLUR_OUTER ; }
"SOLID"      { return MF_BLUR_SOLID ; }

"EVEN_ODD"         { return PT_FILLTYPE_EVEN_ODD        ; }
"INVERSE_EVEN_ODD" { return PT_FILLTYPE_INVERSE_EVEN_ODD; }
"INVERSE_WINDING"  { return PT_FILLTYPE_INVERSE_WINDING ; }
"WINDING"          { return PT_FILLTYPE_WINDING         ; }

"Font"             { return DEC_SPEC_FONT         ; }
"Color"            { return DEC_SPEC_COLOR        ; }
"PathMode"         { return DEC_SPEC_PATHMODE     ; }
"Stroke"           { return DEC_SPEC_STROKE       ; }
"Fill"             { yy_push_state(Shader); return DEC_SPEC_FILL; }
"CompositeMode"    { yy_push_state(XferMode); return DEC_SPEC_COMPOSITEMODE; }
"MaskFilter"       { return DEC_SPEC_MASKFILTER   ; }
"ColorFilter"      { yy_push_state(ColorFilter); return DEC_SPEC_COLORFILTER  ; }
"Body"             { return DEC_SPEC_BODY         ; }
"Declarations"      { return DEC_SPEC_DECLARATION  ; }
"onDraw"           { return DEC_SPEC_ONDRAW       ; }

"}"		                 { return BR_R ; }
<*>.|\n { /*printf("\n--DEFAULT RULE--\n yytext = \"%s\"\n", yytext); fflush (stdout); return (int) *yytext; */
		// debug
		LOGE_printf("\n--DEFAULT RULE--\n yytext = \"%s\"\n", yytext);
		// debug end
		return 0;
		}
%%
