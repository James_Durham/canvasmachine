#include "../jni_env.h"
#include "../utils/java_macros.h"
#include "action_sigs.h"
#include "method_id_namespace.h"
#include "../utils/exception.h"

static void load()
{
    jobject tmp;

    LOAD_CLASS(ActionsCallBacs_cls, "Native/Machine/ActionsCallBacks");

    myGETMETHODID(mi_ns.float_bin_cnst_Act_mid, ActionsCallBacs_cls, FLOAT_BIN_CNST_SIG[1], FLOAT_BIN_CNST_SIG[0]);
    myGETMETHODID(mi_ns.float_tri_cnst_Act_mid, ActionsCallBacs_cls, FLOAT_TRI_CNST_SIG[1], FLOAT_TRI_CNST_SIG[0]);
    myGETMETHODID(mi_ns.float_quad_cnst_Act_mid, ActionsCallBacs_cls, FLOAT_QUAD_CNST_SIG[1], FLOAT_QUAD_CNST_SIG[0]);
    myGETMETHODID(mi_ns.float_list_empty_Act_mid, ActionsCallBacs_cls, FLOAT_LIST_EMPTY_SIG[1], FLOAT_LIST_EMPTY_SIG[0]);
    myGETMETHODID(mi_ns.float_list_F_Act_mid, ActionsCallBacs_cls, FLOAT_LIST_F_SIG[1], FLOAT_LIST_F_SIG[0]);
    myGETMETHODID(mi_ns.float_list_FL_F_Act_mid, ActionsCallBacs_cls, FLOAT_LIST_FL_F_SIG[1], FLOAT_LIST_FL_F_SIG[0]);
    myGETMETHODID(mi_ns.float_bin_list_empty_Act_mid, ActionsCallBacs_cls, FLOAT_BIN_LIST_EMPTY_SIG[1], FLOAT_BIN_LIST_EMPTY_SIG[0]);
    myGETMETHODID(mi_ns.float_bin_list_FBC_Act_mid, ActionsCallBacs_cls, FLOAT_BIN_LIST_FBC_SIG[1], FLOAT_BIN_LIST_FBC_SIG[0]);
    myGETMETHODID(mi_ns.float_bin_list_FBL_FBC_Act_mid, ActionsCallBacs_cls, FLOAT_BIN_LIST_FBL_FBC_SIG[1], FLOAT_BIN_LIST_FBL_FBC_SIG[0]);
    myGETMETHODID(mi_ns.float_quad_list_empty_Act_mid, ActionsCallBacs_cls, FLOAT_QUAD_LIST_EMPTY_SIG[1], FLOAT_QUAD_LIST_EMPTY_SIG[0]);
    myGETMETHODID(mi_ns.float_quad_list_FQC_Act_mid, ActionsCallBacs_cls, FLOAT_QUAD_LIST_FQC_SIG[1], FLOAT_QUAD_LIST_FQC_SIG[0]);
    myGETMETHODID(mi_ns.float_quad_list_FQL_FQC_Act_mid, ActionsCallBacs_cls, FLOAT_QUAD_LIST_FQL_FQC_SIG[1], FLOAT_QUAD_LIST_FQL_FQC_SIG[0]);
    myGETMETHODID(mi_ns.float_array_Act_mid, ActionsCallBacs_cls, FLOAT_ARRAY_SIG[1], FLOAT_ARRAY_SIG[0]);
    myGETMETHODID(mi_ns.float_bin_array_Act_mid, ActionsCallBacs_cls, FLOAT_BIN_ARRAY_SIG[1], FLOAT_BIN_ARRAY_SIG[0]);
    myGETMETHODID(mi_ns.float_quad_array_Act_mid, ActionsCallBacs_cls, FLOAT_QUAD_ARRAY_SIG[1], FLOAT_QUAD_ARRAY_SIG[0]);

    myGETMETHODID(mi_ns.color_cnst_sv_color_string_sv_Act_mid, ActionsCallBacs_cls, COLOR_CNST_SV_COLOR_STRING_SV_SIG[1], COLOR_CNST_SV_COLOR_STRING_SV_SIG[0]);
    myGETMETHODID(mi_ns.color_cnst_sv_color_hsv_sv_Act_mid, ActionsCallBacs_cls, COLOR_CNST_SV_COLOR_HSV_SV_SIG[1], COLOR_CNST_SV_COLOR_HSV_SV_SIG[0]);
    myGETMETHODID(mi_ns.color_cnst_sv_color_argb_sv_Act_mid, ActionsCallBacs_cls, COLOR_CNST_SV_COLOR_ARGB_SV_SIG[1], COLOR_CNST_SV_COLOR_ARGB_SV_SIG[0]);

    myGETMETHODID(mi_ns.color_cnst_list_empty_Act_mid, ActionsCallBacs_cls, COLOR_CNST_LIST_EMPTY_SIG[1], COLOR_CNST_LIST_EMPTY_SIG[0]);
    myGETMETHODID(mi_ns.color_cnst_list_CC_Act_mid, ActionsCallBacs_cls, COLOR_CNST_LIST_CC_SIG[1], COLOR_CNST_LIST_CC_SIG[0]);
    myGETMETHODID(mi_ns.color_cnst_list_CCL_CC_Act_mid, ActionsCallBacs_cls, COLOR_CNST_LIST_CCL_CC_SIG[1], COLOR_CNST_LIST_CCL_CC_SIG[0]);
    myGETMETHODID(mi_ns.color_cnst_array_Act_mid, ActionsCallBacs_cls, COLOR_CNST_ARRAY_SIG[1], COLOR_CNST_ARRAY_SIG[0]);

    myGETMETHODID(mi_ns.pdm_enum_PDM_ADD_Act_mid, ActionsCallBacs_cls, PDM_ENUM_PDM_ADD_SIG[1], PDM_ENUM_PDM_ADD_SIG[0]);
    myGETMETHODID(mi_ns.pdm_enum_PDM_CLEAR_Act_mid, ActionsCallBacs_cls, PDM_ENUM_PDM_CLEAR_SIG[1], PDM_ENUM_PDM_CLEAR_SIG[0]);
    myGETMETHODID(mi_ns.pdm_enum_PDM_DARKEN_Act_mid, ActionsCallBacs_cls, PDM_ENUM_PDM_DARKEN_SIG[1], PDM_ENUM_PDM_DARKEN_SIG[0]);
    myGETMETHODID(mi_ns.pdm_enum_PDM_DST_Act_mid, ActionsCallBacs_cls, PDM_ENUM_PDM_DST_SIG[1], PDM_ENUM_PDM_DST_SIG[0]);
    myGETMETHODID(mi_ns.pdm_enum_PDM_DST_ATOP_Act_mid, ActionsCallBacs_cls, PDM_ENUM_PDM_DST_ATOP_SIG[1], PDM_ENUM_PDM_DST_ATOP_SIG[0]);
    myGETMETHODID(mi_ns.pdm_enum_PDM_DST_IN_Act_mid, ActionsCallBacs_cls, PDM_ENUM_PDM_DST_IN_SIG[1], PDM_ENUM_PDM_DST_IN_SIG[0]);
    myGETMETHODID(mi_ns.pdm_enum_PDM_DST_OUT_Act_mid, ActionsCallBacs_cls, PDM_ENUM_PDM_DST_OUT_SIG[1], PDM_ENUM_PDM_DST_OUT_SIG[0]);
    myGETMETHODID(mi_ns.pdm_enum_PDM_DST_OVER_Act_mid, ActionsCallBacs_cls, PDM_ENUM_PDM_DST_OVER_SIG[1], PDM_ENUM_PDM_DST_OVER_SIG[0]);
    myGETMETHODID(mi_ns.pdm_enum_PDM_LIGHTEN_Act_mid, ActionsCallBacs_cls, PDM_ENUM_PDM_LIGHTEN_SIG[1], PDM_ENUM_PDM_LIGHTEN_SIG[0]);
    myGETMETHODID(mi_ns.pdm_enum_PDM_MULTIPLY_Act_mid, ActionsCallBacs_cls, PDM_ENUM_PDM_MULTIPLY_SIG[1], PDM_ENUM_PDM_MULTIPLY_SIG[0]);
    myGETMETHODID(mi_ns.pdm_enum_PDM_OVERLAY_Act_mid, ActionsCallBacs_cls, PDM_ENUM_PDM_OVERLAY_SIG[1], PDM_ENUM_PDM_OVERLAY_SIG[0]);
    myGETMETHODID(mi_ns.pdm_enum_PDM_SCREEN_Act_mid, ActionsCallBacs_cls, PDM_ENUM_PDM_SCREEN_SIG[1], PDM_ENUM_PDM_SCREEN_SIG[0]);
    myGETMETHODID(mi_ns.pdm_enum_PDM_SRC_Act_mid, ActionsCallBacs_cls, PDM_ENUM_PDM_SRC_SIG[1], PDM_ENUM_PDM_SRC_SIG[0]);
    myGETMETHODID(mi_ns.pdm_enum_PDM_SRC_ATOP_Act_mid, ActionsCallBacs_cls, PDM_ENUM_PDM_SRC_ATOP_SIG[1], PDM_ENUM_PDM_SRC_ATOP_SIG[0]);
    myGETMETHODID(mi_ns.pdm_enum_PDM_SRC_IN_Act_mid, ActionsCallBacs_cls, PDM_ENUM_PDM_SRC_IN_SIG[1], PDM_ENUM_PDM_SRC_IN_SIG[0]);
    myGETMETHODID(mi_ns.pdm_enum_PDM_SRC_OUT_Act_mid, ActionsCallBacs_cls, PDM_ENUM_PDM_SRC_OUT_SIG[1], PDM_ENUM_PDM_SRC_OUT_SIG[0]);
    myGETMETHODID(mi_ns.pdm_enum_PDM_SRC_OVER_Act_mid, ActionsCallBacs_cls, PDM_ENUM_PDM_SRC_OVER_SIG[1], PDM_ENUM_PDM_SRC_OVER_SIG[0]);
    myGETMETHODID(mi_ns.pdm_enum_PDM_XOR_Act_mid, ActionsCallBacs_cls, PDM_ENUM_PDM_XOR_SIG[1], PDM_ENUM_PDM_XOR_SIG[0]);
    myGETMETHODID(mi_ns.pe_element_PE_COMPOSE_Act_mid, ActionsCallBacs_cls, PE_ELEMENT_PE_COMPOSE_SIG[1], PE_ELEMENT_PE_COMPOSE_SIG[0]);
    myGETMETHODID(mi_ns.pe_element_PE_CORNER_Act_mid, ActionsCallBacs_cls, PE_ELEMENT_PE_CORNER_SIG[1], PE_ELEMENT_PE_CORNER_SIG[0]);
    myGETMETHODID(mi_ns.pe_element_PE_DASH_Act_mid, ActionsCallBacs_cls, PE_ELEMENT_PE_DASH_SIG[1], PE_ELEMENT_PE_DASH_SIG[0]);
    myGETMETHODID(mi_ns.pe_element_PE_DISCRETE_Act_mid, ActionsCallBacs_cls, PE_ELEMENT_PE_DISCRETE_SIG[1], PE_ELEMENT_PE_DISCRETE_SIG[0]);
    myGETMETHODID(mi_ns.pe_element_PE_PATHDASH_Act_mid, ActionsCallBacs_cls, PE_ELEMENT_PE_PATHDASH_SIG[1], PE_ELEMENT_PE_PATHDASH_SIG[0]);
    myGETMETHODID(mi_ns.pe_element_PE_SUM_Act_mid, ActionsCallBacs_cls, PE_ELEMENT_PE_SUM_SIG[1], PE_ELEMENT_PE_SUM_SIG[0]);
    myGETMETHODID(mi_ns.pathdash_style_enum_MORPH_Act_mid, ActionsCallBacs_cls, PATHDASH_STYLE_ENUM_MORPH_SIG[1], PATHDASH_STYLE_ENUM_MORPH_SIG[0]);
    myGETMETHODID(mi_ns.pathdash_style_enum_ROTATE_Act_mid, ActionsCallBacs_cls, PATHDASH_STYLE_ENUM_ROTATE_SIG[1], PATHDASH_STYLE_ENUM_ROTATE_SIG[0]);
    myGETMETHODID(mi_ns.pathdash_style_enum_TRANSLATE_Act_mid, ActionsCallBacs_cls, PATHDASH_STYLE_ENUM_TRANSLATE_SIG[1], PATHDASH_STYLE_ENUM_TRANSLATE_SIG[0]);
    myGETMETHODID(mi_ns.shader_BITMAP_Act_mid, ActionsCallBacs_cls, SHADER_BITMAP_SIG[1], SHADER_BITMAP_SIG[0]);
    myGETMETHODID(mi_ns.shader_COMPOSE_Act_mid, ActionsCallBacs_cls, SHADER_COMPOSE_SIG[1], SHADER_COMPOSE_SIG[0]);
    myGETMETHODID(mi_ns.shader_LINEAR_Act_mid, ActionsCallBacs_cls, SHADER_LINEAR_SIG[1], SHADER_LINEAR_SIG[0]);
    myGETMETHODID(mi_ns.shader_RADIAL_Act_mid, ActionsCallBacs_cls, SHADER_RADIAL_SIG[1], SHADER_RADIAL_SIG[0]);
    myGETMETHODID(mi_ns.shader_SWEEP_Act_mid, ActionsCallBacs_cls, SHADER_SWEEP_SIG[1], SHADER_SWEEP_SIG[0]);
    myGETMETHODID(mi_ns.shader_tile_CLAMP_Act_mid, ActionsCallBacs_cls, SHADER_TILE_CLAMP_SIG[1], SHADER_TILE_CLAMP_SIG[0]);
    myGETMETHODID(mi_ns.shader_tile_MIRROR_Act_mid, ActionsCallBacs_cls, SHADER_TILE_MIRROR_SIG[1], SHADER_TILE_MIRROR_SIG[0]);
    myGETMETHODID(mi_ns.shader_tile_REPEAT_Act_mid, ActionsCallBacs_cls, SHADER_TILE_REPEAT_SIG[1], SHADER_TILE_REPEAT_SIG[0]);
    myGETMETHODID(mi_ns.xfermode_Avoid_Act_mid, ActionsCallBacs_cls, XFERMODE_AVOID_SIG[1], XFERMODE_AVOID_SIG[0]);
    myGETMETHODID(mi_ns.xfermode_PixelXor_Act_mid, ActionsCallBacs_cls, XFERMODE_PIXELXOR_SIG[1], XFERMODE_PIXELXOR_SIG[0]);
    myGETMETHODID(mi_ns.xfermode_PorterDuff_Act_mid, ActionsCallBacs_cls, XFERMODE_PORTERDUFF_SIG[1], XFERMODE_PORTERDUFF_SIG[0]);
    myGETMETHODID(mi_ns.xf_avoid_enum_AVOID_Act_mid, ActionsCallBacs_cls, XF_AVOID_ENUM_AVOID_SIG[1], XF_AVOID_ENUM_AVOID_SIG[0]);
    myGETMETHODID(mi_ns.xf_avoid_enum_TARGET_Act_mid, ActionsCallBacs_cls, XF_AVOID_ENUM_TARGET_SIG[1], XF_AVOID_ENUM_TARGET_SIG[0]);
    myGETMETHODID(mi_ns.maskfilter_BLUR_Act_mid, ActionsCallBacs_cls, MASKFILTER_BLUR_SIG[1], MASKFILTER_BLUR_SIG[0]);
    myGETMETHODID(mi_ns.maskfilter_EMBOSS_Act_mid, ActionsCallBacs_cls, MASKFILTER_EMBOSS_SIG[1], MASKFILTER_EMBOSS_SIG[0]);
    myGETMETHODID(mi_ns.mf_blur_enum_INNER_Act_mid, ActionsCallBacs_cls, MF_BLUR_ENUM_INNER_SIG[1], MF_BLUR_ENUM_INNER_SIG[0]);
    myGETMETHODID(mi_ns.mf_blur_enum_NORMAL_Act_mid, ActionsCallBacs_cls, MF_BLUR_ENUM_NORMAL_SIG[1], MF_BLUR_ENUM_NORMAL_SIG[0]);
    myGETMETHODID(mi_ns.mf_blur_enum_OUTER_Act_mid, ActionsCallBacs_cls, MF_BLUR_ENUM_OUTER_SIG[1], MF_BLUR_ENUM_OUTER_SIG[0]);
    myGETMETHODID(mi_ns.mf_blur_enum_SOLID_Act_mid, ActionsCallBacs_cls, MF_BLUR_ENUM_SOLID_SIG[1], MF_BLUR_ENUM_SOLID_SIG[0]);
    myGETMETHODID(mi_ns.colorfilter_ColorMatrix_Act_mid, ActionsCallBacs_cls, COLORFILTER_COLORMATRIX_SIG[1], COLORFILTER_COLORMATRIX_SIG[0]);
    myGETMETHODID(mi_ns.colorfilter_Lighting_Act_mid, ActionsCallBacs_cls, COLORFILTER_LIGHTING_SIG[1], COLORFILTER_LIGHTING_SIG[0]);
    myGETMETHODID(mi_ns.colorfilter_PorterDuff_Act_mid, ActionsCallBacs_cls, COLORFILTER_PORTERDUFF_SIG[1], COLORFILTER_PORTERDUFF_SIG[0]);
    myGETMETHODID(mi_ns.colormatrix_Scale_Act_mid, ActionsCallBacs_cls, COLORMATRIX_SCALE_SIG[1], COLORMATRIX_SCALE_SIG[0]);
    myGETMETHODID(mi_ns.colormatrix_Saturation_Act_mid, ActionsCallBacs_cls, COLORMATRIX_SATURATION_SIG[1], COLORMATRIX_SATURATION_SIG[0]);
    myGETMETHODID(mi_ns.colormatrix_YUV2RGB_Act_mid, ActionsCallBacs_cls, COLORMATRIX_YUV2RGB_SIG[1], COLORMATRIX_YUV2RGB_SIG[0]);
    myGETMETHODID(mi_ns.colormatrix_RGB2YUV_Act_mid, ActionsCallBacs_cls, COLORMATRIX_RGB2YUV_SIG[1], COLORMATRIX_RGB2YUV_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_CLIPPATH_Act_mid, ActionsCallBacs_cls, CANVASCALL_CLIPPATH_SIG[1], CANVASCALL_CLIPPATH_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_CLIPRECT_ffff_Act_mid, ActionsCallBacs_cls, CANVASCALL_CLIPRECT_FFFF_SIG[1], CANVASCALL_CLIPRECT_FFFF_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_CLIPRECT_rf_Act_mid, ActionsCallBacs_cls, CANVASCALL_CLIPRECT_RF_SIG[1], CANVASCALL_CLIPRECT_RF_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_SAVE_Act_mid, ActionsCallBacs_cls, CANVASCALL_SAVE_SIG[1], CANVASCALL_SAVE_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_RESTORE_Act_mid, ActionsCallBacs_cls, CANVASCALL_RESTORE_SIG[1], CANVASCALL_RESTORE_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_ROTATE_f_Act_mid, ActionsCallBacs_cls, CANVASCALL_ROTATE_F_SIG[1], CANVASCALL_ROTATE_F_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_ROTATE_fff_Act_mid, ActionsCallBacs_cls, CANVASCALL_ROTATE_FFF_SIG[1], CANVASCALL_ROTATE_FFF_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_SCALE_ff_Act_mid, ActionsCallBacs_cls, CANVASCALL_SCALE_FF_SIG[1], CANVASCALL_SCALE_FF_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_SCALE_ffff_Act_mid, ActionsCallBacs_cls, CANVASCALL_SCALE_FFFF_SIG[1], CANVASCALL_SCALE_FFFF_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_SKEW_Act_mid, ActionsCallBacs_cls, CANVASCALL_SKEW_SIG[1], CANVASCALL_SKEW_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_TRANSLATE_Act_mid, ActionsCallBacs_cls, CANVASCALL_TRANSLATE_SIG[1], CANVASCALL_TRANSLATE_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_DRAWARC_Act_mid, ActionsCallBacs_cls, CANVASCALL_DRAWARC_SIG[1], CANVASCALL_DRAWARC_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_DRAWBITMAP_Act_mid, ActionsCallBacs_cls, CANVASCALL_DRAWBITMAP_SIG[1], CANVASCALL_DRAWBITMAP_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_DRAWBITMAPMESH_Act_mid, ActionsCallBacs_cls, CANVASCALL_DRAWBITMAPMESH_SIG[1], CANVASCALL_DRAWBITMAPMESH_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_DRAWCIRCLE_Act_mid, ActionsCallBacs_cls, CANVASCALL_DRAWCIRCLE_SIG[1], CANVASCALL_DRAWCIRCLE_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_DRAWCOLOR_i_Act_mid, ActionsCallBacs_cls, CANVASCALL_DRAWCOLOR_I_SIG[1], CANVASCALL_DRAWCOLOR_I_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_DRAWCOLOR_ie_Act_mid, ActionsCallBacs_cls, CANVASCALL_DRAWCOLOR_IE_SIG[1], CANVASCALL_DRAWCOLOR_IE_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_DRAWLINES_Act_mid, ActionsCallBacs_cls, CANVASCALL_DRAWLINES_SIG[1], CANVASCALL_DRAWLINES_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_DRAWOVAL_Act_mid, ActionsCallBacs_cls, CANVASCALL_DRAWOVAL_SIG[1], CANVASCALL_DRAWOVAL_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_DRAWPATH_Act_mid, ActionsCallBacs_cls, CANVASCALL_DRAWPATH_SIG[1], CANVASCALL_DRAWPATH_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_DRAWPICTURE_pirf_Act_mid, ActionsCallBacs_cls, CANVASCALL_DRAWPICTURE_PIRF_SIG[1], CANVASCALL_DRAWPICTURE_PIRF_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_DRAWPICTURE_pi_Act_mid, ActionsCallBacs_cls, CANVASCALL_DRAWPICTURE_PI_SIG[1], CANVASCALL_DRAWPICTURE_PI_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_DRAWPOINTS_Act_mid, ActionsCallBacs_cls, CANVASCALL_DRAWPOINTS_SIG[1], CANVASCALL_DRAWPOINTS_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_DRAWRECT_ffff_Act_mid, ActionsCallBacs_cls, CANVASCALL_DRAWRECT_FFFF_SIG[1], CANVASCALL_DRAWRECT_FFFF_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_DRAWRECT_rf_Act_mid, ActionsCallBacs_cls, CANVASCALL_DRAWRECT_RF_SIG[1], CANVASCALL_DRAWRECT_RF_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_DRAWROUNDRECT_Act_mid, ActionsCallBacs_cls, CANVASCALL_DRAWROUNDRECT_SIG[1], CANVASCALL_DRAWROUNDRECT_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_DRAWTEXT_Act_mid, ActionsCallBacs_cls, CANVASCALL_DRAWTEXT_SIG[1], CANVASCALL_DRAWTEXT_SIG[0]);
    myGETMETHODID(mi_ns.canvascall_DRAWTEXTONPATH_Act_mid, ActionsCallBacs_cls, CANVASCALL_DRAWTEXTONPATH_SIG[1], CANVASCALL_DRAWTEXTONPATH_SIG[0]);
    myGETMETHODID(mi_ns.pt_initcall_FILLTYPE_Act_mid, ActionsCallBacs_cls, PT_INITCALL_FILLTYPE_SIG[1], PT_INITCALL_FILLTYPE_SIG[0]);
    myGETMETHODID(mi_ns.pt_initcall_CLOSE_Act_mid, ActionsCallBacs_cls, PT_INITCALL_CLOSE_SIG[1], PT_INITCALL_CLOSE_SIG[0]);
    myGETMETHODID(mi_ns.pt_initcall_CUBICTO_Act_mid, ActionsCallBacs_cls, PT_INITCALL_CUBICTO_SIG[1], PT_INITCALL_CUBICTO_SIG[0]);
    myGETMETHODID(mi_ns.pt_initcall_RCUBICTO_Act_mid, ActionsCallBacs_cls, PT_INITCALL_RCUBICTO_SIG[1], PT_INITCALL_RCUBICTO_SIG[0]);
    myGETMETHODID(mi_ns.pt_initcall_LINETO_Act_mid, ActionsCallBacs_cls, PT_INITCALL_LINETO_SIG[1], PT_INITCALL_LINETO_SIG[0]);
    myGETMETHODID(mi_ns.pt_initcall_RLINETO_Act_mid, ActionsCallBacs_cls, PT_INITCALL_RLINETO_SIG[1], PT_INITCALL_RLINETO_SIG[0]);
    myGETMETHODID(mi_ns.pt_initcall_MOVETO_Act_mid, ActionsCallBacs_cls, PT_INITCALL_MOVETO_SIG[1], PT_INITCALL_MOVETO_SIG[0]);
    myGETMETHODID(mi_ns.pt_initcall_RMOVETO_Act_mid, ActionsCallBacs_cls, PT_INITCALL_RMOVETO_SIG[1], PT_INITCALL_RMOVETO_SIG[0]);
    myGETMETHODID(mi_ns.pt_initcall_QUADTO_Act_mid, ActionsCallBacs_cls, PT_INITCALL_QUADTO_SIG[1], PT_INITCALL_QUADTO_SIG[0]);
    myGETMETHODID(mi_ns.pt_initcall_RQUADTO_Act_mid, ActionsCallBacs_cls, PT_INITCALL_RQUADTO_SIG[1], PT_INITCALL_RQUADTO_SIG[0]);
    myGETMETHODID(mi_ns.pt_initcall_TRANSFORM_Act_mid, ActionsCallBacs_cls, PT_INITCALL_TRANSFORM_SIG[1], PT_INITCALL_TRANSFORM_SIG[0]);
    myGETMETHODID(mi_ns.filltype_enum_EVEN_ODD_Act_mid, ActionsCallBacs_cls, FILLTYPE_ENUM_EVEN_ODD_SIG[1], FILLTYPE_ENUM_EVEN_ODD_SIG[0]);
    myGETMETHODID(mi_ns.filltype_enum_INVERSE_EVEN_ODD_Act_mid, ActionsCallBacs_cls, FILLTYPE_ENUM_INVERSE_EVEN_ODD_SIG[1], FILLTYPE_ENUM_INVERSE_EVEN_ODD_SIG[0]);
    myGETMETHODID(mi_ns.filltype_enum_INVERSE_WINDING_Act_mid, ActionsCallBacs_cls, FILLTYPE_ENUM_INVERSE_WINDING_SIG[1], FILLTYPE_ENUM_INVERSE_WINDING_SIG[0]);
    myGETMETHODID(mi_ns.filltype_enum_WINDING_Act_mid, ActionsCallBacs_cls, FILLTYPE_ENUM_WINDING_SIG[1], FILLTYPE_ENUM_WINDING_SIG[0]);
    myGETMETHODID(mi_ns.rf_modcall_INSET_Act_mid, ActionsCallBacs_cls, RF_MODCALL_INSET_SIG[1], RF_MODCALL_INSET_SIG[0]);
    myGETMETHODID(mi_ns.rf_modcall_INTERSECT_Act_mid, ActionsCallBacs_cls, RF_MODCALL_INTERSECT_SIG[1], RF_MODCALL_INTERSECT_SIG[0]);
    myGETMETHODID(mi_ns.rf_modcall_OFFSET_Act_mid, ActionsCallBacs_cls, RF_MODCALL_OFFSET_SIG[1], RF_MODCALL_OFFSET_SIG[0]);
    myGETMETHODID(mi_ns.rf_modcall_SORT_Act_mid, ActionsCallBacs_cls, RF_MODCALL_SORT_SIG[1], RF_MODCALL_SORT_SIG[0]);
    myGETMETHODID(mi_ns.rf_modcall_UNION_ffff_Act_mid, ActionsCallBacs_cls, RF_MODCALL_UNION_FFFF_SIG[1], RF_MODCALL_UNION_FFFF_SIG[0]);
    myGETMETHODID(mi_ns.rf_modcall_UNION_ff_Act_mid, ActionsCallBacs_cls, RF_MODCALL_UNION_FF_SIG[1], RF_MODCALL_UNION_FF_SIG[0]);
    myGETMETHODID(mi_ns.pa_fntcall_HINTING_Act_mid, ActionsCallBacs_cls, PA_FNTCALL_HINTING_SIG[1], PA_FNTCALL_HINTING_SIG[0]);
    myGETMETHODID(mi_ns.pa_fntcall_TEXTALIGN_Act_mid, ActionsCallBacs_cls, PA_FNTCALL_TEXTALIGN_SIG[1], PA_FNTCALL_TEXTALIGN_SIG[0]);
    myGETMETHODID(mi_ns.pa_fntcall_TEXTSCALEX_Act_mid, ActionsCallBacs_cls, PA_FNTCALL_TEXTSCALEX_SIG[1], PA_FNTCALL_TEXTSCALEX_SIG[0]);
    myGETMETHODID(mi_ns.pa_fntcall_TEXTSIZE_Act_mid, ActionsCallBacs_cls, PA_FNTCALL_TEXTSIZE_SIG[1], PA_FNTCALL_TEXTSIZE_SIG[0]);
    myGETMETHODID(mi_ns.pa_fntcall_TEXTSKEWX_Act_mid, ActionsCallBacs_cls, PA_FNTCALL_TEXTSKEWX_SIG[1], PA_FNTCALL_TEXTSKEWX_SIG[0]);
    myGETMETHODID(mi_ns.pa_fntcall_SETTYPEFACE_Act_mid, ActionsCallBacs_cls, PA_FNTCALL_SETTYPEFACE_SIG[1], PA_FNTCALL_SETTYPEFACE_SIG[0]);
    myGETMETHODID(mi_ns.pa_align_enum_CENTER_Act_mid, ActionsCallBacs_cls, PA_ALIGN_ENUM_CENTER_SIG[1], PA_ALIGN_ENUM_CENTER_SIG[0]);
    myGETMETHODID(mi_ns.pa_align_enum_LEFT_Act_mid, ActionsCallBacs_cls, PA_ALIGN_ENUM_LEFT_SIG[1], PA_ALIGN_ENUM_LEFT_SIG[0]);
    myGETMETHODID(mi_ns.pa_align_enum_RIGHT_Act_mid, ActionsCallBacs_cls, PA_ALIGN_ENUM_RIGHT_SIG[1], PA_ALIGN_ENUM_RIGHT_SIG[0]);
    myGETMETHODID(mi_ns.typeface_DEFAULT_Act_mid, ActionsCallBacs_cls, TYPEFACE_DEFAULT_SIG[1], TYPEFACE_DEFAULT_SIG[0]);
    myGETMETHODID(mi_ns.typeface_DEFAULT_BOLD_Act_mid, ActionsCallBacs_cls, TYPEFACE_DEFAULT_BOLD_SIG[1], TYPEFACE_DEFAULT_BOLD_SIG[0]);
    myGETMETHODID(mi_ns.typeface_MONOSPACE_Act_mid, ActionsCallBacs_cls, TYPEFACE_MONOSPACE_SIG[1], TYPEFACE_MONOSPACE_SIG[0]);
    myGETMETHODID(mi_ns.typeface_SANS_SERIF_Act_mid, ActionsCallBacs_cls, TYPEFACE_SANS_SERIF_SIG[1], TYPEFACE_SANS_SERIF_SIG[0]);
    myGETMETHODID(mi_ns.typeface_SERIF_Act_mid, ActionsCallBacs_cls, TYPEFACE_SERIF_SIG[1], TYPEFACE_SERIF_SIG[0]);
    myGETMETHODID(mi_ns.pa_strkcall_STROKECAP_Act_mid, ActionsCallBacs_cls, PA_STRKCALL_STROKECAP_SIG[1], PA_STRKCALL_STROKECAP_SIG[0]);
    myGETMETHODID(mi_ns.pa_strkcall_STROKEJOIN_Act_mid, ActionsCallBacs_cls, PA_STRKCALL_STROKEJOIN_SIG[1], PA_STRKCALL_STROKEJOIN_SIG[0]);
    myGETMETHODID(mi_ns.pa_strkcall_STROKEMITER_Act_mid, ActionsCallBacs_cls, PA_STRKCALL_STROKEMITER_SIG[1], PA_STRKCALL_STROKEMITER_SIG[0]);
    myGETMETHODID(mi_ns.pa_strkcall_STROKEWIDTH_Act_mid, ActionsCallBacs_cls, PA_STRKCALL_STROKEWIDTH_SIG[1], PA_STRKCALL_STROKEWIDTH_SIG[0]);
    myGETMETHODID(mi_ns.pa_strkcall_PATHEFFECT_Act_mid, ActionsCallBacs_cls, PA_STRKCALL_PATHEFFECT_SIG[1], PA_STRKCALL_PATHEFFECT_SIG[0]);
    myGETMETHODID(mi_ns.cap_enum_BUTT_Act_mid, ActionsCallBacs_cls, CAP_ENUM_BUTT_SIG[1], CAP_ENUM_BUTT_SIG[0]);
    myGETMETHODID(mi_ns.cap_enum_ROUND_Act_mid, ActionsCallBacs_cls, CAP_ENUM_ROUND_SIG[1], CAP_ENUM_ROUND_SIG[0]);
    myGETMETHODID(mi_ns.cap_enum_SQUARE_Act_mid, ActionsCallBacs_cls, CAP_ENUM_SQUARE_SIG[1], CAP_ENUM_SQUARE_SIG[0]);
    myGETMETHODID(mi_ns.join_enum_BEVEL_Act_mid, ActionsCallBacs_cls, JOIN_ENUM_BEVEL_SIG[1], JOIN_ENUM_BEVEL_SIG[0]);
    myGETMETHODID(mi_ns.join_enum_MITER_Act_mid, ActionsCallBacs_cls, JOIN_ENUM_MITER_SIG[1], JOIN_ENUM_MITER_SIG[0]);
    myGETMETHODID(mi_ns.join_enum_ROUND_Act_mid, ActionsCallBacs_cls, JOIN_ENUM_ROUND_SIG[1], JOIN_ENUM_ROUND_SIG[0]);
    myGETMETHODID(mi_ns.pa_fnt_init_Act_mid, ActionsCallBacs_cls, PA_FNT_INIT_SIG[1], PA_FNT_INIT_SIG[0]);
    myGETMETHODID(mi_ns.pa_clr_init_Act_mid, ActionsCallBacs_cls, PA_CLR_INIT_SIG[1], PA_CLR_INIT_SIG[0]);
    myGETMETHODID(mi_ns.pa_pathmode_init_Act_mid, ActionsCallBacs_cls, PA_PATHMODE_INIT_SIG[1], PA_PATHMODE_INIT_SIG[0]);
    myGETMETHODID(mi_ns.pa_stroke_init_Act_mid, ActionsCallBacs_cls, PA_STROKE_INIT_SIG[1], PA_STROKE_INIT_SIG[0]);
    myGETMETHODID(mi_ns.pa_fill_init_Act_mid, ActionsCallBacs_cls, PA_FILL_INIT_SIG[1], PA_FILL_INIT_SIG[0]);
    myGETMETHODID(mi_ns.pa_cmstmd_init_Act_mid, ActionsCallBacs_cls, PA_CMSTMD_INIT_SIG[1], PA_CMSTMD_INIT_SIG[0]);
    myGETMETHODID(mi_ns.pa_mf_init_Act_mid, ActionsCallBacs_cls, PA_MF_INIT_SIG[1], PA_MF_INIT_SIG[0]);
    myGETMETHODID(mi_ns.pa_cf_init_Act_mid, ActionsCallBacs_cls, PA_CF_INIT_SIG[1], PA_CF_INIT_SIG[0]);
    myGETMETHODID(mi_ns.pa_style_enum_FILL_Act_mid, ActionsCallBacs_cls, PA_STYLE_ENUM_FILL_SIG[1], PA_STYLE_ENUM_FILL_SIG[0]);
    myGETMETHODID(mi_ns.pa_style_enum_FILL_AND_STROKE_Act_mid, ActionsCallBacs_cls, PA_STYLE_ENUM_FILL_AND_STROKE_SIG[1], PA_STYLE_ENUM_FILL_AND_STROKE_SIG[0]);
    myGETMETHODID(mi_ns.pa_style_enum_STROKE_Act_mid, ActionsCallBacs_cls, PA_STYLE_ENUM_STROKE_SIG[1], PA_STYLE_ENUM_STROKE_SIG[0]);
    myGETMETHODID(mi_ns.pexSet_midAct_mid, ActionsCallBacs_cls, PEXSET_MID_SIG[1], PEXSET_MID_SIG[0]);
    myGETMETHODID(mi_ns.pexSet_Act_mid, ActionsCallBacs_cls, PEXSET_SIG[1], PEXSET_SIG[0]);
    myGETMETHODID(mi_ns.ptxInitial_midAct_mid, ActionsCallBacs_cls, PTXINITIAL_MID_SIG[1], PTXINITIAL_MID_SIG[0]);
    myGETMETHODID(mi_ns.ptxInitial_Act_mid, ActionsCallBacs_cls, PTXINITIAL_SIG[1], PTXINITIAL_SIG[0]);
    myGETMETHODID(mi_ns.pixRecord_midAct_mid, ActionsCallBacs_cls, PIXRECORD_MID_SIG[1], PIXRECORD_MID_SIG[0]);
    myGETMETHODID(mi_ns.pixRecord_Act_mid, ActionsCallBacs_cls, PIXRECORD_SIG[1], PIXRECORD_SIG[0]);
    myGETMETHODID(mi_ns.bmxComposite_midAct_mid, ActionsCallBacs_cls, BMXCOMPOSITE_MID_SIG[1], BMXCOMPOSITE_MID_SIG[0]);
    myGETMETHODID(mi_ns.bmxComposite_Act_mid, ActionsCallBacs_cls, BMXCOMPOSITE_SIG[1], BMXCOMPOSITE_SIG[0]);
    myGETMETHODID(mi_ns.rfxMod_midAct_mid, ActionsCallBacs_cls, RFXMOD_MID_SIG[1], RFXMOD_MID_SIG[0]);
    myGETMETHODID(mi_ns.rfxMod_Act_mid, ActionsCallBacs_cls, RFXMOD_SIG[1], RFXMOD_SIG[0]);
    myGETMETHODID(mi_ns.paxInitial_midAct_mid, ActionsCallBacs_cls, PAXINITIAL_MID_SIG[1], PAXINITIAL_MID_SIG[0]);
    myGETMETHODID(mi_ns.paxInitial_Act_mid, ActionsCallBacs_cls, PAXINITIAL_SIG[1], PAXINITIAL_SIG[0]);
    myGETMETHODID(mi_ns.alloc_PE_ref_sv_Act_mid, ActionsCallBacs_cls, ALLOC_PE_REF_SV_SIG[1], ALLOC_PE_REF_SV_SIG[0]);
    myGETMETHODID(mi_ns.alloc_PT_ref_sv_Act_mid, ActionsCallBacs_cls, ALLOC_PT_REF_SV_SIG[1], ALLOC_PT_REF_SV_SIG[0]);
    myGETMETHODID(mi_ns.alloc_PI_ref_sv_Act_mid, ActionsCallBacs_cls, ALLOC_PI_REF_SV_SIG[1], ALLOC_PI_REF_SV_SIG[0]);
    myGETMETHODID(mi_ns.alloc_BM_ref_sv_Act_mid, ActionsCallBacs_cls, ALLOC_BM_REF_SV_SIG[1], ALLOC_BM_REF_SV_SIG[0]);
    myGETMETHODID(mi_ns.alloc_PA_ref_sv_Act_mid, ActionsCallBacs_cls, ALLOC_PA_REF_SV_SIG[1], ALLOC_PA_REF_SV_SIG[0]);
    myGETMETHODID(mi_ns.alloc_RF_ref_sv_Act_mid, ActionsCallBacs_cls, ALLOC_RF_REF_SV_SIG[1], ALLOC_RF_REF_SV_SIG[0]);
    myGETMETHODID(mi_ns.declarations_midAct_mid, ActionsCallBacs_cls, DECLARATIONS_MID_SIG[1], DECLARATIONS_MID_SIG[0]);
    myGETMETHODID(mi_ns.declarations_Act_mid, ActionsCallBacs_cls, DECLARATIONS_SIG[1], DECLARATIONS_SIG[0]);
    myGETMETHODID(mi_ns.onDraw_midAct_mid, ActionsCallBacs_cls, ONDRAW_MID_SIG[1], ONDRAW_MID_SIG[0]);
    myGETMETHODID(mi_ns.onDraw_Act_mid, ActionsCallBacs_cls, ONDRAW_SIG[1], ONDRAW_SIG[0]);

}

