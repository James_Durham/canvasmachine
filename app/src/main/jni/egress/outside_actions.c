//
// Created by james on 1/3/2016.
//

#include <stddef.h>
#include "outside_actions.h"
#include "../jni_env.h"
#include "../utils/exception.h"
#include "method_id_namespace.h"
#include "../utils/ToJava.h"

__thread jobject this_actionsCB = NULL;

#include <stdarg.h>

static inline int call(char *debug_ctx, jmethodID mid, ...) {
    va_list args;
    va_start(args, mid);
    int ret = (*env)->CallIntMethodV(env, this_actionsCB, mid, args);
    va_end(args);
    if (JNI_TRUE == (*env)->ExceptionCheck(env)) abort(); //assertion


}

int float_bin_cnst_Act(float a, float b) {
    return call("float_bin_cnst_Act", mi_ns.float_bin_cnst_Act_mid, a, b);
}

int float_tri_cnst_Act(float a, float b, float c) {
    return call("float_tri_cnst_Act", mi_ns.float_tri_cnst_Act_mid, a, b, c);
}

int float_quad_cnst_Act(float a, float b, float c, float d) {
    return call("float_quad_cnst_Act", mi_ns.float_quad_cnst_Act_mid, a, b, c, d);
}

int float_list_empty_Act() {
    return call("float_list_empty_Act", mi_ns.float_list_empty_Act_mid);
}

int float_list_F_Act(float apnd) {
    return call("float_list_F_Act", mi_ns.float_list_F_Act_mid, apnd);
}

int float_list_FL_F_Act(float apnd) {
    return call("float_list_FL_F_Act", mi_ns.float_list_FL_F_Act_mid, apnd);
}

int float_bin_list_empty_Act() {
    return call("float_bin_list_empty_Act", mi_ns.float_bin_list_empty_Act_mid);
}

int float_bin_list_FBC_Act() {
    return call("float_bin_list_FBC_Act", mi_ns.float_bin_list_FBC_Act_mid);
}

int float_bin_list_FBL_FBC_Act() {
    return call("float_bin_list_FBL_FBC_Act", mi_ns.float_bin_list_FBL_FBC_Act_mid);
}

int float_quad_list_empty_Act() {
    return call("float_quad_list_empty_Act", mi_ns.float_quad_list_empty_Act_mid);
}

int float_quad_list_FQC_Act() {
    return call("float_quad_list_FQC_Act", mi_ns.float_quad_list_FQC_Act_mid);
}

int float_quad_list_FQL_FQC_Act() {
    return call("float_quad_list_FQL_FQC_Act", mi_ns.float_quad_list_FQL_FQC_Act_mid);
}

int float_array_Act() {
    return call("float_array_Act", mi_ns.float_array_Act_mid);
}

int float_bin_array_Act() {
    return call("float_bin_array_Act", mi_ns.float_bin_array_Act_mid);
}

int float_quad_array_Act() {
    return call("float_quad_array_Act", mi_ns.float_quad_array_Act_mid);
}

int color_cnst_sv_color_string_sv_Act(int c) {
    return call("color_cnst_sv_color_string_sv_Act", mi_ns.color_cnst_sv_color_string_sv_Act_mid,
                c);
}

int color_cnst_sv_color_hsv_sv_Act(int c) {
    return call("color_cnst_sv_color_hsv_sv_Act", mi_ns.color_cnst_sv_color_hsv_sv_Act_mid, c);
}

int color_cnst_sv_color_argb_sv_Act(int c) {
    return call("color_cnst_sv_color_argb_sv_Act", mi_ns.color_cnst_sv_color_argb_sv_Act_mid, c);
}


int color_cnst_list_empty_Act() {
    return call("color_cnst_list_empty_Act", mi_ns.color_cnst_list_empty_Act_mid);
}

int color_cnst_list_CC_Act() {
    return call("color_cnst_list_CC_Act", mi_ns.color_cnst_list_CC_Act_mid);
}

int color_cnst_list_CCL_CC_Act() {
    return call("color_cnst_list_CCL_CC_Act", mi_ns.color_cnst_list_CCL_CC_Act_mid);
}

int color_cnst_array_Act() {
    return call("color_cnst_array_Act", mi_ns.color_cnst_array_Act_mid);
}


int pdm_enum_PDM_ADD_Act() {
    return call("pdm_enum_PDM_ADD_Act", mi_ns.pdm_enum_PDM_ADD_Act_mid);
}

int pdm_enum_PDM_CLEAR_Act() {
    return call("pdm_enum_PDM_CLEAR_Act", mi_ns.pdm_enum_PDM_CLEAR_Act_mid);
}

int pdm_enum_PDM_DARKEN_Act() {
    return call("pdm_enum_PDM_DARKEN_Act", mi_ns.pdm_enum_PDM_DARKEN_Act_mid);
}

int pdm_enum_PDM_DST_Act() {
    return call("pdm_enum_PDM_DST_Act", mi_ns.pdm_enum_PDM_DST_Act_mid);
}

int pdm_enum_PDM_DST_ATOP_Act() {
    return call("pdm_enum_PDM_DST_ATOP_Act", mi_ns.pdm_enum_PDM_DST_ATOP_Act_mid);
}

int pdm_enum_PDM_DST_IN_Act() {
    return call("pdm_enum_PDM_DST_IN_Act", mi_ns.pdm_enum_PDM_DST_IN_Act_mid);
}

int pdm_enum_PDM_DST_OUT_Act() {
    return call("pdm_enum_PDM_DST_OUT_Act", mi_ns.pdm_enum_PDM_DST_OUT_Act_mid);
}

int pdm_enum_PDM_DST_OVER_Act() {
    return call("pdm_enum_PDM_DST_OVER_Act", mi_ns.pdm_enum_PDM_DST_OVER_Act_mid);
}

int pdm_enum_PDM_LIGHTEN_Act() {
    return call("pdm_enum_PDM_LIGHTEN_Act", mi_ns.pdm_enum_PDM_LIGHTEN_Act_mid);
}

int pdm_enum_PDM_MULTIPLY_Act() {
    return call("pdm_enum_PDM_MULTIPLY_Act", mi_ns.pdm_enum_PDM_MULTIPLY_Act_mid);
}

int pdm_enum_PDM_OVERLAY_Act() {
    return call("pdm_enum_PDM_OVERLAY_Act", mi_ns.pdm_enum_PDM_OVERLAY_Act_mid);
}

int pdm_enum_PDM_SCREEN_Act() {
    return call("pdm_enum_PDM_SCREEN_Act", mi_ns.pdm_enum_PDM_SCREEN_Act_mid);
}

int pdm_enum_PDM_SRC_Act() {
    return call("pdm_enum_PDM_SRC_Act", mi_ns.pdm_enum_PDM_SRC_Act_mid);
}

int pdm_enum_PDM_SRC_ATOP_Act() {
    return call("pdm_enum_PDM_SRC_ATOP_Act", mi_ns.pdm_enum_PDM_SRC_ATOP_Act_mid);
}

int pdm_enum_PDM_SRC_IN_Act() {
    return call("pdm_enum_PDM_SRC_IN_Act", mi_ns.pdm_enum_PDM_SRC_IN_Act_mid);
}

int pdm_enum_PDM_SRC_OUT_Act() {
    return call("pdm_enum_PDM_SRC_OUT_Act", mi_ns.pdm_enum_PDM_SRC_OUT_Act_mid);
}

int pdm_enum_PDM_SRC_OVER_Act() {
    return call("pdm_enum_PDM_SRC_OVER_Act", mi_ns.pdm_enum_PDM_SRC_OVER_Act_mid);
}

int pdm_enum_PDM_XOR_Act() {
    return call("pdm_enum_PDM_XOR_Act", mi_ns.pdm_enum_PDM_XOR_Act_mid);
}

int pe_element_PE_COMPOSE_Act() {
    return call("pe_element_PE_COMPOSE_Act", mi_ns.pe_element_PE_COMPOSE_Act_mid);
}

int pe_element_PE_CORNER_Act(float radius) {
    return call("pe_element_PE_CORNER_Act", mi_ns.pe_element_PE_CORNER_Act_mid, radius);
}

int pe_element_PE_DASH_Act(float phase) {
    return call("pe_element_PE_DASH_Act", mi_ns.pe_element_PE_DASH_Act_mid, phase);
}

int pe_element_PE_DISCRETE_Act(float segmentLength, float deviation) {
    return call("pe_element_PE_DISCRETE_Act", mi_ns.pe_element_PE_DISCRETE_Act_mid, segmentLength,
                deviation);
}

int pe_element_PE_PATHDASH_Act(int path, float advance, float phase) {
    return call("pe_element_PE_PATHDASH_Act", mi_ns.pe_element_PE_PATHDASH_Act_mid, path, advance,
                phase);
}

int pe_element_PE_SUM_Act() {
    return call("pe_element_PE_SUM_Act", mi_ns.pe_element_PE_SUM_Act_mid);
}

int pathdash_style_enum_MORPH_Act() {
    return call("pathdash_style_enum_MORPH_Act", mi_ns.pathdash_style_enum_MORPH_Act_mid);
}

int pathdash_style_enum_ROTATE_Act() {
    return call("pathdash_style_enum_ROTATE_Act", mi_ns.pathdash_style_enum_ROTATE_Act_mid);
}

int pathdash_style_enum_TRANSLATE_Act() {
    return call("pathdash_style_enum_TRANSLATE_Act", mi_ns.pathdash_style_enum_TRANSLATE_Act_mid);
}

int shader_BITMAP_Act(int bitmap) {
    return call("shader_BITMAP_Act", mi_ns.shader_BITMAP_Act_mid, bitmap);
}

int shader_COMPOSE_Act() {
    return call("shader_COMPOSE_Act", mi_ns.shader_COMPOSE_Act_mid);
}

int shader_LINEAR_Act(float x0, float y0, float x1, float y1) {
    return call("shader_LINEAR_Act", mi_ns.shader_LINEAR_Act_mid, x0, y0, x1, y1);
}

int shader_RADIAL_Act(float centerX, float centerY, float radius) {
    return call("shader_RADIAL_Act", mi_ns.shader_RADIAL_Act_mid, centerX, centerY, radius);
}

int shader_SWEEP_Act(float cx, float cy) {
    return call("shader_SWEEP_Act", mi_ns.shader_SWEEP_Act_mid, cx, cy);
}

int shader_tile_CLAMP_Act() {
    return call("shader_tile_CLAMP_Act", mi_ns.shader_tile_CLAMP_Act_mid);
}

int shader_tile_MIRROR_Act() {
    return call("shader_tile_MIRROR_Act", mi_ns.shader_tile_MIRROR_Act_mid);
}

int shader_tile_REPEAT_Act() {
    return call("shader_tile_REPEAT_Act", mi_ns.shader_tile_REPEAT_Act_mid);
}

int xfermode_Avoid_Act(int opColor, int tolerance) {
    return call("xfermode_Avoid_Act", mi_ns.xfermode_Avoid_Act_mid, opColor, tolerance);
}

int xfermode_PixelXor_Act(int opColor) {
    return call("xfermode_PixelXor_Act", mi_ns.xfermode_PixelXor_Act_mid, opColor);
}

int xfermode_PorterDuff_Act() {
    return call("xfermode_PorterDuff_Act", mi_ns.xfermode_PorterDuff_Act_mid);
}

int xf_avoid_enum_AVOID_Act() {
    return call("xf_avoid_enum_AVOID_Act", mi_ns.xf_avoid_enum_AVOID_Act_mid);
}

int xf_avoid_enum_TARGET_Act() {
    return call("xf_avoid_enum_TARGET_Act", mi_ns.xf_avoid_enum_TARGET_Act_mid);
}

int maskfilter_BLUR_Act(float radius) {
    return call("maskfilter_BLUR_Act", mi_ns.maskfilter_BLUR_Act_mid, radius);
}

int maskfilter_EMBOSS_Act(float ambient, float specular, float blurRadius) {
    return call("maskfilter_EMBOSS_Act", mi_ns.maskfilter_EMBOSS_Act_mid, ambient, specular,
                blurRadius);
}

int mf_blur_enum_INNER_Act() {
    return call("mf_blur_enum_INNER_Act", mi_ns.mf_blur_enum_INNER_Act_mid);
}

int mf_blur_enum_NORMAL_Act() {
    return call("mf_blur_enum_NORMAL_Act", mi_ns.mf_blur_enum_NORMAL_Act_mid);
}

int mf_blur_enum_OUTER_Act() {
    return call("mf_blur_enum_OUTER_Act", mi_ns.mf_blur_enum_OUTER_Act_mid);
}

int mf_blur_enum_SOLID_Act() {
    return call("mf_blur_enum_SOLID_Act", mi_ns.mf_blur_enum_SOLID_Act_mid);
}

int colorfilter_ColorMatrix_Act() {
    return call("colorfilter_ColorMatrix_Act", mi_ns.colorfilter_ColorMatrix_Act_mid);
}

int colorfilter_Lighting_Act(int mul, int add) {
    return call("colorfilter_Lighting_Act", mi_ns.colorfilter_Lighting_Act_mid, mul, add);
}

int colorfilter_PorterDuff_Act(int color) {
    return call("colorfilter_PorterDuff_Act", mi_ns.colorfilter_PorterDuff_Act_mid, color);
}

int colormatrix_Scale_Act(float rScale, float gScale, float bScale, float aScale) {
    return call("colormatrix_Scale_Act", mi_ns.colormatrix_Scale_Act_mid, rScale, gScale, bScale,
                aScale);
}

int colormatrix_Saturation_Act(float sat) {
    return call("colormatrix_Saturation_Act", mi_ns.colormatrix_Saturation_Act_mid, sat);
}

int colormatrix_YUV2RGB_Act() {
    return call("colormatrix_YUV2RGB_Act", mi_ns.colormatrix_YUV2RGB_Act_mid);
}

int colormatrix_RGB2YUV_Act() {
    return call("colormatrix_RGB2YUV_Act", mi_ns.colormatrix_RGB2YUV_Act_mid);
}

int canvascall_CLIPPATH_Act(int path) {
    return call("canvascall_CLIPPATH_Act", mi_ns.canvascall_CLIPPATH_Act_mid, path);
}

int canvascall_CLIPRECT_ffff_Act(float left, float top, float right, float bottom) {
    return call("canvascall_CLIPRECT_ffff_Act", mi_ns.canvascall_CLIPRECT_ffff_Act_mid, left, top,
                right, bottom);
}

int canvascall_CLIPRECT_rf_Act(int rect) {
    return call("canvascall_CLIPRECT_rf_Act", mi_ns.canvascall_CLIPRECT_rf_Act_mid, rect);
}

int canvascall_SAVE_Act() {
    return call("canvascall_SAVE_Act", mi_ns.canvascall_SAVE_Act_mid);
}

int canvascall_RESTORE_Act() {
    return call("canvascall_RESTORE_Act", mi_ns.canvascall_RESTORE_Act_mid);
}

int canvascall_ROTATE_f_Act(float degrees) {
    return call("canvascall_ROTATE_f_Act", mi_ns.canvascall_ROTATE_f_Act_mid, degrees);
}

int canvascall_ROTATE_fff_Act(float degrees, float px, float py) {
    return call("canvascall_ROTATE_fff_Act", mi_ns.canvascall_ROTATE_fff_Act_mid, degrees, px, py);
}

int canvascall_SCALE_ff_Act(float sx, float sy) {
    return call("canvascall_SCALE_ff_Act", mi_ns.canvascall_SCALE_ff_Act_mid, sx, sy);
}

int canvascall_SCALE_ffff_Act(float sx, float sy, float px, float py) {
    return call("canvascall_SCALE_ffff_Act", mi_ns.canvascall_SCALE_ffff_Act_mid, sx, sy, px, py);
}

int canvascall_SKEW_Act(float sx, float sy) {
    return call("canvascall_SKEW_Act", mi_ns.canvascall_SKEW_Act_mid, sx, sy);
}

int canvascall_TRANSLATE_Act(float dx, float dy) {
    return call("canvascall_TRANSLATE_Act", mi_ns.canvascall_TRANSLATE_Act_mid, dx, dy);
}

int canvascall_DRAWARC_Act(int oval, float startAngle, float sweepAngle, int useCenter, int paint) {
    return call("canvascall_DRAWARC_Act", mi_ns.canvascall_DRAWARC_Act_mid, oval, startAngle,
                sweepAngle, useCenter, paint);
}

int canvascall_DRAWBITMAP_Act(int bitmap, float left, float top, int paint) {
    return call("canvascall_DRAWBITMAP_Act", mi_ns.canvascall_DRAWBITMAP_Act_mid, bitmap, left, top,
                paint);
}

int canvascall_DRAWBITMAPMESH_Act(int bitmap, int meshWidth, int meshHeight, int vertOffset,
                                  int colorOffset, int paint) {
    return call("canvascall_DRAWBITMAPMESH_Act", mi_ns.canvascall_DRAWBITMAPMESH_Act_mid, bitmap,
                meshWidth, meshHeight, vertOffset, colorOffset, paint);
}

int canvascall_DRAWCIRCLE_Act(float cx, float cy, float radius, int paint) {
    return call("canvascall_DRAWCIRCLE_Act", mi_ns.canvascall_DRAWCIRCLE_Act_mid, cx, cy, radius,
                paint);
}

int canvascall_DRAWCOLOR_i_Act(int color) {
    return call("canvascall_DRAWCOLOR_i_Act", mi_ns.canvascall_DRAWCOLOR_i_Act_mid, color);
}

int canvascall_DRAWCOLOR_ie_Act(int color) {
    return call("canvascall_DRAWCOLOR_ie_Act", mi_ns.canvascall_DRAWCOLOR_ie_Act_mid, color);
}

int canvascall_DRAWLINES_Act(int paint) {
    return call("canvascall_DRAWLINES_Act", mi_ns.canvascall_DRAWLINES_Act_mid, paint);
}

int canvascall_DRAWOVAL_Act(int oval, int paint) {
    return call("canvascall_DRAWOVAL_Act", mi_ns.canvascall_DRAWOVAL_Act_mid, oval, paint);
}

int canvascall_DRAWPATH_Act(int path, int paint) {
    return call("canvascall_DRAWPATH_Act", mi_ns.canvascall_DRAWPATH_Act_mid, path, paint);
}

int canvascall_DRAWPICTURE_pirf_Act(int picture, int dst) {
    return call("canvascall_DRAWPICTURE_pirf_Act", mi_ns.canvascall_DRAWPICTURE_pirf_Act_mid,
                picture, dst);
}

int canvascall_DRAWPICTURE_pi_Act(int picture) {
    return call("canvascall_DRAWPICTURE_pi_Act", mi_ns.canvascall_DRAWPICTURE_pi_Act_mid, picture);
}

int canvascall_DRAWPOINTS_Act(int paint) {
    return call("canvascall_DRAWPOINTS_Act", mi_ns.canvascall_DRAWPOINTS_Act_mid, paint);
}

int canvascall_DRAWRECT_ffff_Act(float left, float top, float right, float bottom, int paint) {
    return call("canvascall_DRAWRECT_ffff_Act", mi_ns.canvascall_DRAWRECT_ffff_Act_mid, left, top,
                right, bottom, paint);
}

int canvascall_DRAWRECT_rf_Act(int rect, int paint) {
    return call("canvascall_DRAWRECT_rf_Act", mi_ns.canvascall_DRAWRECT_rf_Act_mid, rect, paint);
}

int canvascall_DRAWROUNDRECT_Act(int rect, float rx, float ry, int paint) {
    return call("canvascall_DRAWROUNDRECT_Act", mi_ns.canvascall_DRAWROUNDRECT_Act_mid, rect, rx,
                ry, paint);
}

int canvascall_DRAWTEXT_Act(char *text, float x, float y, int paint) {
    jstring text_j = ToString(text);
    int ret;
    ret = call("canvascall_DRAWTEXT_Act", mi_ns.canvascall_DRAWTEXT_Act_mid, text_j, x, y, paint);
    DeleteLR(text_j);
    return ret;
}

int canvascall_DRAWTEXTONPATH_Act(char *text, int path, float hOffset, float vOffset, int paint) {
    jstring text_j = ToString(text);
    int ret;
    ret = call("canvascall_DRAWTEXTONPATH_Act", mi_ns.canvascall_DRAWTEXTONPATH_Act_mid, text_j,
               path, hOffset, vOffset, paint);
    DeleteLR(text_j);
    return ret;
}

int pt_initcall_FILLTYPE_Act() {
    return call("pt_initcall_FILLTYPE_Act", mi_ns.pt_initcall_FILLTYPE_Act_mid);
}

int pt_initcall_CLOSE_Act() {
    return call("pt_initcall_CLOSE_Act", mi_ns.pt_initcall_CLOSE_Act_mid);
}

int pt_initcall_CUBICTO_Act(float x1, float y1, float x2, float y2, float x3, float y3) {
    return call("pt_initcall_CUBICTO_Act", mi_ns.pt_initcall_CUBICTO_Act_mid, x1, y1, x2, y2, x3,
                y3);
}

int pt_initcall_RCUBICTO_Act(float x1, float y1, float x2, float y2, float x3, float y3) {
    return call("pt_initcall_RCUBICTO_Act", mi_ns.pt_initcall_RCUBICTO_Act_mid, x1, y1, x2, y2, x3,
                y3);
}

int pt_initcall_LINETO_Act(float x, float y) {
    return call("pt_initcall_LINETO_Act", mi_ns.pt_initcall_LINETO_Act_mid, x, y);
}

int pt_initcall_RLINETO_Act(float dx, float dy) {
    return call("pt_initcall_RLINETO_Act", mi_ns.pt_initcall_RLINETO_Act_mid, dx, dy);
}

int pt_initcall_MOVETO_Act(float x, float y) {
    return call("pt_initcall_MOVETO_Act", mi_ns.pt_initcall_MOVETO_Act_mid, x, y);
}

int pt_initcall_RMOVETO_Act(float dx, float dy) {
    return call("pt_initcall_RMOVETO_Act", mi_ns.pt_initcall_RMOVETO_Act_mid, dx, dy);
}

int pt_initcall_QUADTO_Act(float x1, float y1, float x2, float y2) {
    return call("pt_initcall_QUADTO_Act", mi_ns.pt_initcall_QUADTO_Act_mid, x1, y1, x2, y2);
}

int pt_initcall_RQUADTO_Act(float dx1, float dy1, float dx2, float dy2) {
    return call("pt_initcall_RQUADTO_Act", mi_ns.pt_initcall_RQUADTO_Act_mid, dx1, dy1, dx2, dy2);
}

int pt_initcall_TRANSFORM_Act(float *mat) {
    jfloatArray mat_j = ToFloatArray(mat, 9);
    return call("pt_initcall_TRANSFORM_Act", mi_ns.pt_initcall_TRANSFORM_Act_mid, mat_j);
    DeleteLR(mat_j);
}

int filltype_enum_EVEN_ODD_Act() {
    return call("filltype_enum_EVEN_ODD_Act", mi_ns.filltype_enum_EVEN_ODD_Act_mid);
}

int filltype_enum_INVERSE_EVEN_ODD_Act() {
    return call("filltype_enum_INVERSE_EVEN_ODD_Act", mi_ns.filltype_enum_INVERSE_EVEN_ODD_Act_mid);
}

int filltype_enum_INVERSE_WINDING_Act() {
    return call("filltype_enum_INVERSE_WINDING_Act", mi_ns.filltype_enum_INVERSE_WINDING_Act_mid);
}

int filltype_enum_WINDING_Act() {
    return call("filltype_enum_WINDING_Act", mi_ns.filltype_enum_WINDING_Act_mid);
}

int rf_modcall_INSET_Act(float dx, float dy) {
    return call("rf_modcall_INSET_Act", mi_ns.rf_modcall_INSET_Act_mid, dx, dy);
}

int rf_modcall_INTERSECT_Act(float left, float top, float right, float bottom) {
    return call("rf_modcall_INTERSECT_Act", mi_ns.rf_modcall_INTERSECT_Act_mid, left, top, right,
                bottom);
}

int rf_modcall_OFFSET_Act(float dx, float dy) {
    return call("rf_modcall_OFFSET_Act", mi_ns.rf_modcall_OFFSET_Act_mid, dx, dy);
}

int rf_modcall_SORT_Act() {
    return call("rf_modcall_SORT_Act", mi_ns.rf_modcall_SORT_Act_mid);
}

int rf_modcall_UNION_ffff_Act(float left, float top, float right, float bottom) {
    return call("rf_modcall_UNION_ffff_Act", mi_ns.rf_modcall_UNION_ffff_Act_mid, left, top, right,
                bottom);
}

int rf_modcall_UNION_ff_Act(float x, float y) {
    return call("rf_modcall_UNION_ff_Act", mi_ns.rf_modcall_UNION_ff_Act_mid, x, y);
}

int pa_fntcall_HINTING_Act(int bool) {
    return call("pa_fntcall_HINTING_Act", mi_ns.pa_fntcall_HINTING_Act_mid, bool);
}

int pa_fntcall_TEXTALIGN_Act() {
    return call("pa_fntcall_TEXTALIGN_Act", mi_ns.pa_fntcall_TEXTALIGN_Act_mid);
}

int pa_fntcall_TEXTSCALEX_Act(float scaleX) {
    return call("pa_fntcall_TEXTSCALEX_Act", mi_ns.pa_fntcall_TEXTSCALEX_Act_mid, scaleX);
}

int pa_fntcall_TEXTSIZE_Act(float textSize) {
    return call("pa_fntcall_TEXTSIZE_Act", mi_ns.pa_fntcall_TEXTSIZE_Act_mid, textSize);
}

int pa_fntcall_TEXTSKEWX_Act(float skewX) {
    return call("pa_fntcall_TEXTSKEWX_Act", mi_ns.pa_fntcall_TEXTSKEWX_Act_mid, skewX);
}

int pa_fntcall_SETTYPEFACE_Act() {
    return call("pa_fntcall_SETTYPEFACE_Act", mi_ns.pa_fntcall_SETTYPEFACE_Act_mid);
}

int pa_align_enum_CENTER_Act() {
    return call("pa_align_enum_CENTER_Act", mi_ns.pa_align_enum_CENTER_Act_mid);
}

int pa_align_enum_LEFT_Act() {
    return call("pa_align_enum_LEFT_Act", mi_ns.pa_align_enum_LEFT_Act_mid);
}

int pa_align_enum_RIGHT_Act() {
    return call("pa_align_enum_RIGHT_Act", mi_ns.pa_align_enum_RIGHT_Act_mid);
}

int typeface_DEFAULT_Act() {
    return call("typeface_DEFAULT_Act", mi_ns.typeface_DEFAULT_Act_mid);
}

int typeface_DEFAULT_BOLD_Act() {
    return call("typeface_DEFAULT_BOLD_Act", mi_ns.typeface_DEFAULT_BOLD_Act_mid);
}

int typeface_MONOSPACE_Act() {
    return call("typeface_MONOSPACE_Act", mi_ns.typeface_MONOSPACE_Act_mid);
}

int typeface_SANS_SERIF_Act() {
    return call("typeface_SANS_SERIF_Act", mi_ns.typeface_SANS_SERIF_Act_mid);
}

int typeface_SERIF_Act() {
    return call("typeface_SERIF_Act", mi_ns.typeface_SERIF_Act_mid);
}

int pa_strkcall_STROKECAP_Act() {
    return call("pa_strkcall_STROKECAP_Act", mi_ns.pa_strkcall_STROKECAP_Act_mid);
}

int pa_strkcall_STROKEJOIN_Act() {
    return call("pa_strkcall_STROKEJOIN_Act", mi_ns.pa_strkcall_STROKEJOIN_Act_mid);
}

int pa_strkcall_STROKEMITER_Act(float miter) {
    return call("pa_strkcall_STROKEMITER_Act", mi_ns.pa_strkcall_STROKEMITER_Act_mid, miter);
}

int pa_strkcall_STROKEWIDTH_Act(float width) {
    return call("pa_strkcall_STROKEWIDTH_Act", mi_ns.pa_strkcall_STROKEWIDTH_Act_mid, width);
}

int pa_strkcall_PATHEFFECT_Act(int effect) {
    return call("pa_strkcall_PATHEFFECT_Act", mi_ns.pa_strkcall_PATHEFFECT_Act_mid, effect);
}

int cap_enum_BUTT_Act() {
    return call("cap_enum_BUTT_Act", mi_ns.cap_enum_BUTT_Act_mid);
}

int cap_enum_ROUND_Act() {
    return call("cap_enum_ROUND_Act", mi_ns.cap_enum_ROUND_Act_mid);
}

int cap_enum_SQUARE_Act() {
    return call("cap_enum_SQUARE_Act", mi_ns.cap_enum_SQUARE_Act_mid);
}

int join_enum_BEVEL_Act() {
    return call("join_enum_BEVEL_Act", mi_ns.join_enum_BEVEL_Act_mid);
}

int join_enum_MITER_Act() {
    return call("join_enum_MITER_Act", mi_ns.join_enum_MITER_Act_mid);
}

int join_enum_ROUND_Act() {
    return call("join_enum_ROUND_Act", mi_ns.join_enum_ROUND_Act_mid);
}

int pa_fnt_init_Act() {
    return call("pa_fnt_init_Act", mi_ns.pa_fnt_init_Act_mid);
}

int pa_clr_init_Act(int color) {
    return call("pa_clr_init_Act", mi_ns.pa_clr_init_Act_mid, color);
}

int pa_pathmode_init_Act() {
    return call("pa_pathmode_init_Act", mi_ns.pa_pathmode_init_Act_mid);
}

int pa_stroke_init_Act() {
    return call("pa_stroke_init_Act", mi_ns.pa_stroke_init_Act_mid);
}

int pa_fill_init_Act() {
    return call("pa_fill_init_Act", mi_ns.pa_fill_init_Act_mid);
}

int pa_cmstmd_init_Act() {
    return call("pa_cmstmd_init_Act", mi_ns.pa_cmstmd_init_Act_mid);
}

int pa_mf_init_Act() {
    return call("pa_mf_init_Act", mi_ns.pa_mf_init_Act_mid);
}

int pa_cf_init_Act() {
    return call("pa_cf_init_Act", mi_ns.pa_cf_init_Act_mid);
}

int pa_style_enum_FILL_Act() {
    return call("pa_style_enum_FILL_Act", mi_ns.pa_style_enum_FILL_Act_mid);
}

int pa_style_enum_FILL_AND_STROKE_Act() {
    return call("pa_style_enum_FILL_AND_STROKE_Act", mi_ns.pa_style_enum_FILL_AND_STROKE_Act_mid);
}

int pa_style_enum_STROKE_Act() {
    return call("pa_style_enum_STROKE_Act", mi_ns.pa_style_enum_STROKE_Act_mid);
}

int pexSet_midAct(int effect) {
    return call("pexSet_midAct", mi_ns.pexSet_midAct_mid, effect);
}

int pexSet_Act() {
    return call("pexSet_Act", mi_ns.pexSet_Act_mid);
}

int ptxInitial_midAct(int path) {
    return call("ptxInitial_midAct", mi_ns.ptxInitial_midAct_mid, path);
}

int ptxInitial_Act() {
    return call("ptxInitial_Act", mi_ns.ptxInitial_Act_mid);
}

int pixRecord_midAct(int picture, int width, int height) {
    return call("pixRecord_midAct", mi_ns.pixRecord_midAct_mid, picture, width, height);
}

int pixRecord_Act() {
    return call("pixRecord_Act", mi_ns.pixRecord_Act_mid);
}

int bmxComposite_midAct(int bitmap) {
    return call("bmxComposite_midAct", mi_ns.bmxComposite_midAct_mid, bitmap);
}

int bmxComposite_Act() {
    return call("bmxComposite_Act", mi_ns.bmxComposite_Act_mid);
}

int rfxMod_midAct(int rect) {
    return call("rfxMod_midAct", mi_ns.rfxMod_midAct_mid, rect);
}

int rfxMod_Act() {
    return call("rfxMod_Act", mi_ns.rfxMod_Act_mid);
}

int paxInitial_midAct(int paint) {
    return call("paxInitial_midAct", mi_ns.paxInitial_midAct_mid, paint);
}

int paxInitial_Act() {
    return call("paxInitial_Act", mi_ns.paxInitial_Act_mid);
}

int alloc_PE_ref_sv_Act(int effect) {
    return call("alloc_PE_ref_sv_Act", mi_ns.alloc_PE_ref_sv_Act_mid, effect);
}

int alloc_PT_ref_sv_Act(int path) {
    return call("alloc_PT_ref_sv_Act", mi_ns.alloc_PT_ref_sv_Act_mid, path);
}

int alloc_PI_ref_sv_Act(int picture) {
    return call("alloc_PI_ref_sv_Act", mi_ns.alloc_PI_ref_sv_Act_mid, picture);
}

int alloc_BM_ref_sv_Act(int bitmap, int width, int height) {
    return call("alloc_BM_ref_sv_Act", mi_ns.alloc_BM_ref_sv_Act_mid, bitmap, width, height);
}

int alloc_PA_ref_sv_Act(int paint, int flags) {
    return call("alloc_PA_ref_sv_Act", mi_ns.alloc_PA_ref_sv_Act_mid, paint, flags);
}

int alloc_RF_ref_sv_Act(int rect, float left, float top, float right, float bottom) {
    return call("alloc_RF_ref_sv_Act", mi_ns.alloc_RF_ref_sv_Act_mid, rect, left, top, right,
                bottom);
}

int declarations_midAct() {
    return call("declarations_midAct", mi_ns.declarations_midAct_mid);
}

int declarations_Act() {
    return call("declarations_Act", mi_ns.declarations_Act_mid);
}

int onDraw_midAct() {
    return call("onDraw_midAct", mi_ns.onDraw_midAct_mid);
}

int onDraw_Act() {
    return call("onDraw_Act", mi_ns.onDraw_Act_mid);
}
