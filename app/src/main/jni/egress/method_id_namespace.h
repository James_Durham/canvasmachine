//
// Created by james on 1/5/2016.
//

#ifndef CANVASMACHINE_METHOD_ID_NAMESPACE_H
#define CANVASMACHINE_METHOD_ID_NAMESPACE_H

#include <jni.h>
struct m_id_namespace{

    jmethodID float_bin_cnst_Act_mid;
    jmethodID float_tri_cnst_Act_mid;
    jmethodID float_quad_cnst_Act_mid;
    jmethodID float_list_empty_Act_mid;
    jmethodID float_list_F_Act_mid;
    jmethodID float_list_FL_F_Act_mid;
    jmethodID float_bin_list_empty_Act_mid;
    jmethodID float_bin_list_FBC_Act_mid;
    jmethodID float_bin_list_FBL_FBC_Act_mid;
    jmethodID float_quad_list_empty_Act_mid;
    jmethodID float_quad_list_FQC_Act_mid;
    jmethodID float_quad_list_FQL_FQC_Act_mid;
    jmethodID float_array_Act_mid;
    jmethodID float_bin_array_Act_mid;
    jmethodID float_quad_array_Act_mid;

    jmethodID color_cnst_sv_color_string_sv_Act_mid;
    jmethodID color_cnst_sv_color_hsv_sv_Act_mid;
    jmethodID color_cnst_sv_color_argb_sv_Act_mid;

    jmethodID color_cnst_list_empty_Act_mid;
    jmethodID color_cnst_list_CC_Act_mid;
    jmethodID color_cnst_list_CCL_CC_Act_mid;
    jmethodID color_cnst_array_Act_mid;

    jmethodID pdm_enum_PDM_ADD_Act_mid;
    jmethodID pdm_enum_PDM_CLEAR_Act_mid;
    jmethodID pdm_enum_PDM_DARKEN_Act_mid;
    jmethodID pdm_enum_PDM_DST_Act_mid;
    jmethodID pdm_enum_PDM_DST_ATOP_Act_mid;
    jmethodID pdm_enum_PDM_DST_IN_Act_mid;
    jmethodID pdm_enum_PDM_DST_OUT_Act_mid;
    jmethodID pdm_enum_PDM_DST_OVER_Act_mid;
    jmethodID pdm_enum_PDM_LIGHTEN_Act_mid;
    jmethodID pdm_enum_PDM_MULTIPLY_Act_mid;
    jmethodID pdm_enum_PDM_OVERLAY_Act_mid;
    jmethodID pdm_enum_PDM_SCREEN_Act_mid;
    jmethodID pdm_enum_PDM_SRC_Act_mid;
    jmethodID pdm_enum_PDM_SRC_ATOP_Act_mid;
    jmethodID pdm_enum_PDM_SRC_IN_Act_mid;
    jmethodID pdm_enum_PDM_SRC_OUT_Act_mid;
    jmethodID pdm_enum_PDM_SRC_OVER_Act_mid;
    jmethodID pdm_enum_PDM_XOR_Act_mid;

    jmethodID pe_element_PE_COMPOSE_Act_mid;
    jmethodID pe_element_PE_CORNER_Act_mid;
    jmethodID pe_element_PE_DASH_Act_mid;
    jmethodID pe_element_PE_DISCRETE_Act_mid;
    jmethodID pe_element_PE_PATHDASH_Act_mid;
    jmethodID pe_element_PE_SUM_Act_mid;

    jmethodID pathdash_style_enum_MORPH_Act_mid;
    jmethodID pathdash_style_enum_ROTATE_Act_mid;
    jmethodID pathdash_style_enum_TRANSLATE_Act_mid;

    jmethodID shader_BITMAP_Act_mid;
    jmethodID shader_COMPOSE_Act_mid;
    jmethodID shader_LINEAR_Act_mid;
    jmethodID shader_RADIAL_Act_mid;
    jmethodID shader_SWEEP_Act_mid;
    jmethodID shader_tile_CLAMP_Act_mid;
    jmethodID shader_tile_MIRROR_Act_mid;
    jmethodID shader_tile_REPEAT_Act_mid;
    jmethodID xfermode_Avoid_Act_mid;

    jmethodID xfermode_PixelXor_Act_mid;
    jmethodID xfermode_PorterDuff_Act_mid;
    jmethodID xf_avoid_enum_AVOID_Act_mid;
    jmethodID xf_avoid_enum_TARGET_Act_mid;
    jmethodID maskfilter_BLUR_Act_mid;

    jmethodID maskfilter_EMBOSS_Act_mid;
    jmethodID mf_blur_enum_INNER_Act_mid;
    jmethodID mf_blur_enum_NORMAL_Act_mid;
    jmethodID mf_blur_enum_OUTER_Act_mid;
    jmethodID mf_blur_enum_SOLID_Act_mid;

    jmethodID colorfilter_ColorMatrix_Act_mid;
    jmethodID colorfilter_Lighting_Act_mid;
    jmethodID colorfilter_PorterDuff_Act_mid;
    jmethodID colormatrix_Scale_Act_mid;
    jmethodID colormatrix_Saturation_Act_mid;
    jmethodID colormatrix_YUV2RGB_Act_mid;
    jmethodID colormatrix_RGB2YUV_Act_mid;

    jmethodID canvascall_CLIPPATH_Act_mid;
    jmethodID canvascall_CLIPRECT_ffff_Act_mid;
    jmethodID canvascall_CLIPRECT_rf_Act_mid;
    jmethodID canvascall_SAVE_Act_mid;
    jmethodID canvascall_RESTORE_Act_mid;
    jmethodID canvascall_ROTATE_f_Act_mid;
    jmethodID canvascall_ROTATE_fff_Act_mid;
    jmethodID canvascall_SCALE_ff_Act_mid;
    jmethodID canvascall_SCALE_ffff_Act_mid;
    jmethodID canvascall_SKEW_Act_mid;
    jmethodID canvascall_TRANSLATE_Act_mid;
    jmethodID canvascall_DRAWARC_Act_mid;
    jmethodID canvascall_DRAWBITMAP_Act_mid;
    jmethodID canvascall_DRAWBITMAPMESH_Act_mid;
    jmethodID canvascall_DRAWCIRCLE_Act_mid;
    jmethodID canvascall_DRAWCOLOR_i_Act_mid;
    jmethodID canvascall_DRAWCOLOR_ie_Act_mid;
    jmethodID canvascall_DRAWLINES_Act_mid;
    jmethodID canvascall_DRAWOVAL_Act_mid;
    jmethodID canvascall_DRAWPATH_Act_mid;
    jmethodID canvascall_DRAWPICTURE_pirf_Act_mid;
    jmethodID canvascall_DRAWPICTURE_pi_Act_mid;
    jmethodID canvascall_DRAWPOINTS_Act_mid;
    jmethodID canvascall_DRAWRECT_ffff_Act_mid;
    jmethodID canvascall_DRAWRECT_rf_Act_mid;
    jmethodID canvascall_DRAWROUNDRECT_Act_mid;
    jmethodID canvascall_DRAWTEXT_Act_mid;
    jmethodID canvascall_DRAWTEXTONPATH_Act_mid;

    jmethodID pt_initcall_FILLTYPE_Act_mid;
    jmethodID pt_initcall_CLOSE_Act_mid;
    jmethodID pt_initcall_CUBICTO_Act_mid;
    jmethodID pt_initcall_RCUBICTO_Act_mid;
    jmethodID pt_initcall_LINETO_Act_mid;
    jmethodID pt_initcall_RLINETO_Act_mid;
    jmethodID pt_initcall_MOVETO_Act_mid;
    jmethodID pt_initcall_RMOVETO_Act_mid;
    jmethodID pt_initcall_QUADTO_Act_mid;
    jmethodID pt_initcall_RQUADTO_Act_mid;
    jmethodID pt_initcall_TRANSFORM_Act_mid;

    jmethodID filltype_enum_EVEN_ODD_Act_mid;
    jmethodID filltype_enum_INVERSE_EVEN_ODD_Act_mid;
    jmethodID filltype_enum_INVERSE_WINDING_Act_mid;
    jmethodID filltype_enum_WINDING_Act_mid;

    jmethodID rf_modcall_INSET_Act_mid;
    jmethodID rf_modcall_INTERSECT_Act_mid;
    jmethodID rf_modcall_OFFSET_Act_mid;
    jmethodID rf_modcall_SORT_Act_mid;
    jmethodID rf_modcall_UNION_ffff_Act_mid;
    jmethodID rf_modcall_UNION_ff_Act_mid;

    jmethodID pa_fntcall_HINTING_Act_mid;
    jmethodID pa_fntcall_TEXTALIGN_Act_mid;
    jmethodID pa_fntcall_TEXTSCALEX_Act_mid;
    jmethodID pa_fntcall_TEXTSIZE_Act_mid;
    jmethodID pa_fntcall_TEXTSKEWX_Act_mid;
    jmethodID pa_fntcall_SETTYPEFACE_Act_mid;
    jmethodID pa_align_enum_CENTER_Act_mid;
    jmethodID pa_align_enum_LEFT_Act_mid;
    jmethodID pa_align_enum_RIGHT_Act_mid;

    jmethodID typeface_DEFAULT_Act_mid;
    jmethodID typeface_DEFAULT_BOLD_Act_mid;
    jmethodID typeface_MONOSPACE_Act_mid;
    jmethodID typeface_SANS_SERIF_Act_mid;
    jmethodID typeface_SERIF_Act_mid;

    jmethodID pa_strkcall_STROKECAP_Act_mid;
    jmethodID pa_strkcall_STROKEJOIN_Act_mid;
    jmethodID pa_strkcall_STROKEMITER_Act_mid;
    jmethodID pa_strkcall_STROKEWIDTH_Act_mid;
    jmethodID pa_strkcall_PATHEFFECT_Act_mid;

    jmethodID cap_enum_BUTT_Act_mid;
    jmethodID cap_enum_ROUND_Act_mid;
    jmethodID cap_enum_SQUARE_Act_mid;
    jmethodID join_enum_BEVEL_Act_mid;
    jmethodID join_enum_MITER_Act_mid;
    jmethodID join_enum_ROUND_Act_mid;

    jmethodID pa_fnt_init_Act_mid;
    jmethodID pa_clr_init_Act_mid;
    jmethodID pa_pathmode_init_Act_mid;
    jmethodID pa_stroke_init_Act_mid;
    jmethodID pa_fill_init_Act_mid;
    jmethodID pa_cmstmd_init_Act_mid;
    jmethodID pa_mf_init_Act_mid;
    jmethodID pa_cf_init_Act_mid;
    jmethodID pa_style_enum_FILL_Act_mid;
    jmethodID pa_style_enum_FILL_AND_STROKE_Act_mid;
    jmethodID pa_style_enum_STROKE_Act_mid;

    jmethodID pexSet_midAct_mid;
    jmethodID pexSet_Act_mid;

    jmethodID ptxInitial_midAct_mid;
    jmethodID ptxInitial_Act_mid;

    jmethodID pixRecord_midAct_mid;
    jmethodID pixRecord_Act_mid;

    jmethodID bmxComposite_midAct_mid;
    jmethodID bmxComposite_Act_mid;

    jmethodID rfxMod_midAct_mid;
    jmethodID rfxMod_Act_mid;

    jmethodID paxInitial_midAct_mid;
    jmethodID paxInitial_Act_mid;

    jmethodID alloc_PE_ref_sv_Act_mid;
    jmethodID alloc_PT_ref_sv_Act_mid;
    jmethodID alloc_PI_ref_sv_Act_mid;
    jmethodID alloc_BM_ref_sv_Act_mid;
    jmethodID alloc_PA_ref_sv_Act_mid;
    jmethodID alloc_RF_ref_sv_Act_mid;

    jmethodID declarations_midAct_mid;
    jmethodID declarations_Act_mid;

    jmethodID onDraw_midAct_mid;
    jmethodID onDraw_Act_mid;

};

extern jclass ActionsCallBacs_cls;

extern struct m_id_namespace mi_ns;

#endif //CANVASMACHINE_METHOD_ID_NAMESPACE_H
