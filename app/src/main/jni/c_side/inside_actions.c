#include "inside_actions.h"
#include "constants.h"
#include "util/color.h"
#include "util/matix_const.h"

int PE_ref_sv_Act( int d ) {return d;}
int BM_ref_sv_Act( int d ) {return d;}
int PA_ref_sv_Act( int d ) {return d;}
int RF_ref_sv_Act( int d ) {return d;}
int PT_ref_sv_Act( int d ) {return d;}
int PI_ref_sv_Act( int d ) {return d;}

int color_string_COLOR_S_RED_Act()         {return RED_CLR;}
int color_string_COLOR_S_BLUE_Act()        {return BLUE_CLR;}
int color_string_COLOR_S_GREEN_Act()       {return GREEN_CLR;}
int color_string_COLOR_S_BLACK_Act()       {return BLACK_CLR;}
int color_string_COLOR_S_WHITE_Act()       {return WHITE_CLR;}
int color_string_COLOR_S_GRAY_Act()        {return GRAY_CLR;}
int color_string_COLOR_S_CYAN_Act()        {return CYAN_CLR;}
int color_string_COLOR_S_MAGENTA_Act()     {return MAGENTA_CLR;}
int color_string_COLOR_S_YELLOW_Act()      {return YELLOW_CLR;}
int color_string_COLOR_S_LIGHTGRAY_Act()   {return LIGHTGRAY_CLR;}
int color_string_COLOR_S_DARKGRAY_Act()    {return DARKGRAY_CLR;}
int color_string_COLOR_S_GREY_Act()        {return GRAY_CLR;}
int color_string_COLOR_S_LIGHTGREY_Act()   {return LIGHTGRAY_CLR;}
int color_string_COLOR_S_DARKGREY_Act()    {return DARKGRAY_CLR;}
int color_string_COLOR_S_AQUA_Act()        {return AQUA_CLR;}
int color_string_COLOR_S_FUCHSIA_Act()     {return FUCHSIA_CLR;}
int color_string_COLOR_S_LIME_Act()        {return LIME_CLR;}
int color_string_COLOR_S_MAROON_Act()      {return MAROON_CLR;}
int color_string_COLOR_S_NAVY_Act()        {return NAVY_CLR;}
int color_string_COLOR_S_OLIVE_Act()       {return OLIVE_CLR;}
int color_string_COLOR_S_PURPLE_Act()      {return PURPLE_CLR;}
int color_string_COLOR_S_SILVER_Act()      {return SILVER_CLR;}
int color_string_COLOR_S_TEAL_Act()        {return TEAL_CLR;}
int color_string_COLOR_S_TRANSPARENT_Act() {return TRANSPARENT_CLR;}

int color_hsv_sv_Act( int a, float h, float s, float v ) { return ahsv_(a, h, s, v); }
int color_argb_sv_Act( int a, int r, int g, int b ) { return argb_(a, r, g, b); }


matrix *matrixliteral_Rotate_Act( float degrees, float px, float py )
{ return RotateMatrix(degrees, px, py);}
matrix *matrixliteral_Scale_Act( float sx, float sy, float px, float py )
{ return ScaleMatrix(sx, sy, px, py); }
matrix *matrixliteral_Skew_Act( float kx, float ky, float px, float py )
{ return SkewMatrix(kx, ky, px, py); }
matrix *matrixliteral_Translate_Act( float dx, float dy )
{ return TranslateMatrix( dx, dy ); }
matrix *matrixliteral_Concat_Act( matrix left, matrix right ) // TODO check order of parameters
{ return MultiplyMatrix( &left, &right ); }

int paint_flags_sv_empty_Act()
{ return 0;}
int paint_flags_sv_PF_Act( int flag )
{ return flag; }
int paint_flags_sv_PFs_PF_Act( int flags, int flag )
{ return flags | flag; }
int paint_flag_sv_ANTI_ALIAS_Act()
{ return ANTI_ALIAS_FLAG_PAINT; }
int paint_flag_sv_DITHER_Act()
{ return DITHER_FLAG_PAINT; }
int paint_flag_sv_FAKE_BOLD_TEXT_Act()
{ return FAKE_BOLD_TEXT_FLAG_PAINT; }
int paint_flag_sv_FILTER_BITMAP_Act()
{ return FILTER_BITMAP_FLAG_PAINT; }
int paint_flag_sv_LINEAR_TEXT_Act()
{ return LINEAR_TEXT_FLAG_PAINT; }
int paint_flag_sv_STRIKE_THRU_TEXT_Act()
{ return STRIKE_THRU_TEXT_FLAG_PAINT; }
int paint_flag_sv_SUBPIXEL_TEXT_Act()
{ return SUBPIXEL_TEXT_FLAG_PAINT; }
int paint_flag_sv_UNDERLINE_TEXT_Act()
{ return UNDERLINE_TEXT_FLAG_PAINT; }

