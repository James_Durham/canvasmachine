//
// Created by James on 9/3/2015.
//

#ifndef NEWAPIWORK_SCRATCH_COLOR_H
#define NEWAPIWORK_SCRATCH_COLOR_H
int argb_(int alpha, int red, int green, int blue);
int ahsv_(int a, float h, float s, float v);
#endif //NEWAPIWORK_SCRATCH_COLOR_H
