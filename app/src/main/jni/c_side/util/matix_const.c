//
// Created by James on 8/30/2015.
//

#include <stddef.h>
#include "matix_const.h"
#define _GNU_SOURCE
#include <math.h>
#undef _GNU_SOURCE
#include "../../utils/xmalloc.h"


#if defined __APPLE__ || defined  _WIN32
    static void sincosf(float rad, float *sin_dst, float *cos_dst);
#endif
// most of this stuff is from skia

enum {
    kMScaleX,
    kMSkewX,
    kMTransX,
    kMSkewY,
    kMScaleY,
    kMTransY,
    kMPersp0,
    kMPersp1,
    kMPersp2
};


static inline float sdot(float a, float b, float c, float d) {
    return a * b + c * d;
}
static inline float DegreesToRadians(float deg) { return deg * (float) M_PI / 180.0f; }
static inline float rowcol3(const matrix a, int row, const matrix b, int col) {
    return a[0 + row] * b[0 + col] + a[1 + row] * b[3 + col] + a[2 + row] * b[6 + col];
}

matrix *RotateMatrix(float degrees, float px, float py) {
    //matrix *ret = xmalloc(sizeof(matrix));
    float sinV, cosV;
    sincosf(DegreesToRadians(degrees), &sinV, &cosV);
    return SinCosMatrix(sinV, cosV, px, py);
}

matrix *SinCosMatrix(float sinV, float cosV, float px, float py) {
    matrix *ret = xmalloc(sizeof(matrix));
    const float oneMinusCosV = 1 - cosV;
    *(ret) [kMScaleX] = cosV;
    *(ret) [kMSkewX] = -sinV;
    *(ret) [kMTransX] = sdot(sinV, py, oneMinusCosV, px);;

    *(ret) [kMSkewY] = sinV;
    *(ret) [kMScaleY] = cosV;
    *(ret) [kMTransY] = sdot(-sinV, px, oneMinusCosV, py);

    *(ret) [kMPersp0] = *(ret) [kMPersp1] = 0;
    *(ret) [kMPersp2] = 1;

    return ret;
}

matrix *SkewMatrix(float sx, float sy, float px, float py) {
    matrix *ret = xmalloc(sizeof(matrix));
    *(ret) [kMScaleX] = 1;
    *(ret) [kMSkewX] = sx;
    *(ret) [kMTransX] = -sx * py;

    *(ret) [kMSkewY] = sy;
    *(ret) [kMScaleY] = 1;
    *(ret) [kMTransY] = -sy * px;

    *(ret) [kMPersp0] = *(ret) [kMPersp1] = 0;
    *(ret) [kMPersp2] = 1;

    return ret;
}

matrix *TranslateMatrix(float dx, float dy) {
    matrix *ret = xmalloc(sizeof(matrix));
    *(ret) [kMTransX] = dx;
    *(ret) [kMTransY] = dy;

    *(ret) [kMScaleX] = *(ret) [kMScaleY] = *(ret) [kMPersp2] = 1;
    *(ret) [kMSkewX] = *(ret) [kMSkewY] = *(ret) [kMPersp0] = *(ret) [kMPersp1] = 0;
    return ret;
}

matrix *ScaleMatrix(float sx, float sy, float px, float py) {
    return ScaleTranslateMatrix(sx, sy, px - sx * px, py - sy * py);
}

matrix *ScaleTranslateMatrix(float sx, float sy, float tx, float ty) {
    matrix *ret = xmalloc(sizeof(matrix));
    *(ret) [kMScaleX] = sx;
    *(ret) [kMSkewX]  = 0;
    *(ret) [kMTransX] = tx;

    *(ret) [kMSkewY]= 0;
    *(ret) [kMScaleY] = sy;
    *(ret) [kMTransY] = ty;

    *(ret) [kMPersp0] = 0;
    *(ret) [kMPersp1] = 0;
    *(ret) [kMPersp2] = 1;
    return ret;
}

matrix *MultiplyMatrix(matrix *free_a, matrix *free_b) {
    matrix *ret = xmalloc(sizeof(matrix));
    *(ret)[kMScaleX] = rowcol3(*free_a, 0, *free_b, 0);
    *(ret)[kMSkewX] = rowcol3(*free_a, 0, *free_b, 1);
    *(ret)[kMTransX] = rowcol3(*free_a, 0, *free_b, 2);
    *(ret)[kMSkewY] = rowcol3(*free_a, 3, *free_b, 0);
    *(ret)[kMScaleY] = rowcol3(*free_a, 3, *free_b, 1);
    *(ret)[kMTransY] = rowcol3(*free_a, 3, *free_b, 2);
    *(ret)[kMPersp0] = rowcol3(*free_a, 6, *free_b, 0);
    *(ret)[kMPersp1] = rowcol3(*free_a, 6, *free_b, 1);
    *(ret)[kMPersp2] = rowcol3(*free_a, 6, *free_b, 2);
    xfree(free_a);
    xfree(free_b);
    return ret;
}
#if defined __APPLE__ || defined  _WIN32
static void sincosf(float rad, float *sin_dst, float *cos_dst)
{
    *sin_dst = sinf(rad);
    *cos_dst = cosf(rad);
}
#endif
