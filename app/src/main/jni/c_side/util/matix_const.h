//
// Created by James on 8/30/2015.
//

#ifndef NEWAPIWORK_SCRATCH_MATIX_CONST_H
#define NEWAPIWORK_SCRATCH_MATIX_CONST_H
typedef float matrix[9];

matrix *RotateMatrix(float degrees, float px, float py);
matrix *SinCosMatrix(float sinV, float cosV, float px, float py);
matrix *SkewMatrix(float sx, float sy, float px, float py);
matrix *TranslateMatrix(float  dx, float dy);
matrix *ScaleMatrix(float sx, float sy, float px, float py);
matrix *ScaleTranslateMatrix(float sx, float sy, float tx, float ty);

matrix *MultiplyMatrix(matrix *free_a, matrix *free_b);

#endif //NEWAPIWORK_SCRATCH_MATIX_CONST_H
