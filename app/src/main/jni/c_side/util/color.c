//
// Created by James on 9/3/2015.
//

#include <math.h>
#include "color.h"
static void HSVtoRGB( float *r, float *g, float *b, float h, float s, float v ) {
    int i;
    float f, p, q, t;

    if (s < 0.001) {
        // achromatic (grey)
        *r = *g = *b = v;
        return;
    }

    h /= 60.0f;            // sector 0 to 5
    i = (int) floor(h);
    f = h - i;            // fractional part of h
    p = v * (1.0f - s);
    q = v * (1.0f - s * f);
    t = v * (1.0f - s * (1.0f - f));

    switch (i) {
        case 0:
            *r = v;
            *g = t;
            *b = p;
            break;
        case 1:
            *r = q;
            *g = v;
            *b = p;
            break;
        case 2:
            *r = p;
            *g = v;
            *b = t;
            break;
        case 3:
            *r = p;
            *g = q;
            *b = v;
            break;
        case 4:
            *r = t;
            *g = p;
            *b = v;
            break;
        default:        // case 5:
            *r = v;
            *g = p;
            *b = q;
            break;
    }
}
int argb_(int alpha, int red, int green, int blue)
{
    return  ((0xFF & alpha) << 24) | ((0xFF & red) << 16) | ((0xFF & green) << 8) | (0xFF & blue);
}
int ahsv_(int a, float h, float s, float v)
{
    float r, g, b;
    HSVtoRGB(&r, &g, &b, h, s, v);
    int ri, gi, bi;
    ri = (int) (256 * r);
    bi = (int) (256 * b);
    gi = (int) (256 * g);
    //assert(ri < 256);
    //assert(bi < 256);
    //assert(gi < 256);
    return argb_(a, ri, gi, bi);
}
