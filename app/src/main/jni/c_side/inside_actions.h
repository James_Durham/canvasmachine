//
// Created by james on 12/29/2015.
//

#ifndef CANVASMACHINE_ACTIONS_H
#define CANVASMACHINE_ACTIONS_H

#include "util/matix_const.h"

int PE_ref_sv_Act( int d );
int BM_ref_sv_Act( int d );
int PA_ref_sv_Act( int d );
int RF_ref_sv_Act( int d );
int PT_ref_sv_Act( int d );
int PI_ref_sv_Act( int d );

int color_string_COLOR_S_RED_Act();
int color_string_COLOR_S_BLUE_Act();
int color_string_COLOR_S_GREEN_Act();
int color_string_COLOR_S_BLACK_Act();
int color_string_COLOR_S_WHITE_Act();
int color_string_COLOR_S_GRAY_Act();
int color_string_COLOR_S_CYAN_Act();
int color_string_COLOR_S_MAGENTA_Act();
int color_string_COLOR_S_YELLOW_Act();
int color_string_COLOR_S_LIGHTGRAY_Act();
int color_string_COLOR_S_DARKGRAY_Act();
int color_string_COLOR_S_GREY_Act();
int color_string_COLOR_S_LIGHTGREY_Act();
int color_string_COLOR_S_DARKGREY_Act();
int color_string_COLOR_S_AQUA_Act();
int color_string_COLOR_S_FUCHSIA_Act();
int color_string_COLOR_S_LIME_Act();
int color_string_COLOR_S_MAROON_Act();
int color_string_COLOR_S_NAVY_Act();
int color_string_COLOR_S_OLIVE_Act();
int color_string_COLOR_S_PURPLE_Act();
int color_string_COLOR_S_SILVER_Act();
int color_string_COLOR_S_TEAL_Act();
int color_string_COLOR_S_TRANSPARENT_Act();

int color_hsv_sv_Act( int a, float h, float s, float v );
int color_argb_sv_Act( int a, int r, int g, int b );


matrix *matrixliteral_Rotate_Act( float degrees, float px, float py );
matrix *matrixliteral_Scale_Act( float sx, float sy, float px, float py );
matrix *matrixliteral_Skew_Act( float kx, float ky, float px, float py );
matrix *matrixliteral_Translate_Act( float dx, float dy );
matrix *matrixliteral_Concat_Act( matrix left, matrix right ); // TODO check order of parameters

int paint_flags_sv_empty_Act();
int paint_flags_sv_PF_Act( int flag );
int paint_flags_sv_PFs_PF_Act( int flags, int flag );
int paint_flag_sv_ANTI_ALIAS_Act();
int paint_flag_sv_DITHER_Act();
int paint_flag_sv_FAKE_BOLD_TEXT_Act();
int paint_flag_sv_FILTER_BITMAP_Act();
int paint_flag_sv_LINEAR_TEXT_Act();
int paint_flag_sv_STRIKE_THRU_TEXT_Act();
int paint_flag_sv_SUBPIXEL_TEXT_Act();
int paint_flag_sv_UNDERLINE_TEXT_Act();

#endif //CANVASMACHINE_ACTIONS_H
