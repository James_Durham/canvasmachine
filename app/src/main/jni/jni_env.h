//
// Created by James on 8/31/2015.
//

#ifndef NEWAPIWORK_SCRATCH_JNI_ENV_H
#define NEWAPIWORK_SCRATCH_JNI_ENV_H
#include <jni.h>
#include <stddef.h>
extern JNIEnv *env;
extern JavaVM *vm;
JNIEXPORT jint JNICALL _JNI_OnLoad(JavaVM *jvm, void *reserved);
#endif //NEWAPIWORK_SCRATCH_JNI_ENV_H
