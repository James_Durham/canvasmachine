#include "jni_env.h"
#include "grammer_source/lex.yy.h"
#include "grammer_source/grammer.tab.h"
#include "utils/exception.h"
#include "utils/inline_LOGx.h"
#include "egress/outside_actions.h"

static struct container {
    char *str;
    YY_BUFFER_STATE bs;
};

static void free_container(struct container *c)
{
    yy_delete_buffer(c->bs);
    free(c->str);
    free(c);
}

// making this thread local
static __thread struct container *current;
static jthrowable tmp;
JNIEXPORT jlong JNICALL
                Java_Native_CanvasMachine_container_1scan_1string(JNIEnv *env, jobject instance, jstring str_) {
    struct container *c = malloc(sizeof(struct container)); //no checks
    current = c; //no checks
    int len = (*env)->GetStringLength(env, str_);
    int size = (*env)->GetStringUTFLength(env, str_);
    c->str = malloc((size_t) size+1); //no checks
    (*env)->GetStringUTFRegion(env, str_, 0, len, c->str);
    c->bs = yy_scan_string(c->str);
    return (jlong) c;
}

JNIEXPORT void JNICALL
Java_Native_CanvasMachine_container_1delete_1buffer(JNIEnv *env, jobject instance, jlong buffer_state) {
    free_container((struct container *) buffer_state);
}

JNIEXPORT jint JNICALL
               Java_Native_CanvasMachine_myparse(JNIEnv *env_, jobject instance, jobject cb) {
    char ret[128];
    /////////////////
    LOGI_puts("CanvasMachine_yyparse");
    /////////////////
    int exception_code;
    jmp_buf here;
    except = &here;
    env = env_;
    if ((exception_code = setjmp(*except)))
    {
        //if thread enters here abortion should occur.
        // exceptions can't be allowed in parse code.
        LOGI_printf("\nexception_base: %d, msg: %s\n", exception_code, msg);
        snprintf(ret, 128, "E exception_base: %d, msg: %s", exception_code, msg);
        abort();
        return NULL;// code should never get here.
        //except = NULL;
        //env = NULL;
        //return (*env)->NewStringUTF(env, ret);
    }
    this_actionsCB = cb;
    return yyparse();

}

