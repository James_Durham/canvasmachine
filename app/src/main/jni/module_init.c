//
// Created by James on 8/31/2015.
//

#include "module_init.h"
#include "utils/exception.h"
#include "egress/load.c"
#include "utils/java_macros.h"


static void load_classes() {
    jclass tmp;
    if (0 != (*env)->PushLocalFrame(env, 50)) { throw(JNI_EX, "load_classes() PushLocalFrame"); }
    (*env)->PopLocalFrame(env, NULL);
}





void init_() {
    load_classes();
    load();
}
