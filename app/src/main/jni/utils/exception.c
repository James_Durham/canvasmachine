//
// Created by James on 8/30/2015.
//

#include "exception.h"
#include "inline_LOGx.h"
jmp_buf *except = NULL;
int my_errn = 0;
char *msg = NULL;
#define _BUF_SIZE_ 512
static char _BUF_[_BUF_SIZE_];

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

static void dispatch_throw(int exception_base);
void throw(int exception_base, char *p_msg, ...) {
    va_list args;
    va_start(args, p_msg);
    vsnprintf(_BUF_, (size_t) _BUF_SIZE_, p_msg, args);
    va_end(args);
    msg = _BUF_;
    dispatch_throw(exception_base);
}
static void dispatch_throw(int exception_base)
{
    if (except == NULL) {LOGE_puts("except == NULL") ; abort();}
    longjmp(*except, exception_base);
}

