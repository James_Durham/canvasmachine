#include <android/log.h>
inline static void LOGI_puts(const char *mesg)
{
    __android_log_write(ANDROID_LOG_INFO, "puts", mesg);
}
inline static void LOGI_printf(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    __android_log_vprint(ANDROID_LOG_INFO, "printf", format, ap);
    va_end(ap);
}

inline static void LOGE_puts(const char *mesg)
{
    __android_log_write(ANDROID_LOG_ERROR, "puts", mesg);
}
inline static void LOGE_printf(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    __android_log_vprint(ANDROID_LOG_ERROR, "printf", format, ap);
    va_end(ap);
}
