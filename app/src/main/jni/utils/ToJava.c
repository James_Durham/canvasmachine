//
// Created by James on 8/30/2015.
//

#include "ToJava.h"
#include "../jni_env.h"
#include "exception.h"

static inline void ex(char *dbg)
{
    if((*env)->ExceptionCheck(env))
    {
        throw(JNI_EX, dbg);
    }
}

jobject ToGR(jobject localToDel_) {
    jobject ret = (*env)->NewGlobalRef(env, localToDel_);
    if (ret == NULL)
    { throw(JNI_EX, "ToGR JNI_EX"); }
    (*env)->DeleteLocalRef(env, localToDel_);
    return ret;
}
jfloatArray ToFloatArray(float *floats, int num)
{
    jfloatArray ret = (*env)->NewFloatArray(env, num);
    ex("ToFloatArray, NewFloatArray JNI_EX");
    (*env)->SetFloatArrayRegion(env, ret, 0, num, floats);
    ex("ToFloatArray, SetFloatArrayRegion JNI_EX");
    return ret;
}
jstring  ToString(char *txt)
{
    jstring ret = (*env)->NewStringUTF(env, txt);
    ex("ToString JNI_EX");
    return ret;
}
void DeleteLR(jobject localToDel_)
{
    (*env)->DeleteLocalRef(env, localToDel_);
}