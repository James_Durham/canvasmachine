//
// Created by James on 8/30/2015.
//

#ifndef NEWAPIWORK_SCRATCH_TOJAVA_H
#define NEWAPIWORK_SCRATCH_TOJAVA_H

#include <jni.h>

jobject ToGR(jobject localToDel_);
jfloatArray ToFloatArray(float *floats, int num);
jstring  ToString(char *txt);
void DeleteLR(jobject localToDel_);
#endif //NEWAPIWORK_SCRATCH_TOJAVA_H
