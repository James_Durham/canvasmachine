//
// Created by James on 8/30/2015.
//

#ifndef NEWAPIWORK_SCRATCH_XMALLOC_H
#define NEWAPIWORK_SCRATCH_XMALLOC_H
#include <stdlib.h>



extern void *xcalloc( size_t num, size_t size );
extern void  xfree( void *ptr );
extern void *xmalloc( size_t size );
extern void *xrealloc( void *ptr, size_t size );
extern char *xstrdup(char * in);
#endif //NEWAPIWORK_SCRATCH_XMALLOC_H
