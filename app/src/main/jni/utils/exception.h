//
// Created by James on 8/30/2015.
//

#ifndef NEWAPIWORK_SCRATCH_EXCEPTION_H
#define NEWAPIWORK_SCRATCH_EXCEPTION_H

#include <stdlib.h>
#include <setjmp.h>

void throw(int exception_base, char *p_msg, ...);

extern jmp_buf *except;
extern int my_errn;
extern char *msg;


/* exceptions base */
#define ALLOC_EX -1
#define JNI_EX   -2
#define OTHER_EX -3
#endif //NEWAPIWORK_SCRATCH_EXCEPTION_H
