//
// Created by James on 8/30/2015.
//

#include "xmalloc.h"
#include "exception.h"
#include <errno.h>
#include <string.h>

extern void *xcalloc(size_t num, size_t size) {
    void *ret = calloc(num, size);
    if (ret == NULL) {
        my_errn = errno;
        throw(ALLOC_EX, "xcalloc strerror \"%s\"", strerror(my_errn));
    }
    return ret;
}

extern void xfree(void *ptr) {
    free(ptr);
}

extern void *xmalloc(size_t size) {
    void *ret = malloc(size);
    if (ret == NULL) {
        my_errn = errno;
        throw(ALLOC_EX, "xmalloc strerror \"%s\"", strerror(my_errn));
    }
    return ret;
}

extern void *xrealloc(void *ptr, size_t size) {
    void *ret = realloc(ptr, size);
    if (ret == NULL) {
        my_errn = errno;
        throw(ALLOC_EX, "xrealloc strerror \"%s\"", strerror(my_errn));
    }
    return ret;
}

char *xstrdup(char *in) {
    char *ret = strdup(in);
    if (ret == NULL) {
        my_errn = errno;
        throw(ALLOC_EX, "xstrdup strerror \"%s\"", strerror(my_errn));
    }
    return ret;

}
