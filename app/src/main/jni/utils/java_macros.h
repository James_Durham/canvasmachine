#ifndef JAVA_MACROS_H
#define JAVA_MACROS_H

#define LOAD_CLASS(VAR_, SIG_) do {\
  if ( NULL == ( tmp = (*env)->FindClass(env, SIG_ ) ) )\
  { throw(JNI_EX, "LOAD_CLASS(" #VAR_ ", " #SIG_ ") FindClass"  ); }\
  if ( NULL == ( VAR_ = (*env)->NewGlobalRef(env, tmp ) ) )\
  { throw(JNI_EX, "LOAD_CLASS(" #VAR_ ", " #SIG_ ") NewGlobalRef"  ); }\
} while (0)

#define myGETSTATICOBJECTFIELD(OBJ, CLASS, FID) do {\
    jobject tmp;\
    if ( NULL == (tmp = (*env)->GetStaticObjectField(env, CLASS , FID )))\
 { throw(JNI_EX, "myGETSTATICOBJECTFIELD(" #OBJ ", " #CLASS ", " #FID ") GetStaticObjectField"  ); } \
    if ( NULL == (OBJ = (*env)->NewGlobalRef(env, tmp ))) \
 { throw(JNI_EX, "myGETSTATICOBJECTFIELD(" #OBJ ", " #CLASS ", " #FID ") NewGlobalRef"  ); } \
 (*env)->DeleteLocalRef(env, tmp);\
} while (0)
#define myGETSTATICFIELDID(FID, CLASS, SIG_STRUCT, SIG) do {\
    if (NULL == ( FID = (*env)->GetStaticFieldID(env, CLASS, SIG_STRUCT.SIG [0], SIG_STRUCT.SIG [1] )))\
    { throw(JNI_EX, "myGETSTATICFIELDID(" #FID ", " #CLASS ", " #SIG_STRUCT ", " #SIG ") GetStaticFieldID"); }\
} while (0)
#define myGETMETHODID(MID, CLASS, NAME, SIG) do { \
if (NULL == ( MID = (*env)->GetMethodID(env, CLASS , NAME , SIG ))) \
{ throw(JNI_EX, "myGETMETHODID(" #MID ", " #CLASS ", " #NAME ", " #SIG ") GetMethodID"); } \
} while (0)


#endif //JAVA_MACROS_H
