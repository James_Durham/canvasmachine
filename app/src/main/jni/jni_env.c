//
// Created by James on 8/31/2015.
//

#include "jni_env.h"
#include "module_init.h"
#include "utils/exception.h"
#include <stdio.h>
#include <android/log.h>
JNIEnv *env = NULL;
JavaVM *vm = NULL;

inline static void LOGI_puts(const char *mesg)
{
    __android_log_write(ANDROID_LOG_INFO, "puts", mesg);
}
inline static void LOGI_printf(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    __android_log_vprint(ANDROID_LOG_INFO, "printf", format, ap);
    va_end(ap);
}

jint JNICALL JNI_OnLoad(JavaVM *jvm, void *reserved) {
    /////////////////
    LOGI_puts("JNI_OnLoad");
    /////////////////
    int exception_code;
    jmp_buf here;
    except = &here;
    vm = jvm;
    if ((*vm)->GetEnv(vm, (void **) &env, JNI_VERSION_1_6))
    {
        return JNI_ERR;
    }
    if ((exception_code = setjmp(*except)))
    {
        LOGI_printf("\nexception_base: %d, msg: %s\n", exception_code, msg);
        return JNI_ERR;
    }
    init_();
    except = NULL;
    env = NULL;
    return JNI_VERSION_1_6;
}
