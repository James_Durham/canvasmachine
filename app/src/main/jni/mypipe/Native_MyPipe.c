#include <jni.h>
#include <stdio.h>
#include <errno.h>
#include <malloc.h>
#include <unistd.h>

static inline FILE ** TOFILEPP(jlong v) {return (FILE **) v; }
static inline int * TOINTP(jlong v) {return (int *) v; }

// FROM SWIG
/* Support for throwing Java exceptions */
typedef enum {
    JavaOutOfMemoryError = 1,
    JavaIOException,
    JavaRuntimeException,
    JavaIndexOutOfBoundsException,
    JavaArithmeticException,
    JavaIllegalArgumentException,
    JavaNullPointerException,
    JavaDirectorPureVirtual,
    JavaUnknownError
} JavaExceptionCodes;

typedef struct {
    JavaExceptionCodes code;
    const char *java_exception;
} JavaExceptions_t;


static void JavaThrowException(JNIEnv *jenv, JavaExceptionCodes code, const char *msg) {
    jclass excep;
    static const JavaExceptions_t java_exceptions[] = {
            {JavaOutOfMemoryError,          "java/lang/OutOfMemoryError"},
            {JavaIOException,               "java/io/IOException"},
            {JavaRuntimeException,          "java/lang/RuntimeException"},
            {JavaIndexOutOfBoundsException, "java/lang/IndexOutOfBoundsException"},
            {JavaArithmeticException,       "java/lang/ArithmeticException"},
            {JavaIllegalArgumentException,  "java/lang/IllegalArgumentException"},
            {JavaNullPointerException,      "java/lang/NullPointerException"},
            {JavaDirectorPureVirtual,       "java/lang/RuntimeException"},
            {JavaUnknownError,              "java/lang/UnknownError"},
            {(JavaExceptionCodes) 0,        "java/lang/UnknownError"}
    };
    const JavaExceptions_t *except_ptr = java_exceptions;

    while (except_ptr->code != code && except_ptr->code)
        except_ptr++;

    (*jenv)->ExceptionClear(jenv);
    excep = (*jenv)->FindClass(jenv, except_ptr->java_exception);
    if (excep)
        (*jenv)->ThrowNew(jenv, excep, msg);
}
// END SWIG SOURCE

inline static void *db_Adress(JNIEnv *env, jobject buf) {
    return (*env)->GetDirectBufferAddress(env, buf);
}
#define DB_AD(RET, PENV, BUF) if (NULL == ( RET = db_Adress(PENV, BUF))) { JavaThrowException( PENV, JavaRuntimeException, "GetDirectBufferAddress returned NULL"); return -1; }
/*
 * Class:     Native_MyPipe
 * Method:    fread
 * Signature: (Ljava/nio/ByteBuffer;IIJJ)I
 */
JNIEXPORT jint JNICALL Java_Native_MyPipe_MyPipe_1JNI_fread
        (JNIEnv *penv, jclass class, jobject buf, jint pos, jint limit, jlong errno_p,
         jlong read_pp) {
    FILE **read_ = TOFILEPP(read_pp);
    int *errno_ = TOINTP(errno_p);
    void *buf_;
    DB_AD(buf_, penv, buf);
    void *start = buf_ + pos;
    size_t amount = (size_t) limit - pos;
    int call_ret = fread(start, 1, amount, *read_);
    *errno_ = errno;
    return pos + call_ret;
}

/*
 * Class:     Native_MyPipe
 * Method:    fwrite
 * Signature: (Ljava/nio/ByteBuffer;IIJJ)I
 */
JNIEXPORT jint JNICALL Java_Native_MyPipe_MyPipe_1JNI_fwrite
        (JNIEnv *penv, jclass class, jobject buf, jint pos, jint limit, jlong errno_p,
         jlong write_pp) {
    FILE **write_ = TOFILEPP(write_pp);
    int *errno_ = TOINTP(errno_p);
    void *buf_;
    DB_AD(buf_, penv, buf);
    void *start = buf_ + pos;
    size_t amount = (size_t) limit - pos;
    int call_ret = fwrite(start, 1, amount, *write_);
    *errno_ = errno;
    return pos + call_ret;
}

/*
 * Class:     Native_MyPipe
 * Method:    newErrno
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_Native_MyPipe_MyPipe_1JNI_newErrno
        (JNIEnv *penv, jclass class) {
    int *alloc = malloc(sizeof(int));
    if (alloc == NULL) {JavaThrowException(penv, JavaOutOfMemoryError, "alloc errno");
        return NULL;}
    return (jlong) alloc;
}

/*
 * Class:     Native_MyPipe
 * Method:    deleteErrno
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_Native_MyPipe_MyPipe_1JNI_deleteErrno
        (JNIEnv *penv, jclass class, jlong errno_p) {
    int *errno_ = TOINTP(errno_p);
    free(errno_);
}

/*
 * Class:     Native_MyPipe
 * Method:    new_FILE_pp
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_Native_MyPipe_MyPipe_1JNI_new_1FILE_1pp
        (JNIEnv *penv, jclass class) {
    FILE **alloc = malloc(sizeof(FILE *));
    if (alloc == NULL) {JavaThrowException(penv, JavaOutOfMemoryError, "alloc file_p");
        return NULL;}
    *alloc = NULL;
    return (jlong) alloc;
}


/*
 * Class:     Native_MyPipe
 * Method:    delete_FILE_pp
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_Native_MyPipe_MyPipe_1JNI_delete_1FILE_1pp
        (JNIEnv *penv, jclass class, jlong file_pp) {
    FILE **file_ = TOFILEPP(file_pp);
    free(file_);
}


/*
 * Class:     Native_MyPipe
 * Method:    fpipe
 * Signature: (JJJ)I
 */
JNIEXPORT jint JNICALL Java_Native_MyPipe_MyPipe_1JNI_fpipe
        (JNIEnv *penv, jclass class, jlong read_pp, jlong write_pp, jlong errno_p) {
    FILE **write_ = TOFILEPP(write_pp);
    FILE **read_ = TOFILEPP(read_pp);
    int *errno_ = TOINTP(errno_p);
    int fds[2];
    if (pipe(fds) != 0)
    {
        *errno_ = errno;
        JavaThrowException(penv, JavaIOException, "pipe(fds) != 0");
        return -1;
    }
    if (NULL == (*read_ = fdopen(fds[0], "rb")))
    {
        *errno_ = errno;
        JavaThrowException(penv, JavaIOException, "NULL == (*read_ = fdopen(fds[0], \"rb\"))");
        return -1;
    }
    if (NULL == (*write_ = fdopen(fds[1], "wb")))
    {
        *errno_ = errno;
        JavaThrowException(penv, JavaIOException, "NULL == (*write_ = fdopen(fds[1], \"wb\"))");
        return -1;
    }
    return 0;
}

/*
 * Class:     Native_MyPipe
 * Method:    errStr
 * Signature: (J)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_Native_MyPipe_MyPipe_1JNI_errStr
        (JNIEnv *penv, jclass class, jlong errno_p) {
    int *errno_ = TOINTP(errno_p);
    return (*penv)->NewStringUTF(penv, strerror(*errno_));
}

/*
 * Class:     Native_MyPipe
 * Method:    feof
 * Signature: (J)Z
 */
JNIEXPORT jboolean JNICALL Java_Native_MyPipe_MyPipe_1JNI_feof
        (JNIEnv *penv, jclass class, jlong file_pp) {
    FILE **file_ = TOFILEPP(file_pp);
    jboolean ret =  (feof(*file_)) ? (jboolean) JNI_TRUE : (jboolean) JNI_FALSE;
    return ret ;
}

/*
 * Class:     Native_MyPipe
 * Method:    ferror
 * Signature: (J)Z
 */
JNIEXPORT jboolean JNICALL Java_Native_MyPipe_MyPipe_1JNI_ferror
        (JNIEnv *penv, jclass class, jlong file_pp) {
    FILE **file_ = TOFILEPP(file_pp);
    jboolean ret =  (ferror(*file_)) ? (jboolean) JNI_TRUE : (jboolean) JNI_FALSE;
    return ret ;
}

/*
 * Class:     Native_MyPipe
 * Method:    fclose
 * Signature: (JJ)Z
 */
JNIEXPORT jboolean JNICALL Java_Native_MyPipe_MyPipe_1JNI_fclose
        (JNIEnv *penv, jclass class, jlong file_pp, jlong errno_p) {
    FILE **file_ = TOFILEPP(file_pp);
    int *errno_ = TOINTP(errno_p);
    if(*file_ == NULL) return JNI_TRUE;
    if (fclose(*file_))
    {
        *errno_ = errno;
        *file_ = NULL;
        return (jboolean) JNI_FALSE;
    } else {
        *file_ = NULL;
        return (jboolean) JNI_TRUE;
    }
}

JNIEXPORT jboolean JNICALL
Java_Native_MyPipe_MyPipe_1JNI_fflush(JNIEnv *env, jclass type, jlong file_pp, jlong errno_p) {
    FILE **file_ = TOFILEPP(file_pp);
    int *errno_ = TOINTP(errno_p);
    if (fflush(*file_))
    {
        *errno_ = errno;
        return (jboolean) JNI_FALSE;
    } else {
        return (jboolean) JNI_TRUE;
    }

}

